CPY Oct 04, 2008, Add the PERT part for the processes Z0_RA_UUB and Z0_RA_DDB 
C Feb 10, 2005, B(2) for bb-->HB process is added, and eliminate 
C "omega" in HB process which is not needed if the input
C B quark mass is the MS-bar running mass at M_B scale. 
C Error: April 14, 2004 (correct ALEPASY for A0, etc)
c-----------------------------------------------------------------------
c %%%%%%%%%%%%%%%%%           VERSION 6.1           %%%%%%%%%%%%%%%%%%%
c-----------------------------------------------------------------------
C
CsB   Nov. 17, 1997 q Q -> Z Z process is added.
C
CsB   Mar. 30, 1997
C     This file and the numerical results for the Y piece were tested against
C     C.-P.'s version. This file contains all information what is in C.-P.'s
C     and some additional comments. The pion initial state is also contained
C     here but not in C.-P.'s file.
C     The numerical results are identical
C     for ppBar, pp and pN processes
C     for W+/-, Z0, A0, AA, AG and H0 bosons
C     for Q = 80 GeV, y = 0 and 2.5 and QT = 2.4 and 10 GeV's
C     (for any of the above combinations).
C     Remains to be checked:
C     - the pion initial state process was not tested against another
C       code or the literature,
C     - the AA and AG pieces use 'fake' (DY type) Y pieces,
C       and in order to compare to experiments the real NLO pieces have to
C       be implemented. (It is under way as of 3/30/97),
C     - for AA and AG the A1-3 pieces in ppBar, pp and pN collisions are zero,
C     - for H0 production all the perturbative pieces are the same
C       (for any hadronic initial state),
C     - for pp collision the A1 and A3 pieces are zero (for all bosons)
C       when y = 0,
C     - agreement of GG and GL options with C.-P.
C
C
CCPY  May 1996 -- a major bug was found for (P N) process
C                 inside PNX1X2INT(X1,X2...)
C
CsB   Feb.  7, 1996 -- L0 and A3 pieces now ckecked to cancel in ppBar,
C                      pp and pN cases for W+/-, Z and photon.
c
CsB   Jan. 27, 1996 -- Implemented full regular perturbative pieces A1-4.
C                      L0,A3 cancels with L0,A3 of asmptotic piece for low qT.
C                      A2 checks against Mirkes.
c
CsB   Dec. 12, 1995 -- Separation of qqBar and qG processes implemented
c                      by the switch I_Proc
c
CsB   Dec.  5, 1995 -- Installed asymmetric pieces in parton luminosities.
c
CCPY  Oct. 23, 1995 -- Added LTOPT option for LO result
C                      (refer to WZNLO.FOR)
CCPY  Oct. 14, 1995 -- Added virtual gluon "resonance"
C
CCPY This code combines my original PERT.FOR and PERT_PP.FOR.
C    It does calculations for either ppbar or pp or pN collisions.
C
      function fpertppbar(vmas,vpt,rapin,gees,q0in,fresum,pma,pert,asy,
     >                    ier_pert)
c These routines are for computing the perturbative and the asymptotic
c   pieces in lowest order for P+PBAR collisions ONLY.
c
c the function version
c
      implicit none
      integer ier_pert
      real*8 fpertppbar
      real*8 vmas,vpt,rapin,gees(3),q0in,fresum,pma,pert,asy
      call spertppbar(vmas,vpt,rapin,gees,q0in,fresum,pma,pert,asy,
     >                ier_pert)
c returns the contribution of PERTURBATIVE-ASMYPTOTIC
      fpertppbar=pma
      return
      end
c
c The subroutine version
c
      subroutine spertppbar(vmas,vpt,rapin,gees,q0in,
     >                     fresum,pma,gal_pert,gal_asymp,ier_pert)
Cgal: OCT 1993, JULY 1994
CCPY OCTOBER 1992, JULY 1993
C THIS IS MODEIFIED TO ONLY WORK FOR P PBAR MACHINE
C THIS IS PERT_PPBAR.FOR

      IMPLICIT NONE
      INCLUDE 'common.for'

      INTEGER NF_EFF
      REAL*8 PERT
      REAL*8 ASYMP,AMU
      INTEGER IRUN,NRUN
csb      REAL*8 EW_ALFA
      LOGICAL DUMB
cgal:
      integer ier_pert
      real*8 vmas,vpt,rapin,gees(3),q0in,fresum,pma,gal_pert,gal_asymp
      Real*8 Coup_M, Omega
      Double Precision RUN_MASS_TOT
      REAL*8 XMTOP,XMBOT,XMC
      COMMON/XMASS/XMTOP,XMBOT,XMC

      Integer KinCorr
      Common / CorrectKinematics / KinCorr
  
      real*8 pyalem
      external pyalem

      Logical First_1
      Data First_1 /.True./
      
      real*8 Z_beta0, Z_beta1

CJI Jan 2015: Add in hj calculation
      Integer HasJet
      REAL*8 D1s, R, t, ptj
      REAL*8 H0Approx
      External H0Approx
      Common /Jet/ D1s, R, t, ptj, HasJet
      REAL*8 KK,dsigmadt,Kgg,tau,GHJ2

      ier_pert=0
Cgal: no longer do we enforce lowest order QCD; technically wrong here, but
c     numerically insignificant
c      IF(NORDER.EQ.2.and.rdflag) THEN
c       WRITE(*,*) ' FORCE NORDER = 1 IN PERT.FOR FOR ALL PDF'
c       WRITE(NWRT,*) ' FORCE NORDER = 1 IN PERT.FOR FOR ALL PDF'
c       NORDER=1
c     ENDIF

      Q_V=vmas
      S_Q=Q_V

      S_LAM=ALAMBD(S_Q)
      NF_EFF=NFL(S_Q)
c this setup depends on NF_EFF and thus S_Q, and so must be done repetively
C------------
      IF(TYPE_V.EQ.'H0' .OR. TYPE_V.EQ.'AG' .OR.
     >   TYPE_V.EQ.'ZG' .OR. TYPE_V.EQ.'GG') THEN ! Initial state is g g
       CALL HSETUP(NF_EFF)
      ELSEIF (HasJet.Eq.1) then
       Call VJSETUP(NF_EFF)
      ELSE                                        ! Initial state is q Q
       CALL WSETUP(NF_EFF)
      ENDIF

      S_BETA1=(33.0-2.0*NF_EFF)/12.0

c      ECM=ECM*UNIT
      ECM=ECM
      ECM2=ECM**2

CCPY SET NO_SIGMA0=0 FOR INCLUDING CONST=\sigma_0 IN THE RATES
C    SET NO_SIGMA0=1 FOR NOT INCLUDING CONST=\sigma_0 IN THE RATES
CsB        NO_SIGMA0=0

CCPY SET LTOPT=0 FOR CALCULATING THE ASYMPTOTIC PART
C    SET LTOPT=1 FOR CALCULATING THE DELTA_SIGMA FROM QT=0 TO PT
C    SET LTOPT=-1 FOR CALCULATING THE LEADING ORDER RESULT

CCPY
      IF (Type_V.Eq.'W+_RA_UDB' .or. Type_V.Eq.'W-_RA_DUB' 
     > .or. Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB' 
CJI September 2013: Added in new names for Z and W bosons to match c++ version
     > .or. Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD'
     > .or. Type_V.Eq.'WU' .or. Type_V.Eq.'WD' ) THEN
        NO_SIGMA0 = 1
      ENDIF

      IF (NO_SIGMA0.EQ.1) THEN
         CONST=1.0D0/2.0D0
      ELSE
CBY05/18
        IF(TYPE_V.EQ.'W+'.OR. TYPE_V.EQ.'HP' ) THEN
C THIS IS THE \sigma_0 IN MY NOTES
          CONST=PI*WCOUPL*2.0/3.0

        ELSEIF(TYPE_V.EQ.'W-' .OR.TYPE_V.EQ.'HM') THEN
C THIS IS THE \sigma_0 IN MY NOTES
          CONST=PI*WCOUPL*2.0/3.0

        ELSEIF(TYPE_V.EQ.'Z0' .OR. TYPE_V.EQ.'HZ') THEN
C THIS IS THE \sigma_0 IN MY NOTES
          CONST=PI*ZCOUPL/3.0

        ELSEIF(TYPE_V.EQ.'H0') THEN
C THIS IS THE (\sigma_0)*(Q_V)^2 IN MY NOTES
!ZL compensate running alpha_s for non-canonical choice
          Z_beta0=4*S_BETA1
          Z_beta1=34*3**2/3.d0-2*NF_EFF*(4/3.d0+5*3/3.d0)
          CONST=HCOUPL*(ALPI(C2*S_Q)*Q_V)**2
     &     *(1+Z_beta0/2d0*ALPI(S_Q)*log(C2**2)+1d0/16d0*ALPI(S_Q)**2
     &       *(Z_beta0**2*log(C2**2)**2-2*Z_beta1*log(C2**2)))

        Else If (TYPE_V.EQ.'H+') then
          CONST=Pi*HpCOUPL*2.0/3.0 ! = Pi/3 CR^2/4
CsB       Moved top mass factor here from Subroutine Standard, since Q_V is not
C         defined there.
          IF (I_MODEL.EQ.1) THEN  ! TOPCOLOR MODEL
            IF(I_RUNMASS.EQ.1) THEN
              COUP_M=RUN_MASS_TOT(Q_V,6)
            ELSE
              COUP_M=MT
            ENDIF
          ELSE IF (I_MODEL.EQ.2) THEN ! FOR 2HDMIII:
            IF(I_RUNMASS.EQ.1) THEN
              COUP_M=Sqrt(RUN_MASS_TOT(Q_V,6)*RUN_MASS_TOT(Q_V,4))
            ELSE
              COUP_M=Sqrt(MT*qmas_c)
            ENDIF
          End If
          CF = 4.d0/3.d0
          CONST=CONST*COUP_M**2

        Else If (TYPE_V.EQ.'HB') then
          CONST=Pi*HbCOUPL*2.0/3.0 ! = Pi/3 CR^2/4
CsB       Moved top mass factor here from Subroutine Standard, since Q_V is not
C         defined there.
          IF (I_RUNMASS.EQ.1) THEN
            COUP_M=RUN_MASS_TOT(Q_V,5)
          ELSE
            COUP_M=XMBOT
          End If
          CF = 4.d0/3.d0
      
          IF(FIRST_1) THEN
            Write(*,*) ' '
            WRITE(11,*) ' I_RUNMASS, COUP_M, XMBOT '
            WRITE(11,*)  I_RUNMASS, COUP_M, XMBOT
            FIRST_1=.FALSE.
          ENDIF
          
C          If (LTOpt.GT.-1.AND.I_RUNMASS.EQ.1) then
CCPY Feb 10, 2005, eliminate "omega" in HB process which 
C is not needed if the input
C B quark mass is the MS-bar running mass at M_B scale. 
C In C(1) function, we do not include the 3.d0*DLog((Q_V/XMBOT)**2)
C term of Omega, where 
C  Omega = 3.d0*DLog((Q_V/XMBOT)**2) + 4.d0.
C The 2nd term, 4, in Omega is due to the conversion from pole mass to MS-bar
C running mass. Thus, it is not correct to include the following line
c COUP_M = COUP_M*( 1.d0 + CF*Alpi(Q_V)/4.d0*Omega )
C when a MS-bar running mass is used.
C          End If

          CONST=CONST*COUP_M**2
!          Print*, ' CONST,HbCOUPL = ', CONST,HbCOUPL
C          Print*, ' Q_V,COUP_M = ', Q_V,COUP_M

        ELSEIF(TYPE_V.EQ.'A0') THEN
C THIS IS THE \sigma_0 IN MY NOTES
C ASSUME EW_ALFA IS NOT RUNNING
C          EW_ALFA=1.0D0/137.0359895D0
C          CONST=EW_ALFA
C          CONST=(CONST**2)*4.0*PI/9.0/Q_V/Q_V
          CONST=PI**2*pyalem(q_v**2)*4.0/3.0

CCPY For fitting or the plotter, we also include the factor for the 
C decay of gamma^* into a lepton pair
 	      CONST=CONST*pyalem(q_v**2)/3.0/pi/q_v/q_v

        ELSEIF(TYPE_V.EQ.'AA') THEN
C From notes 'How to do qqB -> gamma gamma...' pp1:
C Const = sigma_0 = 4/3 Pi^2 alpha
C With alpha = e^2/(4 Pi)
          CONST = 4.d0/3.d0*PI**2*pyalem(q_v**2)

        ELSEIF(TYPE_V.EQ.'ZZ') THEN
CsB________From Ohnemus, Owens PRD43 (91) 3627 (3): the overall factor is the
C          same for qQ->AA and qQ->ZZ, only g+ and g- differ.
CZL          CONST = 4.d0/3.d0*PI**2*pyalem(q_v**2)
          CONST = 4.d0/3.d0*PI**2*ACOUPL/(2d0*PI)

        ELSEIF(TYPE_V.EQ.'WW_UUB'.or.TYPE_V.EQ.'WW_DDB') THEN
          CONST = 4.d0/3.d0*PI**2*ACOUPL/(2d0*PI)
CZL          CONST = 4.d0/3.d0*PI**2*pyalem(q_v**2)

        ELSEIF(TYPE_V.EQ.'GL') THEN
C THIS IS THE \sigma_0 IN MY NOTES
           CONST=8.0/9.0*PI**3*ALPI(C2*S_Q)

        ELSEIF(TYPE_V.EQ.'AG') THEN
C THIS IS THE \sigma_0 IN MY NOTES
!ZL compensate running alpha_s for non-canonical choice
          Z_beta0=4*S_BETA1
          Z_beta1=34*3**2/3.d0-2*NF_EFF*(4/3.d0+5*3/3.d0)
           CONST=2*PI/8.0/8.0*PI**2*ALPI(C2*S_Q)**2
!     &     *(1+Z_beta0/2d0*ALPI(S_Q)*log(C2**2)+1d0/16d0*ALPI(S_Q)**2
!     &       *(Z_beta0**2*log(C2**2)**2-2*Z_beta1*log(C2**2)))

        ELSEIF(TYPE_V.EQ.'ZG') THEN
CsB The overall constant is the same as for GG -> AA
           CONST=2*PI/8.0/8.0*PI**2*ALPI(C2*S_Q)**2

        ELSEIF(TYPE_V.EQ.'GG') THEN
CSM
           CONST=PI**3/16.0*ALPI(C2*S_Q)
        ELSEif(TYPE_V.eq.'HJ') THEN
            KK = 1d0 + ALPI(S_Q)*11d0/4d0
            Kgg = 1d0/4d0/(3d0**2-1d0)**2
            tau = 4d0 * mt**2/mh**2
            GHJ2 = asin(1d0/sqrt(tau))**2
            GHJ2 = 1d0+(1d0-tau)*GHJ2
            GHJ2 = 4d0*dsqrt(2d0)*(ALPI(S_Q)/4d0)**2*GMU*tau**2*GHJ2**2
            dsigmadt=GHJ2*PI*ALPI(S_Q)/4d0*Kgg
            const = PI*dsigmadt/S_Q**4/QT_V**2
        ELSE
          PRINT*,' WRONG TYPE_V'
          CALL QUIT
        ENDIF
      ENDIF

      DUMB=.false.
      if(DUMB) then
      WRITE(nout,*) ' ECM,IBEAM,LEPASY,LTOPT,No_Sigma0'
      WRITE(nout,*) ECM,IBEAM,LEPASY,LTOPT,NO_SIGMA0
      WRITE(NOUT,*) ' ISET,INONPERT,IFLAG_C3,C1,C2,C3,C4'
      WRITE(NOUT,*) ISET,INONPERT,IFLAG_C3,C1,C2,C3,C4
      WRITE(NOUT,*) ' MT,MW,MZ,MH,MA'
      WRITE(NOUT,*) MT,MW,MZ,MH,MA
      WRITE(NOUT,*) ' TYPE_V'
      WRITE(NOUT,'(2X,A2)') TYPE_V
      WRITE(NOUT,*)' S_Q,S_LAM,S_BETA1,B0,A1,A2,B1,B2,NF_EFF,NORDER'
      WRITE(NOUT,*)S_Q,S_LAM,S_BETA1,B0,A1,A2,B1,B2,NF_EFF,NORDER
      WRITE(NOUT,*) ' Q_V'
      WRITE(NOUT,*) Q_V
      WRITE(NOUT,*) ' '
      endif

      nrun=1
      DO 900 IRUN=1,NRUN

cgal: ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cgal: enters the values of the parameters AFTER the setup routine

	qt_v=vpt
	y_v=rapin

cgal: ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

cgal:      READ(NINR,*) QT_V,Y_V
      IF(MOD(IRUN,5).EQ.0) THEN
       WRITE(*,*)QT_V,Y_V,IRUN
      ENDIF

      PERT=0.D0
      ASYMP=0.D0

      CALL YMAXIMUM
      IF(DABS(Y_V).GT.YMAX) THEN
C       WRITE(NWRT,*) 'ABS(Y_V)  > YMAX'
C       WRITE(NWRT,*)QT_V,Y_V,PERT,ASYMP,YMAX,NORDER
CsB       WRITE(NOUT,*) 'ABS(Y_V)  > YMAX'
CsB       WRITE(NOUT,920)QT_V,Y_V,PERT,ASYMP,YMAX,NORDER
       ier_pert=1
       GOTO 900
      elseIF(abs(abs(ymax)-ABS(Y_V)).LT.YMAX/100) THEN
c if rapidity is close to YMAX, then we are in a region where the program
c   is not accurate; we flag this situation
       ier_pert=2
      ENDIF

cpn Feb 2004       ACC_RERR=1.D-3
       ACC_RERR=1.D-4
C       ACC_RERR=1.D-5
       
       FIT3=.FALSE.

CsB___Definition of momentum fractions x1 and x2
      If (KinCorr.Eq.0) then
        X_A=Q_V/ECM*DEXP(Y_V)
        X_B=Q_V/ECM/DEXP(Y_V)
      Else If (KinCorr.Eq.1) then
        X_A=Sqrt(Q_V**2+QT_V**2)/ECM*DEXP(Y_V)
        X_B=Sqrt(Q_V**2+QT_V**2)/ECM/DEXP(Y_V)
      End If

CCPY      AMU=C4*Q_V
CBY Oct,2018
CJI MARCH 2019: Separate muF and muR
C setting the pert scale
        IF(iscale.EQ.0) THEN
           AMU=S_Q
        ELSE
           AMU=sqrt(S_Q**2+QT_V**2)
        ENDIF

      IF(LTOPT.EQ.-1) THEN ! LO piece
        CALL ASYMPTO(X_A,X_B,AMU,ASYMP)
      ELSE
C only change the scale of pert calculation
        CALL PERTURB(PERT,AMU)
        If (I_Pert.Eq.0 .or. I_pert.Eq.3 ) then ! L0 and A3 singular pieces
          CALL ASYMPTO(X_A,X_B,AMU,ASYMP)
        Else
          ASYMP = 0.d0
        End If
      ENDIF

      IF(DEBUG) THEN
       WRITE(NWRT,*) ' ACC_RERR =',ACC_RERR
       WRITE(NWRT,*) ' AMU,C4,Q_V,S_Q,C1,C2,C3'
       WRITE(NWRT,*) AMU,C4,Q_V,S_Q,C1,C2,C3
       IF(FIT3) THEN
        WRITE(NWRT,*) 'FIT3 = TRUE'
       ELSE
        WRITE(NWRT,*) 'FIT3 = FALSE'
       ENDIF
      ENDIF

C      WRITE(NWRT,*)'QT_V,Y_V,Q_V,PERT,ASYMP,YMAX,X_A,X_B,NORDER'
C      WRITE(NWRT,*)QT_V,Y_V,Q_V,PERT,ASYMP,YMAX,X_A,X_B,NORDER
C      WRITE(NWRT,*)' '

CsB      WRITE(NOUT,920)QT_V,Y_V,PERT,ASYMP,YMAX,NORDER
920   FORMAT(F7.2,2X,F7.2,2X,D16.4,2X,D16.4,2X,F7.2,2X,I3)
C      IF(ASK('MORE')) GOTO 10

900   CONTINUE

cgal: send back d(SIGMA)/d(Q^2)d(QT)d(Y)
      gal_pert=pert
      gal_asymp=asymp
      pma=pert-asymp

      if(HASJET.EQ.1) then
          gal_pert = gal_pert*x_a*x_b*ECM2
          gal_asymp = gal_asymp*x_a*x_b*ECM2
          pma = pma*x_a*x_b*ECM2
      endif

      END

C ==========================================================================
CsB   Calculation of the regular pieces
C --------------------------------------------------------------------------
      SUBROUTINE PERTURB(PERT,AMU)
C --------------------------------------------------------------------------
CsB This routine returns the regular pieces.
C   The main part of the calculation is done in X1X2Int
      IMPLICIT NONE
      INCLUDE 'common.for'

      REAL*8 S,T,U,TM_V,AMU_PERT
      COMMON/PERT/ S,T,U,TM_V,AMU_PERT
      REAL*8 X1LOW,X2LOW,X1MIN,X2MIN,UPLIM,AERR,RERR,ERREST
      INTEGER IER,IACTA,IACTB

      REAL*8 ADZINT,ADZ2NT
      EXTERNAL ADZINT,ADZ2NT
      LOGICAL DOX1,DOX2
      REAL*8 X1INT,X2INT
      EXTERNAL X1INT,X2INT
      REAL*8 X1INTEG,X2INTEG
      REAL*8 EXPY,RTAUP,PERT,AMU
      REAL*8 SMALL
      DATA SMALL/1.D-8/

CCPY: Oct  2008, add the corresponding PERT part for
C  Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
      IF (Type_V.Eq.'W+_RA_UDB' .or. Type_V.Eq.'W-_RA_DUB') THEN
        PERT=0.D0
        RETURN
      ENDIF


      AMU_PERT=AMU
      S=ECM2
      EXPY=DEXP(Y_V)
      TM_V=DSQRT(QT_V**2+Q_V**2)
      T=-TM_V*ECM/EXPY+Q_V**2
      U=-TM_V*ECM*EXPY+Q_V**2

      RTAUP=(TM_V+QT_V)/ECM
      X1LOW=RTAUP*EXPY
      X2LOW=RTAUP/EXPY

      UPLIM=1.0D0-SMALL
      X1MIN=-U/(S+T-Q_V**2)
      X2MIN=-T/(S+U-Q_V**2)

      IF(DEBUG) THEN
       WRITE(NWRT,*)'S,T,U,TM_V,EXPY,RTAUP,AMU'
       WRITE(NWRT,*)S,T,U,TM_V,EXPY,RTAUP,AMU
       WRITE(NWRT,*)'X1LOW,X1MIN,X2LOW,X2MIN'
       WRITE(NWRT,*)X1LOW,X1MIN,X2LOW,X2MIN
      ENDIF

      DOX1=.TRUE.
      DOX2=.TRUE.
      IF(X1LOW.GT.1.0) THEN
CCPY       WRITE(NWRT,*)' DOX1=FALSE'
       DOX1=.FALSE.
      ENDIF
      IF(X2LOW.GT.1.0) THEN
CCPY       WRITE(NWRT,*)' DOX2=FALSE'
       DOX2=.FALSE.
      ENDIF

      X1INTEG=0.
      X2INTEG=0.

      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

      IF(DOX1) THEN
       IF(DOX2) THEN
        X1INTEG=ADZINT(X1INT,X1LOW,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
        X2INTEG=ADZ2NT(X2INT,X2LOW,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       ELSE
        X1INTEG=ADZINT(X1INT,X1MIN,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       ENDIF
      ELSE
       IF(DOX2) THEN
        X2INTEG=ADZ2NT(X2INT,X2MIN,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       ELSE
        WRITE(NWRT,*)' ERROR IN PERTURB '
        WRITE(*,*)' >>> ERROR IN PERTURB. x1Low, x2Low = ', x1Low, x2Low
        CALL QUIT
       ENDIF
      ENDIF

      IF(DEBUG) THEN
       WRITE(NWRT,*)'X1INTEG,X2INTEG,CONST'
       WRITE(NWRT,*)X1INTEG,X2INTEG,CONST
      ENDIF

C THIS IS d(SIGMA)/d(Q^2)d(QT^2)d(Y)
      PERT=HBARC2*ALPI(muR*AMU)*PI*CONST*(X1INTEG+X2INTEG)
     >/PREV_UNDER/PREV_UNDER
C CONVERT TO d(SIGMA)/d(Q^2)d(QT)d(Y)
      PERT=PERT*2.0*QT_V

      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION X1INT(X1)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 X1INT,X1
      REAL*8 S,T,U,TM_V,AMU_PERT
      COMMON/PERT/ S,T,U,TM_V,AMU_PERT
      REAL*8 PLUMI
      REAL*8 X2,SH,TH,UH
      REAL*8 T1,AMU

      AMU=AMU_PERT
      T1=X1*S+U-Q_V**2
      X2=(-Q_V**2-X1*(T-Q_V**2))/T1
      SH=X1*X2*S
      TH=X1*(T-Q_V**2)+Q_V**2
      UH=X2*(U-Q_V**2)+Q_V**2
      CALL X1X2INT(X1,X2,SH,TH,UH,AMU,PLUMI)
      X1INT=PLUMI/T1

      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION X2INT(X2)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 X2INT,X2
      REAL*8 S,T,U,TM_V,AMU_PERT
      COMMON/PERT/ S,T,U,TM_V,AMU_PERT
      REAL*8 PLUMI
      REAL*8 X1,SH,TH,UH
      REAL*8 T2,AMU

      AMU=AMU_PERT
      T2=X2*S+T-Q_V**2
      X1=(-Q_V**2-X2*(U-Q_V**2))/T2
      SH=X1*X2*S
      TH=X1*(T-Q_V**2)+Q_V**2
      UH=X2*(U-Q_V**2)+Q_V**2
      CALL X1X2INT(X1,X2,SH,TH,UH,AMU,PLUMI)
      X2INT=PLUMI/T2

      RETURN
      END

C --------------------------------------------------------------------------
      SUBROUTINE X1X2INT_PP(X1,X2,SH,TH,UH,AMU_in,PLUMI)
C --------------------------------------------------------------------------
C
CsB This routine is for p pBar.
C
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 X1,X2,SH,TH,UH,AMU,PLUMI,amu_in
      REAL*8 PLUMI_1,PLUMI_2
      REAL*8 U_DB,DB_U,D_UB,UB_D,U_UB,D_DB,UB_U,DB_D
      REAL*8 U_WP_G,DB_WP_G,U_G_WP,DB_G_WP,
     >       D_WM_G,UB_WM_G,D_G_WM,UB_G_WM,
     >       G_Q_Z,G_QB_Z,Q_G_Z,QB_G_Z
c     >       G_Z0,Z0_G
      REAL*8 AMP2QQ,AMP2GQ
      EXTERNAL AMP2QQ,AMP2GQ
      REAL*8 G_G,G_Q,Q_G,Q_QB,QB_Q
      REAL*8 HAMP2GG,HAMP2GQ,HAMP2QQ
      EXTERNAL HAMP2GG,HAMP2GQ,HAMP2QQ
      INTEGER k
      REAL*8 APDFP1,APDFP2,APDFP3,APDFP4,APDFP5,
     >APDFM1,APDFM2,APDFM3,APDFM4,APDFM5
cc      Integer I_Proc
cc      Common / PartProc / I_Proc
CsB   I_Proc = 1 Calculate only QI QJ -->  G V process
C     I_Proc = 2 Calculate only  G QI --> QJ V process
      Real*8 Sig1,Sig2,Sig3,Sig4
      Real*8 C_BB,BB_C,C_G_HP,BB_G_HP,C_HP_G,BB_HP_G,
     ,       B_BB,BB_B,B_G_HP,B_HP_G

      amu=muF*amu_in

      IF(DEBUG) THEN
       WRITE(NWRT,*) ' IN X1X2INT_PP  AMU = ',AMU
      ENDIF
      PLUMI_1=0. ! Presetting QI QJ -->  G V contribution
      PLUMI_2=0. ! Presetting  G QI --> QJ V contribution

      If (I_Proc.Eq.2) Goto 200 ! Calculate only G QI --> QJ V process

C -----------------
C FOR QI QJ --> G V
C -----------------
      Sig1 = 1.d0
C I_Pert Assignments. I_Pert:XSect -> 0:L0, 1:A1, 2:(A2+A0), 3:2*A3, 4:A4.
      If (I_Pert.Eq.1 .or. I_Pert.Eq.3) Sig1 = -1.d0

C -----------------
      IF(TYPE_V.EQ.'W+' .OR. TYPE_V.EQ.'HP') THEN

CsB     u_D = km12 f_{u/p}(x1) f_{D/P}(x2)
C           + km13 f_{u/p}(x1) f_{S/P}(x2) + ...
C
C       and f_{D/P} = f_{d/p}, f_{S/P} = f_{s/p}, ...

       APDFP1=APDF(1,X1,AMU)
       APDFP4=APDF(4,X1,AMU)
       APDFP2=APDF(2,X2,AMU)
       APDFP3=APDF(3,X2,AMU)
       APDFP5=APDF(5,X2,AMU)

       U_DB=VKM(1,2)*APDFP1*APDFP2
     >     +VKM(1,3)*APDFP1*APDFP3
     >     +VKM(1,5)*APDFP1*APDFP5
     >     +VKM(4,2)*APDFP4*APDFP2
     >     +VKM(4,3)*APDFP4*APDFP3
     >     +VKM(4,5)*APDFP4*APDFP5

CsB    D_u = km12 f_{u/P}(x2) f_{D/p}(x1)
C          + km13 f_{u/P}(x2) f_{S/p}(x1) + ...
C
C      and f_{u/P} = f_{U/p}, ...

       APDFM1=APDF(-1,X2,AMU)
       APDFM4=APDF(-4,X2,AMU)
       APDFM2=APDF(-2,X1,AMU)
       APDFM3=APDF(-3,X1,AMU)
       APDFM5=APDF(-5,X1,AMU)

       DB_U=VKM(1,2)*APDFM1*APDFM2
     >     +VKM(1,3)*APDFM1*APDFM3
     >     +VKM(1,5)*APDFM1*APDFM5
     >     +VKM(4,2)*APDFM4*APDFM2
     >     +VKM(4,3)*APDFM4*APDFM3
     >     +VKM(4,5)*APDFM4*APDFM5

      PLUMI_1=1.d0/SH*(     U_DB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*DB_U*AMP2QQ(SH,UH,TH) )

C -----------------
      ELSEIF(TYPE_V.EQ.'W-' .OR. TYPE_V.EQ.'HM') THEN

CsB     d_U = km12 f_{U/P}(x2) f_{d/p}(x1)
C           + km13 f_{U/P}(x2) f_{s/p}(x1) + ...
C
C       and f_{U/P} = f_{u/p}, ...
C
C       Since f_{U/P} = f_{u/p} and f_{d/p} = f_{D/P}
C       after the full x1, x2 integral the contribution of d_U is equal to
C       the contribution of u_D at the W+ production.
C       This is why the total cross sections for W+ and W- production are the
C       same for pP collision.

       APDFP1=APDF(1,X2,AMU)
       APDFP4=APDF(4,X2,AMU)
       APDFP2=APDF(2,X1,AMU)
       APDFP3=APDF(3,X1,AMU)
       APDFP5=APDF(5,X1,AMU)

       D_UB=VKM(1,2)*APDFP1*APDFP2
     >     +VKM(1,3)*APDFP1*APDFP3
     >     +VKM(1,5)*APDFP1*APDFP5
     >     +VKM(4,2)*APDFP4*APDFP2
     >     +VKM(4,3)*APDFP4*APDFP3
     >     +VKM(4,5)*APDFP4*APDFP5

CsB     U_d = km12 f_{U/p}(x1) f_{d/P}(x2)
C           + km13 f_{U/p}(x1) f_{s/P}(x2) + ...
C
C       and f_{d/P} = f_{D/p}, f_{s/P} = f_{S/p}, ...
C
C       Since f_{U/p} = f_{u/P} and f_{d/P} = f_{D/p}
C       after the full x1, x2 integral the contribution of U_d is equal to
C       the contribution of D_u at the W+ production.
C       This is why the total cross sections for W+ and W- production are the
C       same for pP collision.

       APDFM1=APDF(-1,X1,AMU)
       APDFM4=APDF(-4,X1,AMU)
       APDFM2=APDF(-2,X2,AMU)
       APDFM3=APDF(-3,X2,AMU)
       APDFM5=APDF(-5,X2,AMU)

       UB_D=VKM(1,2)*APDFM1*APDFM2
     >     +VKM(1,3)*APDFM1*APDFM3
     >     +VKM(1,5)*APDFM1*APDFM5
     >     +VKM(4,2)*APDFM4*APDFM2
     >     +VKM(4,3)*APDFM4*APDFM3
     >     +VKM(4,5)*APDFM4*APDFM5

      PLUMI_1=1.d0/SH*(     D_UB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*UB_D*AMP2QQ(SH,UH,TH) )

C -----------------
      Else If (TYPE_V.EQ.'H+') then

CsB     c_B = f_{c/p}(x1) f_{B/P}(x2)
C
C       and f_{B/P} = f_{b/p}

       APDFP4=APDF(4,X1,AMU)
       APDFP5=APDF(5,X2,AMU)

       C_BB=APDFP4*APDFP5

CsB    B_c = f_{c/P}(x2) f_{B/p}(x1)
C
C      and f_{c/P} = f_{C/p}, ...

       APDFM4=APDF(-4,X2,AMU)
       APDFM5=APDF(-5,X1,AMU)

       BB_C=APDFM4*APDFM5

      PLUMI_1=1.d0/SH*(     C_BB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*BB_C*AMP2QQ(SH,UH,TH) )

C -----------------
      Else If (TYPE_V.EQ.'HB') then

CsB     b_B = f_{b/p}(x1) f_{B/P}(x2)
C
C       and f_{B/P} = f_{b/p}

       APDFP4=APDF(5,X1,AMU)
       APDFP5=APDF(5,X2,AMU)

       B_BB=APDFP4*APDFP5

CsB    B_b = f_{b/P}(x2) f_{B/p}(x1)
C
C      and f_{b/P} = f_{C/p}, ...

       APDFM4=APDF(-5,X2,AMU)
       APDFM5=APDF(-5,X1,AMU)

       BB_B=APDFM4*APDFM5

      PLUMI_1=1.d0/SH*(     B_BB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*BB_B*AMP2QQ(SH,UH,TH) )

C -----------------
      ELSE IF(TYPE_V.EQ.'Z0'.OR. TYPE_V.EQ.'A0' .OR.
     > TYPE_V.EQ.'AA' .OR. TYPE_V.EQ.'ZZ' .OR.
     > TYPE_V.EQ.'HZ' .OR. TYPE_V.EQ.'GL' ) THEN

       U_UB=APDF(1,X1,AMU)*APDF(1,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(4,X2,AMU)

       D_DB=APDF(2,X1,AMU)*APDF(2,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(5,X2,AMU)

       UB_U=APDF(-1,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(-4,X2,AMU)*APDF(-4,X1,AMU)

       DB_D=APDF(-2,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(-3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(-5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))


CZL
      ELSE IF(Type_V.Eq.'WW_UUB' .or. Type_V.Eq.'WW_DDB') THEN

       IF(Type_V.Eq.'WW_UUB') THEN

       U_UB=APDF(1,X1,AMU)*APDF(1,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(4,X2,AMU)

       UB_U=APDF(-1,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(-4,X2,AMU)*APDF(-4,X1,AMU)

       D_DB=0.d0
       DB_D=0.d0

       ELSEIF(Type_V.Eq.'WW_DDB') THEN

       D_DB=APDF(2,X1,AMU)*APDF(2,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(5,X2,AMU)

       DB_D=APDF(-2,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(-3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(-5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=0.d0
       UB_U=0.d0

       ENDIF

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

C -----------------
CCPY Oct 2008
CJI Sept 2013: Added in type ZU and ZD to match c++ version
      ELSE IF(Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
     > .or. Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD') THEN

        IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'ZU') THEN
       U_UB=APDF(1,X1,AMU)*APDF(1,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(4,X2,AMU)

       UB_U=APDF(-1,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(-4,X2,AMU)*APDF(-4,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=0.D0
       UB_U=UB_U*ZFF2(1)
       DB_D=0.D0

        ELSEIF( Type_V.Eq.'Z0_RA_DDB' .or. Type_V.Eq.'ZD') THEN

       D_DB=APDF(2,X1,AMU)*APDF(2,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(5,X2,AMU)

       DB_D=APDF(-2,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(-3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(-5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=0.D0
       D_DB=D_DB*ZFF2(2)
       UB_U=0.D0
       DB_D=DB_D*ZFF2(2)

	ENDIF

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))


C -----------------
      ELSE IF(TYPE_V.EQ.'H0' .OR. TYPE_V.EQ.'AG' .OR.
     >        TYPE_V.EQ.'ZG' .OR. TYPE_V.EQ.'GG')THEN
C FOR g g ---> H g

        G_G=APDF(0,X1,AMU)*APDF(0,X2,AMU)
        PLUMI_1=1.0/SH*G_G*HAMP2GG(SH,TH,UH)

      ENDIF

      If (I_Proc.Eq.1) Goto 999 ! Calculate only QI QJ --> G V process

 200  Continue
C -----------------
C FOR G QI --> QJ V
C -----------------
      Sig1 = 1.d0
      Sig2 = 1.d0
      Sig3 = 1.d0
      Sig4 = 1.d0
C I_Pert Assignments. I_Pert:Piece -> 0:L0, 1:A1, 2:(A2+A0), 3:2*A3, 4:A4.
      If (I_Pert.Eq.1) then
        Sig2 = -1.d0
        Sig4 = -1.d0
      Else If (I_Pert.Eq.3) then
        Sig1 = -1.d0
        Sig4 = -1.d0
      Else If (I_Pert.Eq.4) then
        Sig1 = -1.d0
        Sig2 = -1.d0
      End If

C -----------------
      IF(TYPE_V.EQ.'W+' .OR. TYPE_V.EQ.'HP') THEN
CsB
       U_G_WP=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(-4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       DB_G_WP=APDF(0,X1,AMU)*(
     > APDF(2,X2,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(3,X2,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )

       U_WP_G=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       DB_WP_G=APDF(0,X2,AMU)*(
     > APDF(-2,X1,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(-3,X1,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(-5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

C These signs correspond to the notes: How to do the Y piece pp6.
         PLUMI_2=1.d0/SH*
     >          ((Sig1*U_G_WP + Sig3*DB_G_WP)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*U_WP_G + Sig4*DB_WP_G)*AMP2GQ(SH,UH,TH))

C -----------------
      ELSEIF(TYPE_V.EQ.'W-'.OR. TYPE_V.EQ.'HM') THEN
CsB
       D_G_WM=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       UB_G_WM=APDF(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(-3,X2,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(-5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )

       UB_WM_G=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(-4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       D_WM_G=APDF(0,X2,AMU)*(
     > APDF(2,X1,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(3,X1,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

C These signs correspond to the notes: How to do the Y piece pp6.
         PLUMI_2=1.d0/SH*
     >          ((Sig1*UB_G_WM + Sig3*D_G_WM)*AMP2GQ(SH,TH,UH)+
     >           (Sig4*UB_WM_G + Sig2*D_WM_G)*AMP2GQ(SH,UH,TH))

C -----------------
      Else If (TYPE_V.EQ.'H+') then

       C_G_HP=APDF(0,X1,AMU)*APDF(-4,X2,AMU)

       BB_G_HP=APDF(0,X1,AMU)*APDF(5,X2,AMU)

       C_HP_G=APDF(0,X2,AMU)*APDF(4,X1,AMU)

       BB_HP_G=APDF(0,X2,AMU)*APDF(-5,X1,AMU)

C These signs correspond to the notes: How to do the Y piece pp6.
         PLUMI_2=1.d0/SH*
     >          ((Sig1*C_G_HP + Sig3*BB_G_HP)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*C_HP_G + Sig4*BB_HP_G)*AMP2GQ(SH,UH,TH))

C -----------------
      Else If (TYPE_V.EQ.'HB') then

       B_G_HP=APDF(0,X1,AMU)*APDF(-5,X2,AMU)

       BB_G_HP=APDF(0,X1,AMU)*APDF(5,X2,AMU)

       B_HP_G=APDF(0,X2,AMU)*APDF(5,X1,AMU)

       BB_HP_G=APDF(0,X2,AMU)*APDF(-5,X1,AMU)

C These signs correspond to the notes: How to do the Y piece pp6.
         PLUMI_2=1.d0/SH*
     >          ((Sig1*B_G_HP + Sig3*BB_G_HP)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*B_HP_G + Sig4*BB_HP_G)*AMP2GQ(SH,UH,TH))

C -----------------
      ELSE IF(TYPE_V.EQ.'Z0'.OR. TYPE_V.EQ.'A0' .OR.
     > TYPE_V.EQ.'AA' .OR. TYPE_V.EQ.'ZZ' .OR.
     > TYPE_V.EQ.'HZ' .OR. TYPE_V.EQ.'GL' ) THEN

       G_QB_Z=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4)
     >+APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_Q_Z=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4)
     >+APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4)
     >+APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4)
     >+APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

CZL
      ELSE IF(Type_V.Eq.'WW_UUB' .or. Type_V.Eq.'WW_DDB') THEN

       IF(Type_V.Eq.'WW_UUB') THEN

       G_QB_Z=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4) )

       G_Q_Z=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4) )

       ELSEIF(Type_V.Eq.'WW_DDB') THEN

       G_QB_Z=APDF(0,X1,AMU)*(
     >APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_Q_Z=APDF(0,X1,AMU)*(
     >APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     >APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     >APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

       ENDIF

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

C -----------------
CCPY Oct 2008
      ELSE IF(Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
     >     .or.Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD') THEN

        IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'ZU') THEN

       G_QB_Z=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4) )

       G_Q_Z=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4) )


        ELSEIF( Type_V.Eq.'Z0_RA_DDB' .or. Type_V.Eq.'ZD') THEN

       G_QB_Z=APDF(0,X1,AMU)*(
     >+APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_Q_Z=APDF(0,X1,AMU)*(
     >+APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     >+APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     >+APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

	ENDIF

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))


C -----------------
      ELSE IF(TYPE_V.EQ.'H0' .OR. TYPE_V.EQ.'AG' .OR.
     >        TYPE_V.EQ.'ZG' .OR. TYPE_V.EQ.'GG')THEN
C FOR g (q, qbar) ---> H (q, qbar)

       G_Q=0.D0
       Q_G=0.D0
       DO 20 K=-5,5
        IF(K.EQ.0) GOTO 20
        G_Q=G_Q+APDF(0,X1,AMU)*APDF(K,X2,AMU)
        Q_G=Q_G+APDF(K,X1,AMU)*APDF(0,X2,AMU)
20     CONTINUE

       PLUMI_2=1.0/SH*(G_Q*HAMP2GQ(SH,TH,UH)+Q_G*HAMP2GQ(SH,UH,TH))

C ALSO ADD IN q qbar --> H g

       Q_QB=0.D0
       QB_Q=0.D0
       DO 30 K=1,5
        Q_QB=Q_QB+APDF(K,X1,AMU)*APDF(-K,X2,AMU)
        QB_Q=QB_Q+APDF(-K,X1,AMU)*APDF(K,X2,AMU)
30     CONTINUE

       PLUMI_2=PLUMI_2+1.0/SH*(Q_QB+QB_Q)*HAMP2QQ(SH,TH,UH)
C -----------------
      ENDIF
999   CONTINUE

      PLUMI=PLUMI_1+PLUMI_2

      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION AMP2QQ(SH,TH,UH)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8  AMP2QQ,SH,TH,UH
      Real*8 Fact,MT_V, RP,RM, Var1,Var2

      RP(Var1,Var2) = (Q_V**2 - Var1)**2 + (Q_V**2 - Var2)**2
      RM(Var1,Var2) = (Q_V**2 - Var1)**2 - (Q_V**2 - Var2)**2

      AMP2QQ=((SH+UH)**2+(TH+SH)**2)/UH/TH ! = t/u + u/t + 2sQ^2/(ut)
      If (Type_V.Eq.'H+' .or. Type_V.Eq.'HB') Amp2qQ = Amp2qQ + 2.d0

CsB Generalization for different helicity cross sections
      MT_V = Sqrt(Q_V**2 + QT_V**2)
      Fact = 1.d0
C I_Pert Assignments. I_Pert:HelXSect -> 0:L0, 1:A1, 2:(A2+A0), 3:2*A3, 4:A4.
C     If (I_Pert.Eq.1) then
C       AMP2QQ = RP(TH,UH)/UH/TH
      If (I_Pert.Eq.1) then
        Fact = QT_V*Q_V/MT_V**2 * RM(UH,TH)/RP(TH,UH)
      Else If (I_Pert.Eq.2) then
        Fact = QT_V**2/MT_V**2
      Else If (I_Pert.Eq.3) then
        Fact = Q_V/MT_V
      Else If (I_Pert.Eq.4) then
        Fact = 2*QT_V/MT_V * RM(UH,TH)/RP(TH,UH)
      End if
      AMP2QQ=Fact*AMP2QQ
C THIS IS DUE TO THE FACTOR (2 \sigma_0 \over 3 PI) IN R_{QQ}
      AMP2QQ=AMP2QQ*2.0/3.0/PI
      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION AMP2GQ(SH,TH,UH)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8  AMP2GQ,SH,TH,UH
      Real*8 Fact,MT_V, RP,RM,Var1,Var2

      RP(Var1,Var2) = (Q_V**2 - Var1)**2 + (Q_V**2 - Var2)**2
c      RM(Var1,Var2) = (Q_V**2 - Var1)**2 - (Q_V**2 - Var2)**2

CCPY
C THE ASSIGNMENT OF TH AND UH ARE IMPORTANT TO GET RIGHT CANCELLATION
C BETWEEN PERTURBATIVE AND ASYMPTOTIC PIECES

      AMP2GQ=-((SH+TH)**2+(TH+UH)**2)/SH/UH ! = 
      If (Type_V.Eq.'H+' .or. Type_V.Eq.'HB') Amp2GQ = Amp2GQ - 2.d0

C THIS IS DUE TO THE FACTOR ( \sigma_0 \over 4 PI) IN R_{GQ}
      AMP2GQ=AMP2GQ/4.0/PI
CsB Generalization for different helicity cross sections
      MT_V = Sqrt(Q_V**2 + QT_V**2)
      Fact = 1.d0
C I_Pert Assignments. I_Pert:XSect -> 0:L0, 1:A1, 2:(A2+A0), 3:2*A3, 4:A4.
      If (I_Pert.Eq.1) then
        Fact = QT_V*Q_V/MT_V**2 *
     *         (2.d0*(SH+TH)**2-(SH+UH)**2)/RP(SH,UH)
      Else If (I_Pert.Eq.2) then
        Fact = QT_V**2/MT_V**2 * RP(-SH,UH)/RP(SH,UH)
      Else If (I_Pert.Eq.3) then
        Fact = Q_V/MT_V*( 1.d0 - 2.d0*UH*(Q_V**2-SH)/RP(SH,UH) )
      Else If (I_Pert.Eq.4) then
        Fact = 2.d0*QT_V/MT_V*(1.d0+(2.d0*SH*(Q_V**2-SH))/RP(SH,UH))
      End If

      AMP2GQ=Fact*AMP2GQ
      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION HAMP2GG(SH,TH,UH)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8  HAMP2GG,SH,TH,UH

      HAMP2GG=(Q_V**8+SH**4+TH**4+UH**4)/SH/TH/UH
C THIS IS DUE TO THE FACTOR (3 \sigma_0 Q_V^2 \over 2 PI) IN R_{GG}
      HAMP2GG=HAMP2GG*3.0/2.0/PI/Q_V**2

      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION HAMP2GQ(SH,TH,UH)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8  HAMP2GQ,SH,TH,UH

      HAMP2GQ=-(SH**2+UH**2)/TH
C THIS IS DUE TO THE FACTOR (4 \sigma_0 Q_V^2 \over 6 PI) IN R_{GQ}
      HAMP2GQ=HAMP2GQ*2.0/3.0/PI/Q_V**2

      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION HAMP2QQ(SH,TH,UH)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8  HAMP2QQ,SH,TH,UH

      HAMP2QQ=(TH**2+UH**2)/SH
C THIS IS DUE TO THE FACTOR (16 \sigma_0 Q_V^2 \over 9 PI) IN R_{QQ}
      HAMP2QQ=HAMP2QQ*16.0/9.0/PI/Q_V**2

      RETURN
      END

C ==========================================================================
      SUBROUTINE ASYMPTO(A_X_A,A_X_B,A_Q,ASYMP)
C --------------------------------------------------------------------------
CsB   Calculation of the leading order and asymptotic (QT singular) pieces 
C     for p-pBar collisions

      IMPLICIT NONE
      INCLUDE 'common.for'

      REAL*8 A_X_A,A_X_B,A_Q,ASYMP,ASYMP_SYM,ASYMP_ANT
      REAL*8 AMU
      REAL*8 A_SUD,DLOGQOPT2,A_ALPI
      COMMON/ASY_SPL/ A_SUD,DLOGQOPT2,A_ALPI
      REAL*8 U_DB,DB_U,D_UB,UB_D,U_UB,D_DB,UB_U,DB_D
      REAL*8 U_DB_2,DB_U_2,D_UB_2,UB_D_2,U_UB_2,D_DB_2,UB_U_2,DB_D_2
      REAL*8 SPLITSUD, HSPLITprime
      EXTERNAL SPLITSUD, HSPLITprime
      LOGICAL FIRST
      REAL*8 X1,X2,U_G_WP,DB_G_WP,U_WP_G,DB_WP_G,
     >D_G_WM,UB_G_WM,D_WM_G,UB_WM_G, G_Q_Z,G_QB_Z,Q_G_Z,QB_G_Z
      INTEGER k !ijump
      REAL*8 APDFP2,APDFP3,APDFP5,SPLITP1,SPLITP4,SPLITP5,
     >APDFM2,APDFM3,APDFM5,SPLITM1,SPLITM4,SPLITM5
      REAL*8 G_G,G_Q,Q_G
      Real*8 C_BB,BB_C,C_BB_2,BB_C_2,C_G_HP,CB_G_HP,C_HP_G,CB_HP_G,
     ,       B_BB,BB_B,B_BB_2,BB_B_2,B_G_HP,BB_G_HP,B_HP_G,BB_HP_G
CJI Jan 2015: Add in hj calculation
      Integer HasJet
      REAL*8 D1s, R, t, ptj
      Common /Jet/ D1s, R, t, ptj, HasJet
CsB   I_Proc = 1 Calculate only QI QJ -->  G V process
C     I_Proc = 2 Calculate only  G QI --> QJ V process
CJI Jan 2021: Include connonical values
      real*8 A1c,A2c,A3c,B1c,B2c,B3c,C1_IN,C2_IN,C3_IN
      common /canonical/ a1c,a2c,a3c,b1c,b2c,b3c,C1_IN,C2_IN,C3_IN

CMRENNA+++
      REAL*8 XMT2Q2,BETA,LOGXB
CMRENNA---
      DATA FIRST /.TRUE./

Cgal: no longer do we enforce lowest order QCD; technically wrong here, but
c      IF(NORDER.EQ.2) THEN
c       IF(FIRST) THEN
c        WRITE(NWRT,*) 'NORDER IS SET TO BE 1 IN MODULE ASYMPTO'
c        FIRST=.FALSE.
c       ENDIF
c      ENDIF

      AMU=muF*A_Q
      A_ALPI=ALPI(muR*a_Q)

CCPY May 2007: TO ENCUSRE CANCELLATION BETWEEEN ASYMPTO AND PERT IN LOW
C QT, WE ALWAYS USE THE CANONICAL PARAMETERS HERE.

      IF(LTOPT.EQ.0)THEN
CMRENNA+++
        IF(I_FSR.EQ.1) THEN
          IF(TYPE_V.EQ.'GG') THEN
C           B1=-2.*BETA1
           B1c=-2.*BETA1
          ELSE
CsB >>>
C           B1=-3.0D0/2.0D0*CF
           B1c=-3.0D0/2.0D0*CF
CCPY           B1 = CF*(-3.0D0/2.0D0 - 2.d0*DLog(C2*b0/C1))
CsB <<<
          ENDIF
          XMT2Q2=(MT/Q_V)**2
          BETA=SQRT(1.0D0-4.0D0*XMT2Q2)
          IF(BETA.GT.1.D-6) THEN
            LOGXB=LOG((1.0D0-BETA)/(1.0D0+BETA))/BETA
          ELSE
            LOGXB=-2.D0
          ENDIF
C          B1=B1+CF*(1.0D0+LOGXB*(1.0D0-2.D0*XMT2Q2))
          B1c=B1c+CF*(1.0D0+LOGXB*(1.0D0-2.D0*XMT2Q2))
        ENDIF
CMRENNA---
CsB >>>
!        A_SUD=A1*DLOG(A_Q**2/QT_V**2)+B1
CZL keep relation C1=b0*C2
C        A_SUD=A1*DLOG(Q_V**2/QT_V**2)+B1
        A_SUD=A1c*DLOG(Q_V**2/QT_V**2)+B1c
C        Print*, ' >>> ', DLog((C1/C2/b0)**2), DLOG(A_Q**2/QT_V**2)
CCPY        A_SUD=A1*(DLog((C1/C2/b0)**2)+DLOG(A_Q**2/QT_V**2))+
CCPY     +        B1
      ELSEIF(LTOPT.EQ.1)THEN
!        DLOGQOPT2=DLOG(A_Q**2/QT_V**2)
!ZL keep relation C1=b0*C2
        DLOGQOPT2=DLOG(Q_V**2/QT_V**2)
CCPY        DLOGQOPT2 = DLog((C1/C2/b0)**2)+DLOG(A_Q**2/QT_V**2)
C        A_SUD=A1/2.0*DLOGQOPT2+B1
        A_SUD=A1c/2.0*DLOGQOPT2+B1c
CsB <<<
        A_SUD=0.5*A_SUD
CCPY Nov 2019: Note the needed change to be made in A_SUD, for the new definition of "PXF=" 
        A_SUD=A_SUD*DLOGQOPT2

      ELSEIF(LTOPT.EQ.-1)THEN
        A_SUD=0.D0
      ENDIF

      IF(TYPE_V.EQ.'HJ') THEN
CCPY NOV 2019;          A_SUD = A_SUD + D1s*dlog(1d0/R**2)
CCPY NOV 2019; DOES THIS NEED ADDITIONAL DLOGQOPT2 FACTOR?
          A_SUD = A_SUD + 0.5*D1s*dlog(1d0/R**2)
      ENDIF

      IF(DEBUG) THEN
       WRITE(NWRT,*) ' IN ASYMPTO: AMU = ',AMU
      ENDIF

      If (I_Proc.Eq.2) Goto 200 ! Calculate only G QI --> QJ V process

CsB Definition of parton densities for different parton processes.
C -----------------
C FOR QI QJ --> G V
C -----------------
      IF(TYPE_V.EQ.'W+' .OR. TYPE_V.EQ.'HP'
     > .OR. Type_V.Eq.'W+_RA_UDB') THEN

C U_DB is meant to indicate that U is from proton, etc.

       APDFP2=APDF(2,A_X_B,AMU)
       APDFP3=APDF(3,A_X_B,AMU)
       APDFP5=APDF(5,A_X_B,AMU)
       SPLITP1=SPLITSUD(1,A_X_A,AMU)
       SPLITP4=SPLITSUD(4,A_X_A,AMU)

       U_DB=VKM(1,2)*SPLITP1*APDFP2
     >     +VKM(1,3)*SPLITP1*APDFP3
     >     +VKM(1,5)*SPLITP1*APDFP5
     >     +VKM(4,2)*SPLITP4*APDFP2
     >     +VKM(4,3)*SPLITP4*APDFP3
     >     +VKM(4,5)*SPLITP4*APDFP5

       APDFM2=APDF(-2,A_X_A,AMU)
       APDFM3=APDF(-3,A_X_A,AMU)
       APDFM5=APDF(-5,A_X_A,AMU)
       SPLITM1=SPLITSUD(-1,A_X_B,AMU)
       SPLITM4=SPLITSUD(-4,A_X_B,AMU)

       DB_U=VKM(1,2)*SPLITM1*APDFM2
     >     +VKM(1,3)*SPLITM1*APDFM3
     >     +VKM(1,5)*SPLITM1*APDFM5
     >     +VKM(4,2)*SPLITM4*APDFM2
     >     +VKM(4,3)*SPLITM4*APDFM3
     >     +VKM(4,5)*SPLITM4*APDFM5

       APDFP2=SPLITSUD(2,A_X_B,AMU)
       APDFP3=SPLITSUD(3,A_X_B,AMU)
       APDFP5=SPLITSUD(5,A_X_B,AMU)
       SPLITP1=APDF(1,A_X_A,AMU)
       SPLITP4=APDF(4,A_X_A,AMU)

       U_DB_2=VKM(1,2)*SPLITP1*APDFP2
     >     +VKM(1,3)*SPLITP1*APDFP3
     >     +VKM(1,5)*SPLITP1*APDFP5
     >     +VKM(4,2)*SPLITP4*APDFP2
     >     +VKM(4,3)*SPLITP4*APDFP3
     >     +VKM(4,5)*SPLITP4*APDFP5

       APDFM2=SPLITSUD(-2,A_X_A,AMU)
       APDFM3=SPLITSUD(-3,A_X_A,AMU)
       APDFM5=SPLITSUD(-5,A_X_A,AMU)
       SPLITM1=APDF(-1,A_X_B,AMU)
       SPLITM4=APDF(-4,A_X_B,AMU)

       DB_U_2=VKM(1,2)*SPLITM1*APDFM2
     >     +VKM(1,3)*SPLITM1*APDFM3
     >     +VKM(1,5)*SPLITM1*APDFM5
     >     +VKM(4,2)*SPLITM4*APDFM2
     >     +VKM(4,3)*SPLITM4*APDFM3
     >     +VKM(4,5)*SPLITM4*APDFM5
CsB
       If (LepAsy.Eq.0) then
         ASYMP=U_DB+DB_U+U_DB_2+DB_U_2
       Else If (LepAsy.Eq.1) then
         ASYMP=U_DB-DB_U+U_DB_2-DB_U_2
       End If

C -----------------
      ELSE IF(TYPE_V.EQ.'W-'.OR. TYPE_V.EQ.'HM'
     > .OR. Type_V.Eq.'W-_RA_DUB') THEN

       APDFP2=SPLITSUD(2,A_X_A,AMU)
       APDFP3=SPLITSUD(3,A_X_A,AMU)
       APDFP5=SPLITSUD(5,A_X_A,AMU)
       SPLITP1=APDF(1,A_X_B,AMU)
       SPLITP4=APDF(4,A_X_B,AMU)

       D_UB=VKM(1,2)*SPLITP1*APDFP2
     >     +VKM(1,3)*SPLITP1*APDFP3
     >     +VKM(1,5)*SPLITP1*APDFP5
     >     +VKM(4,2)*SPLITP4*APDFP2
     >     +VKM(4,3)*SPLITP4*APDFP3
     >     +VKM(4,5)*SPLITP4*APDFP5

       APDFM2=SPLITSUD(-2,A_X_B,AMU)
       APDFM3=SPLITSUD(-3,A_X_B,AMU)
       APDFM5=SPLITSUD(-5,A_X_B,AMU)
       SPLITM1=APDF(-1,A_X_A,AMU)
       SPLITM4=APDF(-4,A_X_A,AMU)

       UB_D=VKM(1,2)*SPLITM1*APDFM2
     >     +VKM(1,3)*SPLITM1*APDFM3
     >     +VKM(1,5)*SPLITM1*APDFM5
     >     +VKM(4,2)*SPLITM4*APDFM2
     >     +VKM(4,3)*SPLITM4*APDFM3
     >     +VKM(4,5)*SPLITM4*APDFM5

       APDFP2=APDF(2,A_X_A,AMU)
       APDFP3=APDF(3,A_X_A,AMU)
       APDFP5=APDF(5,A_X_A,AMU)
       SPLITP1=SPLITSUD(1,A_X_B,AMU)
       SPLITP4=SPLITSUD(4,A_X_B,AMU)

       D_UB_2=VKM(1,2)*SPLITP1*APDFP2
     >     +VKM(1,3)*SPLITP1*APDFP3
     >     +VKM(1,5)*SPLITP1*APDFP5
     >     +VKM(4,2)*SPLITP4*APDFP2
     >     +VKM(4,3)*SPLITP4*APDFP3
     >     +VKM(4,5)*SPLITP4*APDFP5

       APDFM2=APDF(-2,A_X_B,AMU)
       APDFM3=APDF(-3,A_X_B,AMU)
       APDFM5=APDF(-5,A_X_B,AMU)
       SPLITM1=SPLITSUD(-1,A_X_A,AMU)
       SPLITM4=SPLITSUD(-4,A_X_A,AMU)

       UB_D_2=VKM(1,2)*SPLITM1*APDFM2
     >     +VKM(1,3)*SPLITM1*APDFM3
     >     +VKM(1,5)*SPLITM1*APDFM5
     >     +VKM(4,2)*SPLITM4*APDFM2
     >     +VKM(4,3)*SPLITM4*APDFM3
     >     +VKM(4,5)*SPLITM4*APDFM5
CsB
       If (LepAsy.Eq.0) then
         ASYMP=D_UB+UB_D+D_UB_2+UB_D_2
       Else If (LepAsy.Eq.1) then
         ASYMP=D_UB-UB_D+D_UB_2-UB_D_2
       End If

C -----------------
      Else If (TYPE_V.EQ.'H+') then
C      C_BB is meant to indicate that C is from proton, etc.

       APDFP5=APDF(5,A_X_B,AMU)
       SPLITP4=SPLITSUD(4,A_X_A,AMU)

       C_BB=SPLITP4*APDFP5

       APDFM5=APDF(-5,A_X_A,AMU)
       SPLITM4=SPLITSUD(-4,A_X_B,AMU)

       BB_C=SPLITM4*APDFM5

       APDFP5=SPLITSUD(5,A_X_B,AMU)
       SPLITP4=APDF(4,A_X_A,AMU)

       C_BB_2=SPLITP4*APDFP5

       APDFM5=SPLITSUD(-5,A_X_A,AMU)
       SPLITM4=APDF(-4,A_X_B,AMU)

       BB_C_2=SPLITM4*APDFM5

       If (LepAsy.Eq.0) then
         ASYMP=C_BB+BB_C+C_BB_2+BB_C_2
       Else If (LepAsy.Eq.1) then
         ASYMP=C_BB-BB_C+C_BB_2-BB_C_2
       End If

C -----------------
      Else If (TYPE_V.EQ.'HB') then
C      C_BB is meant to indicate that C is from proton, etc.

       APDFP5=APDF(5,A_X_B,AMU)
       SPLITP5=SPLITSUD(5,A_X_A,AMU)

       B_BB=SPLITP5*APDFP5

       APDFM5=APDF(-5,A_X_A,AMU)
       SPLITM5=SPLITSUD(-5,A_X_B,AMU)

       BB_B=SPLITM5*APDFM5

       APDFP5=SPLITSUD(5,A_X_B,AMU)
       SPLITP5=APDF(5,A_X_A,AMU)

       B_BB_2=SPLITP5*APDFP5

       APDFM5=SPLITSUD(-5,A_X_A,AMU)
       SPLITM5=APDF(-5,A_X_B,AMU)

       BB_B_2=SPLITM5*APDFM5

       If (LepAsy.Eq.0) then
         ASYMP=B_BB+BB_B+B_BB_2+BB_B_2
       Else If (LepAsy.Eq.1) then
         ASYMP=B_BB-BB_B+B_BB_2-BB_B_2
       End If

C -----------------
      ELSE IF(TYPE_V.EQ.'Z0'.OR. TYPE_V.EQ.'A0' .OR.
     > Type_V.Eq.'AA' .Or. Type_V.Eq.'ZZ' .OR.
     > TYPE_V.EQ.'HZ' .OR. TYPE_V.EQ.'GL' ) THEN

       U_UB=SPLITSUD(1,A_X_A,AMU)*APDF(1,A_X_B,AMU)
     >     +SPLITSUD(4,A_X_A,AMU)*APDF(4,A_X_B,AMU)

       D_DB=SPLITSUD(2,A_X_A,AMU)*APDF(2,A_X_B,AMU)
     >     +SPLITSUD(3,A_X_A,AMU)*APDF(3,A_X_B,AMU)
     >     +SPLITSUD(5,A_X_A,AMU)*APDF(5,A_X_B,AMU)

       UB_U=SPLITSUD(-1,A_X_B,AMU)*APDF(-1,A_X_A,AMU)
     >     +SPLITSUD(-4,A_X_B,AMU)*APDF(-4,A_X_A,AMU)

       DB_D=SPLITSUD(-2,A_X_B,AMU)*APDF(-2,A_X_A,AMU)
     >     +SPLITSUD(-3,A_X_B,AMU)*APDF(-3,A_X_A,AMU)
     >     +SPLITSUD(-5,A_X_B,AMU)*APDF(-5,A_X_A,AMU)

       U_UB_2=APDF(1,A_X_A,AMU)*SPLITSUD(1,A_X_B,AMU)
     >     +APDF(4,A_X_A,AMU)*SPLITSUD(4,A_X_B,AMU)

       D_DB_2=APDF(2,A_X_A,AMU)*SPLITSUD(2,A_X_B,AMU)
     >     +APDF(3,A_X_A,AMU)*SPLITSUD(3,A_X_B,AMU)
     >     +APDF(5,A_X_A,AMU)*SPLITSUD(5,A_X_B,AMU)

       UB_U_2=APDF(-1,A_X_B,AMU)*SPLITSUD(-1,A_X_A,AMU)
     >     +APDF(-4,A_X_B,AMU)*SPLITSUD(-4,A_X_A,AMU)

       DB_D_2=APDF(-2,A_X_B,AMU)*SPLITSUD(-2,A_X_A,AMU)
     >     +APDF(-3,A_X_B,AMU)*SPLITSUD(-3,A_X_A,AMU)
     >     +APDF(-5,A_X_B,AMU)*SPLITSUD(-5,A_X_A,AMU)

CsB
       If (LepAsy.Eq.0) then
         ASYMP=(U_UB+U_UB_2+UB_U+UB_U_2)*ZFF2(1)
     >        +(D_DB+D_DB_2+DB_D+DB_D_2)*ZFF2(2)
       Else If (LepAsy.Eq.1) then
         ASYMP=(U_UB+U_UB_2-UB_U-UB_U_2)*ZFF2(1)
     >        +(D_DB+D_DB_2-DB_D-DB_D_2)*ZFF2(2)
       End If
CZL
      ELSE IF(Type_V.Eq.'WW_UUB' .or. Type_V.Eq.'WW_DDB') THEN

       IF(Type_V.Eq.'WW_UUB') THEN
       U_UB=SPLITSUD(1,A_X_A,AMU)*APDF(1,A_X_B,AMU)
     >     +SPLITSUD(4,A_X_A,AMU)*APDF(4,A_X_B,AMU)

       UB_U=SPLITSUD(-1,A_X_B,AMU)*APDF(-1,A_X_A,AMU)
     >     +SPLITSUD(-4,A_X_B,AMU)*APDF(-4,A_X_A,AMU)

       U_UB_2=APDF(1,A_X_A,AMU)*SPLITSUD(1,A_X_B,AMU)
     >     +APDF(4,A_X_A,AMU)*SPLITSUD(4,A_X_B,AMU)

       UB_U_2=APDF(-1,A_X_B,AMU)*SPLITSUD(-1,A_X_A,AMU)
     >     +APDF(-4,A_X_B,AMU)*SPLITSUD(-4,A_X_A,AMU)

       D_DB=0.d0
       DB_D=0.d0
       D_DB_2=0.d0
       DB_D_2=0.d0
CZL
       ELSEIF(Type_V.Eq.'WW_DDB') THEN
       D_DB=SPLITSUD(2,A_X_A,AMU)*APDF(2,A_X_B,AMU)
     >     +SPLITSUD(3,A_X_A,AMU)*APDF(3,A_X_B,AMU)
     >     +SPLITSUD(5,A_X_A,AMU)*APDF(5,A_X_B,AMU)

       DB_D=SPLITSUD(-2,A_X_B,AMU)*APDF(-2,A_X_A,AMU)
     >     +SPLITSUD(-3,A_X_B,AMU)*APDF(-3,A_X_A,AMU)
     >     +SPLITSUD(-5,A_X_B,AMU)*APDF(-5,A_X_A,AMU)

       D_DB_2=APDF(2,A_X_A,AMU)*SPLITSUD(2,A_X_B,AMU)
     >     +APDF(3,A_X_A,AMU)*SPLITSUD(3,A_X_B,AMU)
     >     +APDF(5,A_X_A,AMU)*SPLITSUD(5,A_X_B,AMU)

       DB_D_2=APDF(-2,A_X_B,AMU)*SPLITSUD(-2,A_X_A,AMU)
     >     +APDF(-3,A_X_B,AMU)*SPLITSUD(-3,A_X_A,AMU)
     >     +APDF(-5,A_X_B,AMU)*SPLITSUD(-5,A_X_A,AMU)

       U_UB=0.d0
       UB_U=0.d0
       U_UB_2=0.d0
       UB_U_2=0.d0

       ENDIF

CsB
       If (LepAsy.Eq.0) then
         ASYMP=(U_UB+U_UB_2+UB_U+UB_U_2)*ZFF2(1)
     >        +(D_DB+D_DB_2+DB_D+DB_D_2)*ZFF2(2)
       Else If (LepAsy.Eq.1) then
         ASYMP=(U_UB+U_UB_2-UB_U-UB_U_2)*ZFF2(1)
     >        +(D_DB+D_DB_2-DB_D-DB_D_2)*ZFF2(2)
       End If

C -----------------
CCPY
CJI September 2013: added in new Z types to match c++ code
      ELSE IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'  
     >   .or. Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD' ) THEN

CJI September 2013: added in new ZU to match c++ code
        IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'ZU') THEN

       U_UB=SPLITSUD(1,A_X_A,AMU)*APDF(1,A_X_B,AMU)
     >     +SPLITSUD(4,A_X_A,AMU)*APDF(4,A_X_B,AMU)

       UB_U=SPLITSUD(-1,A_X_B,AMU)*APDF(-1,A_X_A,AMU)
     >     +SPLITSUD(-4,A_X_B,AMU)*APDF(-4,A_X_A,AMU)

       U_UB_2=APDF(1,A_X_A,AMU)*SPLITSUD(1,A_X_B,AMU)
     >     +APDF(4,A_X_A,AMU)*SPLITSUD(4,A_X_B,AMU)

       UB_U_2=APDF(-1,A_X_B,AMU)*SPLITSUD(-1,A_X_A,AMU)
     >     +APDF(-4,A_X_B,AMU)*SPLITSUD(-4,A_X_A,AMU)

       D_DB=0.D0
       DB_D=0.D0
       D_DB_2=0.D0
       DB_D_2=0.D0
       
CJI September 2013: added in new ZD to match c++ code
        ELSEIF( Type_V.Eq.'Z0_RA_DDB' .or. Type_V.Eq.'ZD') THEN

       D_DB=SPLITSUD(2,A_X_A,AMU)*APDF(2,A_X_B,AMU)
     >     +SPLITSUD(3,A_X_A,AMU)*APDF(3,A_X_B,AMU)
     >     +SPLITSUD(5,A_X_A,AMU)*APDF(5,A_X_B,AMU)

       DB_D=SPLITSUD(-2,A_X_B,AMU)*APDF(-2,A_X_A,AMU)
     >     +SPLITSUD(-3,A_X_B,AMU)*APDF(-3,A_X_A,AMU)
     >     +SPLITSUD(-5,A_X_B,AMU)*APDF(-5,A_X_A,AMU)

       D_DB_2=APDF(2,A_X_A,AMU)*SPLITSUD(2,A_X_B,AMU)
     >     +APDF(3,A_X_A,AMU)*SPLITSUD(3,A_X_B,AMU)
     >     +APDF(5,A_X_A,AMU)*SPLITSUD(5,A_X_B,AMU)

       DB_D_2=APDF(-2,A_X_B,AMU)*SPLITSUD(-2,A_X_A,AMU)
     >     +APDF(-3,A_X_B,AMU)*SPLITSUD(-3,A_X_A,AMU)
     >     +APDF(-5,A_X_B,AMU)*SPLITSUD(-5,A_X_A,AMU)

       U_UB=0.D0
       UB_U=0.D0
       U_UB_2=0.D0
       UB_U_2=0.D0
       
        ENDIF
CsB
       If (LepAsy.Eq.0) then
         ASYMP=(U_UB+U_UB_2+UB_U+UB_U_2)*ZFF2(1)
     >        +(D_DB+D_DB_2+DB_D+DB_D_2)*ZFF2(2)
       Else If (LepAsy.Eq.1) then
         ASYMP=(U_UB+U_UB_2-UB_U-UB_U_2)*ZFF2(1)
     >        +(D_DB+D_DB_2-DB_D-DB_D_2)*ZFF2(2)
       End If

C -----------------
      ELSE IF(TYPE_V.EQ.'H0' .OR. TYPE_V.EQ.'AG' .OR.
     >        TYPE_V.EQ.'ZG' .OR. TYPE_V.EQ.'GG')THEN
C FOR g g --> H g

       G_G=SPLITSUD(0,A_X_A,AMU)*APDF(0,A_X_B,AMU)
     >    +APDF(0,A_X_A,AMU)*SPLITSUD(0,A_X_B,AMU)

CZL: calculate the spin-flip term for gg->\gamma\gamma subprocess
       if(TYPE_V.EQ.'AG'.and.LEPASY.EQ.1) then
         G_G=HSPLITprime(0,A_X_A,AMU)*APDF(0,A_X_B,AMU)
     >    +APDF(0,A_X_A,AMU)*HSPLITprime(0,A_X_B,AMU)
       endif

C IDENTICAL PARTICLE
       ASYMP=G_G

CJI Added Higgs + 1 jet
      Elseif(type_v.eq.'hj')then
        G_G = SPLITSUD(0,A_X_A,AMU)*APDF(0,A_X_B,AMU)
     >      + APDF(0,A_X_A,AMU)*SPLITSUD(0,A_X_B,AMU)

      ASYMP = G_G

      Else
        Print*, ' [Asympt] No such a Type_V: ', Type_V
      ENDIF

CCPY FOR LEADING ORDER
      IF(LTOPT.EQ.-1) GOTO 999

      If (I_Proc.Eq.1) Goto 999 ! Calculate only QI QJ --> G V process
 200  Continue
C -----------------
C FOR G QI --> QJ V
C -----------------
      X1=A_X_A
      X2=A_X_B

      IF(TYPE_V.EQ.'W+'.OR. TYPE_V.EQ.'HP'
     > .OR. Type_V.Eq.'W+_RA_UDB') THEN

CsB
C DB_G_WP is meant to indicate that DB is from proton, etc.
       DB_G_WP=SPLITSUD(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(-4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       U_G_WP=SPLITSUD(0,X1,AMU)*(
     > APDF(2,X2,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(3,X2,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )

       U_WP_G=SPLITSUD(0,X2,AMU)*(
     > APDF(1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       DB_WP_G=SPLITSUD(0,X2,AMU)*(
     > APDF(-2,X1,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(-3,X1,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(-5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

c       G_WP=SPLITSUD(0,X1,AMU)*(
c     > APDF(-1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(-4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(2,X2,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(3,X2,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )
c
c       WP_G=SPLITSUD(0,X2,AMU)*(
c     > APDF(1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(-2,X1,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(-3,X1,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(-5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

       IF(LEPASY.EQ.0) THEN
        ASYMP=ASYMP+U_G_WP+U_WP_G+DB_G_WP+DB_WP_G
       ELSE If (LepAsy.Eq.1) then
        ASYMP=ASYMP+U_G_WP+U_WP_G-DB_G_WP-DB_WP_G
       ENDIF

c        ASYMP=ASYMP+G_WP+WP_G

C -----------------
      ELSE IF(TYPE_V.EQ.'W-'.OR. TYPE_V.EQ.'HM'
     > .OR. Type_V.Eq.'W-_RA_DUB') THEN

CsB
       D_G_WM=SPLITSUD(0,X1,AMU)*(
     > APDF(1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       UB_G_WM=SPLITSUD(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(-3,X2,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(-5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )

       UB_WM_G=SPLITSUD(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(-4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       D_WM_G=SPLITSUD(0,X2,AMU)*(
     > APDF(2,X1,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(3,X1,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

c       G_WM=SPLITSUD(0,X1,AMU)*(
c     > APDF(1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(-2,X2,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(-3,X2,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(-5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )
c
c       WM_G=SPLITSUD(0,X2,AMU)*(
c     > APDF(-1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(-4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(2,X1,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(3,X1,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

       IF(LEPASY.EQ.0) THEN
        ASYMP=ASYMP+D_G_WM+D_WM_G+UB_G_WM+UB_WM_G
       ELSE If (LepAsy.Eq.1) then
        ASYMP=ASYMP+D_G_WM+D_WM_G-UB_G_WM-UB_WM_G
       ENDIF

c        ASYMP=ASYMP+G_WM+WM_G

C -----------------
      Else If (TYPE_V.EQ.'H+') then
C       CB_G_HP is meant to indicate that CB is from proton, etc.
        CB_G_HP=SPLITSUD(0,X1,AMU)*APDF(-4,X2,AMU)

        B_G_HP=SPLITSUD(0,X1,AMU)*APDF(5,X2,AMU)

        C_HP_G=SPLITSUD(0,X2,AMU)*APDF(4,X1,AMU)

        BB_HP_G=SPLITSUD(0,X2,AMU)*APDF(-5,X1,AMU)

        IF(LEPASY.EQ.0) THEN
          ASYMP=ASYMP+B_G_HP+C_HP_G+CB_G_HP+BB_HP_G
        ELSE If (LepAsy.Eq.1) then
          ASYMP=ASYMP+B_G_HP+C_HP_G-CB_G_HP-BB_HP_G
        ENDIF

C -----------------
      Else If (TYPE_V.EQ.'HB') then
C       BB_G_HP is meant to indicate that DB is from proton, etc.
        BB_G_HP=SPLITSUD(0,X1,AMU)*APDF(-5,X2,AMU)

        B_G_HP=SPLITSUD(0,X1,AMU)*APDF(5,X2,AMU)

        B_HP_G=SPLITSUD(0,X2,AMU)*APDF(5,X1,AMU)

        BB_HP_G=SPLITSUD(0,X2,AMU)*APDF(-5,X1,AMU)

        IF(LEPASY.EQ.0) THEN
          ASYMP=ASYMP+B_G_HP+B_HP_G+BB_G_HP+BB_HP_G
        ELSE If (LepAsy.Eq.1) then
          ASYMP=ASYMP+B_G_HP+B_HP_G-BB_G_HP-BB_HP_G
        ENDIF

C -----------------
      ELSE IF(TYPE_V.EQ.'Z0'.OR. TYPE_V.EQ.'A0' .OR.
     > Type_V.Eq.'AA' .Or. Type_V.Eq.'ZZ' .OR.
     > TYPE_V.EQ.'HZ' .OR. TYPE_V.EQ.'GL' ) THEN

c       G_Z0=SPLITSUD(0,X1,AMU)*(
c     > (APDF(1,X2,AMU)+APDF(-1,X2,AMU))*ZFF2(1)
c     >+(APDF(4,X2,AMU)+APDF(-4,X2,AMU))*ZFF2(4)
c     >+(APDF(2,X2,AMU)+APDF(-2,X2,AMU))*ZFF2(2)
c     >+(APDF(3,X2,AMU)+APDF(-3,X2,AMU))*ZFF2(3)
c     >+(APDF(5,X2,AMU)+APDF(-5,X2,AMU))*ZFF2(5) )
c
c       Z0_G=SPLITSUD(0,X2,AMU)*(
c     > (APDF(1,X1,AMU)+APDF(-1,X1,AMU))*ZFF2(1)
c     >+(APDF(4,X1,AMU)+APDF(-4,X1,AMU))*ZFF2(4)
c     >+(APDF(2,X1,AMU)+APDF(-2,X1,AMU))*ZFF2(2)
c     >+(APDF(3,X1,AMU)+APDF(-3,X1,AMU))*ZFF2(3)
c     >+(APDF(5,X1,AMU)+APDF(-5,X1,AMU))*ZFF2(5) )
c
c        ASYMP=ASYMP+G_Z0+Z0_G
CsB
       G_QB_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(1,X2,AMU))*ZFF2(1)+(APDF(4,X2,AMU))*ZFF2(4)
     >+(APDF(2,X2,AMU))*ZFF2(2)+(APDF(3,X2,AMU))*ZFF2(3)
     >+(APDF(5,X2,AMU))*ZFF2(5) )

       G_Q_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(-1,X2,AMU))*ZFF2(1)+(APDF(-4,X2,AMU))*ZFF2(4)
     >+(APDF(-2,X2,AMU))*ZFF2(2)+(APDF(-3,X2,AMU))*ZFF2(3)
     >+(APDF(-5,X2,AMU))*ZFF2(5) )

       Q_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(1,X1,AMU))*ZFF2(1)+(APDF(4,X1,AMU))*ZFF2(4)
     >+(APDF(2,X1,AMU))*ZFF2(2)+(APDF(3,X1,AMU))*ZFF2(3)
     >+(APDF(5,X1,AMU))*ZFF2(5) )

       QB_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(-1,X1,AMU))*ZFF2(1)+(APDF(-4,X1,AMU))*ZFF2(4)
     >+(APDF(-2,X1,AMU))*ZFF2(2)+(APDF(-3,X1,AMU))*ZFF2(3)
     >+(APDF(-5,X1,AMU))*ZFF2(5) )

       If (LepAsy.Eq.0) then
         ASYMP = ASYMP + G_Q_Z + G_QB_Z + Q_G_Z + QB_G_Z
       Else If (LepAsy.Eq.1) then
         ASYMP = ASYMP - G_Q_Z + G_QB_Z + Q_G_Z - QB_G_Z
       EndIf

CZL
      ELSE IF(Type_V.Eq.'WW_UUB' .or. Type_V.Eq.'WW_DDB') THEN

       IF(Type_V.Eq.'WW_UUB') THEN

       G_QB_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(1,X2,AMU))*ZFF2(1)+(APDF(4,X2,AMU))*ZFF2(4) )

       G_Q_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(-1,X2,AMU))*ZFF2(1)+(APDF(-4,X2,AMU))*ZFF2(4) )

       Q_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(1,X1,AMU))*ZFF2(1)+(APDF(4,X1,AMU))*ZFF2(4) )

       QB_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(-1,X1,AMU))*ZFF2(1)+(APDF(-4,X1,AMU))*ZFF2(4) )

       ELSEIF(Type_V.Eq.'WW_DDB') THEN

       G_QB_Z = SPLITSUD(0,X1,AMU)*(
     >(APDF(2,X2,AMU))*ZFF2(2)+(APDF(3,X2,AMU))*ZFF2(3)
     >+(APDF(5,X2,AMU))*ZFF2(5) )

       G_Q_Z = SPLITSUD(0,X1,AMU)*(
     >(APDF(-2,X2,AMU))*ZFF2(2)+(APDF(-3,X2,AMU))*ZFF2(3)
     >+(APDF(-5,X2,AMU))*ZFF2(5) )

       Q_G_Z = SPLITSUD(0,X2,AMU)*(
     >(APDF(2,X1,AMU))*ZFF2(2)+(APDF(3,X1,AMU))*ZFF2(3)
     >+(APDF(5,X1,AMU))*ZFF2(5) )

       QB_G_Z = SPLITSUD(0,X2,AMU)*(
     >(APDF(-2,X1,AMU))*ZFF2(2)+(APDF(-3,X1,AMU))*ZFF2(3)
     >+(APDF(-5,X1,AMU))*ZFF2(5) )

       ENDIF

       If (LepAsy.Eq.0) then
         ASYMP = ASYMP + G_Q_Z + G_QB_Z + Q_G_Z + QB_G_Z
       Else If (LepAsy.Eq.1) then
         ASYMP = ASYMP - G_Q_Z + G_QB_Z + Q_G_Z - QB_G_Z
       EndIf

C -----------------
CCPY

CJI September 2013: Added in new Z to match c++ code
      ELSE IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
     > .or. Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD' ) THEN 

CJI September 2013: Added in ZU to match c++ code
        IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'ZU') THEN

       G_QB_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(1,X2,AMU))*ZFF2(1)+(APDF(4,X2,AMU))*ZFF2(4) )

       G_Q_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(-1,X2,AMU))*ZFF2(1)+(APDF(-4,X2,AMU))*ZFF2(4) )

       Q_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(1,X1,AMU))*ZFF2(1)+(APDF(4,X1,AMU))*ZFF2(4) )

       QB_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(-1,X1,AMU))*ZFF2(1)+(APDF(-4,X1,AMU))*ZFF2(4) )

CJI September 2013: Added in ZD to match c++ code
        ELSEIF( Type_V.Eq.'Z0_RA_DDB' .or. Type_V.Eq.'ZD') THEN

       G_QB_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(2,X2,AMU))*ZFF2(2)+(APDF(3,X2,AMU))*ZFF2(3)
     >+(APDF(5,X2,AMU))*ZFF2(5) )

       G_Q_Z = SPLITSUD(0,X1,AMU)*(
     > (APDF(-2,X2,AMU))*ZFF2(2)+(APDF(-3,X2,AMU))*ZFF2(3)
     >+(APDF(-5,X2,AMU))*ZFF2(5) )

       Q_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(2,X1,AMU))*ZFF2(2)+(APDF(3,X1,AMU))*ZFF2(3)
     >+(APDF(5,X1,AMU))*ZFF2(5) )

       QB_G_Z = SPLITSUD(0,X2,AMU)*(
     > (APDF(-2,X1,AMU))*ZFF2(2)+(APDF(-3,X1,AMU))*ZFF2(3)
     >+(APDF(-5,X1,AMU))*ZFF2(5) )
       
        ENDIF

       If (LepAsy.Eq.0) then
         ASYMP = ASYMP + G_Q_Z + G_QB_Z + Q_G_Z + QB_G_Z
       Else If (LepAsy.Eq.1) then
         ASYMP = ASYMP - G_Q_Z + G_QB_Z + Q_G_Z - QB_G_Z
       EndIf

C -----------------
      ELSE IF(TYPE_V.EQ.'H0' .OR. TYPE_V.EQ.'AG' .OR.
     >        TYPE_V.EQ.'ZG' .OR. TYPE_V.EQ.'GG')THEN
C FOR q g ---> H g

       G_Q=0.D0
       Q_G=0.D0
       DO 10 K=-5,5
        IF(K.EQ.0) GOTO 10
        G_Q=G_Q+APDF(0,X1,AMU)*SPLITSUD(K,X2,AMU)
        Q_G=Q_G+SPLITSUD(K,X1,AMU)*APDF(0,X2,AMU)
10     CONTINUE

       if(TYPE_V.EQ.'AG'.and.LEPASY.EQ.1) THEN
CZL: calculate the spin-flip term for gg->\gamma\gamma subprocess
         ASYMP=ASYMP+0
       ELSE
CZL: the normal term
         ASYMP=ASYMP+G_Q+Q_G
       ENDIF

      Else
        Print*, ' [Asympt] No such a Type_V: ', Type_V
      ENDIF

999   CONTINUE

C -----------------
CsB   LEPASY IS BUILT IN 5/12/95
      ASYMP_SYM=ASYMP
      ASYMP_ANT=ASYMP

CCPY SET LTOPT=0 FOR CALCULATING THE ASYMPTOTIC PART
C    SET LTOPT=1 FOR CALCULATING THE DELTA_SIGMA FROM QT=0 TO PT
C    SET LTOPT=-1 FOR CALCULATING THE LEADING ORDER RESULT

      IF(LTOPT.EQ.0) THEN ! FOR ASYMPTOTIC PART
C       THIS IS d(SIGMA)/d(Q^2)d(QT^2)d(Y)
        ASYMP_SYM=HBARC2*CONST/ECM2*ASYMP_SYM*A_ALPI/(2.0*QT_V**2)
     >        /PREV_UNDER/PREV_UNDER
        ASYMP_ANT=HBARC2*CONST/ECM2*ASYMP_ANT*A_ALPI/(2.0*QT_V**2)
     >        /PREV_UNDER/PREV_UNDER
C       CONVERT TO d(SIGMA)/d(Q^2)d(QT)d(Y)
        ASYMP_SYM=ASYMP_SYM*2.0*QT_V
        ASYMP_ANT=ASYMP_ANT*2.0*QT_V

      ELSEIF(LTOPT.EQ.1) THEN ! FOR DELTA_SIGMA
C       THIS IS d(SIGMA)/d(Q^2)d(Y)  AFTER INTEGRATING QT^2 FROM 0 TO PT^2
        ASYMP_SYM=HBARC2*CONST/ECM2*ASYMP_SYM
     >        /PREV_UNDER/PREV_UNDER
        ASYMP_ANT=HBARC2*CONST/ECM2*ASYMP_ANT
     >        /PREV_UNDER/PREV_UNDER

      ELSEIF(LTOPT.EQ.-1) THEN ! CALCULATING THE LEADING ORDER RESULT
        ASYMP_SYM=HBARC2*CONST/ECM2*ASYMP_SYM
     >        /PREV_UNDER/PREV_UNDER
        ASYMP_ANT=HBARC2*CONST/ECM2*ASYMP_ANT
     >        /PREV_UNDER/PREV_UNDER
!        Print*, '[ASYMPTO] CONST = ', CONST
      ENDIF

      If (LepAsy.Eq.0) then
        ASYMP=ASYMP_SYM
      Else If (LepAsy.Eq.1) then
        ASYMP=ASYMP_ANT
      EndIf

      RETURN
      END

C --------------------------------------------------------------------------
      FUNCTION SPLITSUD(IPDF,X,AMU)
C --------------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 SPLITSUD,X,AMU
      INTEGER IPDF
      REAL*8 FPDF
      REAL*8 A_SUD,DLOGQOPT2,A_ALPI
      COMMON/ASY_SPL/ A_SUD,DLOGQOPT2,A_ALPI
      REAL*8 PXF,CXF
      REAL*8 SPLIT,HSPLIT,CONVG,CONVQ,HCONVG,HCONVQ_ONE
      REAL*8 HJSPLIT
      EXTERNAL SPLIT,HSPLIT,CONVG,CONVQ,HCONVG,HCONVQ_ONE
      EXTERNAL HJSPLIT
      integer iflavor
      common/flavor/iflavor

      IF(TYPE_V.EQ.'H0' .OR. TYPE_V.EQ.'AG' .OR.
     .   TYPE_V.EQ.'ZG' .OR. TYPE_V.EQ.'GG') GOTO 100

      IF(TYPE_V.EQ.'HJ') GOTO 200

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C FOR W-BOSON

CCPY SET LTOPT=0 FOR CALCULATING THE ASYMPTOTIC PART
C    SET LTOPT=1 FOR CALCULATING THE DELTA_SIGMA FROM QT=0 TO PT
C    SET LTOPT=-1 FOR CALCULATING THE LEADING ORDER RESULT

      IF(LTOPT.EQ.0) THEN
C FOR ASYMPTOTIC PART
        IF(IPDF.EQ.0) THEN
         SPLITSUD=SPLIT(IPDF,X,AMU)
        ELSE
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
         SPLITSUD=SPLIT(IPDF,X,AMU)+A_SUD*FPDF
        ENDIF
        GOTO 999

      ELSEIF(LTOPT.EQ.1) THEN
C FOR DELTA_SIGMA
        IF(IPDF.EQ.0) THEN
         PXF=-A_ALPI*0.5*DLOGQOPT2*SPLIT(IPDF,X,AMU)
         CXF= CONVG(IPDF,X,AMU)
         SPLITSUD=PXF+CXF
        ELSE
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
CCPY Nov 2019: Note the needed change to be made in A_SUD
CBY Nov1,2018
C         PXF=-A_ALPI*DLOGQOPT2*(0.5*SPLIT(IPDF,X,AMU)+A_SUD*FPDF)
         PXF=-A_ALPI*(DLOGQOPT2*0.5*SPLIT(IPDF,X,AMU)+A_SUD*FPDF)

         CXF= CONVQ(IPDF,X,AMU)
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
         SPLITSUD=PXF+CXF-0.5*FPDF
        ENDIF
        GOTO 999

      ELSEIF(LTOPT.EQ.-1) THEN
C FOR LEADING ORDER
        IF(IPDF.EQ.0) THEN
         SPLITSUD=0.D0
        ELSE
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
         SPLITSUD=0.5*FPDF
        ENDIF
        GOTO 999


      ENDIF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
100   CONTINUE
C FOR H0

CCPY SET LTOPT=0 FOR CALCULATING THE ASYMPTOTIC PART
C    SET LTOPT=1 FOR CALCULATING THE DELTA_SIGMA FROM QT=0 TO PT
C    SET LTOPT=-1 FOR CALCULATING THE LEADING ORDER RESULT

      IF(LTOPT.EQ.0) THEN
C FOR ASYMPTOTIC PART
       IF(IPDF.EQ.0) THEN
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
         SPLITSUD=HSPLIT(IPDF,X,AMU)+A_SUD*FPDF
       ELSE
         SPLITSUD=HSPLIT(IPDF,X,AMU)
       ENDIF

CCPY Sept 2006: add the LTOPT.eq.1 option
C This corresponds to  LTO.eq.1 option in MAIN.FOR
      ELSEIF(LTOPT.EQ.1) THEN
C FOR DELTA_SIGMA
        IF(IPDF.EQ.0) THEN
C FROM GLUON SPLITTING TO GLUON
         FPDF=APDF(IPDF,X,AMU)
CCPY Nov 2019: Note the needed change to be made in A_SUD
CBY Nov1,2018
C         PXF=-A_ALPI*DLOGQOPT2*(0.5*HSPLIT(IPDF,X,AMU)+A_SUD*FPDF)
         PXF=-A_ALPI*(DLOGQOPT2*0.5*HSPLIT(IPDF,X,AMU)+A_SUD*FPDF)

         CXF= HCONVG(X,AMU)
         SPLITSUD=PXF+CXF-0.5*FPDF         
        ELSE
C FROM QUARK SPLITTING TO GLUON
         PXF=-A_ALPI*0.5*DLOGQOPT2*HSPLIT(IPDF,X,AMU)
         CXF= HCONVQ_ONE(IPDF,X,AMU)
         SPLITSUD=PXF+CXF
        ENDIF

       ELSEIF(LTOPT.EQ.-1) THEN
C FOR LEADING ORDER
        IF(IPDF.NE.0) THEN
         SPLITSUD=0.D0
        ELSE
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
         SPLITSUD=0.5*FPDF
        ENDIF

      ENDIF
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

200   CONTINUE
C FOR HJ
CCPY SET LTOPT=0 FOR CALCULATING THE ASYMPTOTIC PART
C    SET LTOPT=1 FOR CALCULATING THE DELTA_SIGMA FROM QT=0 TO PT
C    SET LTOPT=-1 FOR CALCULATING THE LEADING ORDER RESULT

      if(LTOPT.eq.0) THEN
C FOR ASYMPTOTIC PART
C      If(ipdf.eq.0) then
          if(ihadron.eq.-2) then
              fpdf=apdf_pion_minus(ipdf,X,amu)
          else
              fpdf=apdf(ipdf,x,amu)
          endif
          if(iflavor.eq.0) then
              if(ipdf.eq.0) then
          splitsud=(HJSPLIT(ipdf,x,amu)+A_SUD*FPDF)
              else
            splitsud=(HJSPLIT(ipdf,x,amu))
          endif
          else if(iflavor.eq.1) then
          splitsud=(HJSPLIT(ipdf,x,amu)+A_SUD*FPDF)
          endif
C      endif
      endif

999   CONTINUE
      RETURN
      END

      FUNCTION SPLIT(IPDF,X,AMU)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 SPLIT,X,AMU
      INTEGER IPDF
      REAL*8 FPDF
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 DNLIM,UPLIM,AERR,RERR,ERREST
      INTEGER IER,IACTA,IACTB
      REAL*8 ADZ3NT,ADZ4NT
      REAL*8 PQQFQ,PQQFQ_1,PQQFQ_2,PQGFG
      REAL*8 BPQQXFQ_1,BPQQXFQ_2,PQGXFG
      EXTERNAL BPQQXFQ_1,BPQQXFQ_2,PQGXFG

      SPLIT=0.D0
      X_INT=X
      IPDF_INT=IPDF
      AMU_INT=AMU

      UPLIM=1.0
      DNLIM=X
      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

      PQQFQ=0.
      PQQFQ_1=0.
      PQQFQ_2=0.
      PQGFG=0.

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF g --> q qbar
      IF(IPDF.EQ.0) THEN
       PQGFG=ADZ4NT(PQGXFG,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN PQGXFG'
        CALL QUIT
       ENDIF
       SPLIT=PQGFG

      ELSE
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF q --> g q  OR  qbar --> g qbar
C FIRST PART:
       PQQFQ_1=ADZ3NT(BPQQXFQ_1,DNLIM,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(*,*)' ERROR IN BPQQXFQ_1'
CZL        CALL QUIT
       ENDIF
C SECOND PART
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
      PQQFQ_2=FPDF*BPQQXFQ_2(X)
C FINAL RESULT OF q --> g q  OR  qbar --> g qbar
      PQQFQ=PQQFQ_1-PQQFQ_2
      SPLIT=PQQFQ

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ENDIF

      RETURN
      END

      FUNCTION BPQQXFQ_1(Z)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 BPQQXFQ_1,Z
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 FPDF1,FPDF2
      REAL*8 T1,T2
      INTEGER IPDF

C DO THE FIRST PART OF SPLITTING q --> g q  OR  qbar --> g qbar
      IPDF=IPDF_INT
      IF(IPDF.EQ.0) THEN
       WRITE(NWRT,*) ' WRONG IPDF IN BPQQXFQ_1'
       CALL QUIT
      ENDIF

          IF(IHADRON.EQ.-2) THEN
           FPDF1=APDF_PION_MINUS(IPDF,X_INT/Z,AMU_INT)
           FPDF2=APDF_PION_MINUS(IPDF,X_INT,AMU_INT)
          ELSE
           FPDF1=APDF(IPDF,X_INT/Z,AMU_INT)
           FPDF2=APDF(IPDF,X_INT,AMU_INT)
          ENDIF

      T1=FPDF1/Z-FPDF2
      T2=CF*(1.0+Z**2)/(1.0-Z)
      BPQQXFQ_1=T1*T2

      RETURN
      END

      FUNCTION BPQQXFQ_2(X)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 BPQQXFQ_2,X

C DO THE SECOND PART OF SPLITTING  q --> g q  OR  qbar --> g qbar
      BPQQXFQ_2=CF*(-2.0*DLOG(1.0-X)-X-0.5*X**2)

      RETURN
      END

      FUNCTION PQGXFG(Z)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 PQGXFG,Z
      REAL*8 X_INT,AMU_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      INTEGER IPDF_INT
      REAL*8 FPDF
      INTEGER IPDF
      REAL*8 T1,T2

C DO THE SPLITTING g --> q qbar
      IPDF=IPDF_INT
      IF(IPDF.NE.0) THEN
       WRITE(NWRT,*) ' WRONG IPDF IN PQGXFG'
       CALL QUIT
      ENDIF

          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X_INT/Z,AMU_INT)
          ELSE
           FPDF=APDF(IPDF,X_INT/Z,AMU_INT)
          ENDIF

      T1=FPDF/Z
      T2=0.5*(Z**2+(1.0-Z)**2)
      PQGXFG=T1*T2

      RETURN
      END

      FUNCTION HSPLIT(IPDF,X,AMU)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 HSPLIT,X,AMU
      INTEGER IPDF
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 DNLIM,UPLIM,AERR,RERR,ERREST
      INTEGER IER,IACTA,IACTB
      REAL*8 ADZ3NT,ADZ4NT,ADZ5NT
      REAL*8 FPDF
      REAL*8 PGGFG_1,PGGFG_2,PGGFG,PGJFJ
      REAL*8 BPGGXFG_1,BPGGXFG_2,BPGJXFJ
      EXTERNAL BPGGXFG_1,BPGGXFG_2,BPGJXFJ

      HSPLIT=0.D0
      X_INT=X
      IPDF_INT=IPDF
      AMU_INT=AMU

      PGGFG_1=0.D0
      PGGFG_2=0.D0
      PGGFG=0.D0
      PGJFJ=0.D0

          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF

      UPLIM=1.0
      DNLIM=X
      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

      IF(IPDF.EQ.0) THEN

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF g --> g g
C FIRST PART:
C FIRST PART OF 1/(1-Z)_+
       PGGFG_1=ADZ3NT(BPGGXFG_1,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN BPGGXFG_1'
        CALL QUIT
       ENDIF
C SECOND PART OF 1/(1-Z)_+
       PGGFG_1=6.0D0*(PGGFG_1+FPDF*DLOG(1.0D0-X))
C SECOND PART:
       PGGFG_2=ADZ4NT(BPGGXFG_2,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN BPGGXFG_2'
        WRITE(NWRT,*) 'DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB,PGGFG_2'
        WRITE(NWRT,*) DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB,PGGFG_2     
cdump        CALL QUIT
       ENDIF
C ADD 2*BETA1*\delta(1-Z)
       PGGFG_2=PGGFG_2+2.0*BETA1*FPDF
C FINAL RESULT FOR g --> g g
       PGGFG=PGGFG_1+PGGFG_2
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       HSPLIT=PGGFG

      ELSE

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF (q, qbar) --> (q, qbar) g
C FOR ALL QUARKS AND ANTI-QUARKS
       PGJFJ=ADZ5NT(BPGJXFJ,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN BPGJXFJ'
        CALL QUIT
       ENDIF
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       HSPLIT=PGJFJ

      ENDIF

      RETURN
      END

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CZL: analog to HSPLIT
CZL: the prime splitting function convoluting with PDF, ie P'xF
      FUNCTION HSPLITprime(IPDF,X,AMU)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 HSPLITprime,X,AMU
      INTEGER IPDF
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 DNLIM,UPLIM,AERR,RERR,ERREST
      INTEGER IER,IACTA,IACTB
      REAL*8 ADZ3NT
      REAL*8 FPDF
      REAL*8 PGGFG,PGJFJ
      REAL*8 BPGGXFGprime
      EXTERNAL BPGGXFGprime

      HSPLITprime=0.D0
      X_INT=X
      IPDF_INT=IPDF
      AMU_INT=AMU

      PGGFG=0.D0
      PGJFJ=0.D0

          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF

      UPLIM=1.0
      DNLIM=X
      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CZL: DO THE prime SPLITTING OF g --> g g
       PGGFG=ADZ3NT(BPGGXFGprime,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN BPGGXFG_1'
        CALL QUIT
       ENDIF
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       HSPLITprime=PGGFG

      RETURN
      END

      FUNCTION BPGGXFG_1(Z)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 BPGGXFG_1,Z
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 FPDF1,FPDF2
      REAL*8 T1,T2
      INTEGER IPDF

C DO THE FIRST PART OF SPLITTING g --> g g
      IPDF=IPDF_INT
      IF(IPDF.NE.0) THEN
       WRITE(NWRT,*) ' WRONG IPDF IN BPGGXFG_1'
       CALL QUIT
      ENDIF

         IF(IHADRON.EQ.-2) THEN
           FPDF1=APDF_PION_MINUS(IPDF,X_INT/Z,AMU_INT)
           FPDF2=APDF_PION_MINUS(IPDF,X_INT,AMU_INT)
          ELSE
           FPDF1=APDF(IPDF,X_INT/Z,AMU_INT)
           FPDF2=APDF(IPDF,X_INT,AMU_INT)
          ENDIF

      T1=FPDF1/Z-FPDF2
      T2=1.0D0/(1.0D0-Z)
      BPGGXFG_1=T1*T2

      RETURN
      END

      FUNCTION BPGGXFGprime(Z)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 BPGGXFGprime,Z
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 FPDF
      REAL*8 T1,T2
      INTEGER IPDF

C DO THE FIRST PART OF SPLITTING g --> g g
      IPDF=IPDF_INT
      IF(IPDF.NE.0) THEN
       WRITE(NWRT,*) ' WRONG IPDF IN BPGGXFG_1'
       CALL QUIT
      ENDIF

         IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X_INT/Z,AMU_INT)
          ELSE
           FPDF=APDF(IPDF,X_INT/Z,AMU_INT)
          ENDIF

      T1=FPDF/Z
      T2=6*(1-Z)/Z
      BPGGXFGprime=T1*T2

      RETURN
      END

      FUNCTION BPGGXFG_2(Z)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 BPGGXFG_2,Z
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 FPDF
      REAL*8 T1,T2
      INTEGER IPDF

C DO THE SECOND PART OF SPLITTING g --> g g
      IPDF=IPDF_INT
      IF(IPDF.NE.0) THEN
       WRITE(NWRT,*) ' WRONG IPDF IN BPGGXFG_2'
       CALL QUIT
      ENDIF

         IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X_INT/Z,AMU_INT)
          ELSE
           FPDF=APDF(IPDF,X_INT/Z,AMU_INT)
          ENDIF


      T1=FPDF/Z
      T2=6.0*(1.0/Z-2.0+Z*(1.0-Z))
      BPGGXFG_2=T1*T2

      RETURN
      END

      FUNCTION BPGJXFJ(Z)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 BPGJXFJ,Z
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 FPDF
      REAL*8 T1,T2
      INTEGER IPDF

C DO THE SPLITTING (q, qbar) --> (q, qbar) g
      IPDF=IPDF_INT
      IF(IPDF.EQ.0) THEN
       WRITE(NWRT,*) ' WRONG IPDF IN BPGJXFJ'
       CALL QUIT
      ENDIF

          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X_INT/Z,AMU_INT)
          ELSE
           FPDF=APDF(IPDF,X_INT/Z,AMU_INT)
          ENDIF

      T1=FPDF/Z
      T2=(4.0/3.0)*(1.0+(1.0-Z)**2)/Z
      BPGJXFJ=T1*T2

      RETURN
      END

      FUNCTION HJSPLIT(IPDF,X,AMU)
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 HJSPLIT,X,AMU
      INTEGER IPDF
      REAL*8 X_INT,AMU_INT
      INTEGER IPDF_INT
      COMMON/SPLIT_INT/ X_INT,AMU_INT,IPDF_INT
      REAL*8 DNLIM,UPLIM,AERR,RERR,ERREST
      INTEGER IER,IACTA,IACTB
      REAL*8 ADZ3NT,ADZ4NT,ADZ5NT
      REAL*8 FPDF
      REAL*8 PGGFG_1,PGGFG_2,PGGFG,PGJFJ
      REAL*8 BPGGXFG_1,BPGGXFG_2,BPGJXFJ
      EXTERNAL BPGGXFG_1,BPGGXFG_2,BPGJXFJ
      REAL*8 PQQFQ, PQQFQ_1, PQQFQ_2, PQGFG
      REAL*8 BPQQXFQ_1,BPQQXFQ_2, PQGXFG
      EXTERNAL BPQQXFQ_1,BPQQXFQ_2,PQGXFG
      integer iflavor
      common/flavor/iflavor

C      REAL*8 HJ_1, HJ_2, HJ_3
C      EXTERNAL HJ_1, HJ_2, HJ_3

      HJSPLIT=0.D0
      X_INT=X
      IPDF_INT=IPDF
      AMU_INT=AMU

      PGGFG_1=0.D0
      PGGFG_2=0.D0
      PGGFG=0.D0
      PGJFJ=0.D0

C      UPLIM = 1.0
C      DNLIM = 0.0
C      AERR=ACC_AERR
C      RERR=ACC_RERR
C      IACTA=2
C      IACTB=2
C
C      PGGFG_1=ADZ3NT(HJ_1,DNLIM,UPLIM,AERR,RERR,ERREST,
C     >               IER,IACTA,IACTB)
C      IF(IER.NE.0) THEN
C       WRITE(NWRT,*)' ERROR IN HJ_1'
C       CALL QUIT
C      ENDIF
C      DNLIM=x
C      PGGFG_2=ADZ4NT(HJ_2,DNLIM,UPLIM,AERR,RERR,ERREST,
C     >               IER,IACTA,IACTB)
C      IF(IER.NE.0) THEN
C       WRITE(NWRT,*)' ERROR IN HJ_2'
C       CALL QUIT
C      ENDIF
C      PGJFJ=ADZ5NT(HJ_3,DNLIM,UPLIM,AERR,RERR,ERREST,
C     >             IER,IACTA,IACTB)
C      IF(IER.NE.0) THEN
C       WRITE(NWRT,*)' ERROR IN HJ_3'
C       CALL QUIT
C      ENDIF
      

          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF

      UPLIM=1.0
      DNLIM=X
      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2
      if(iflavor.eq.0) then
      IF(IPDF.EQ.0) THEN

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF g --> g g
C FIRST PART:
C FIRST PART OF 1/(1-Z)_+
       PGGFG_1=ADZ3NT(BPGGXFG_1,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN BPGGXFG_1'
        CALL QUIT
       ENDIF
C SECOND PART OF 1/(1-Z)_+
       PGGFG_1=6.0D0*(PGGFG_1+FPDF*DLOG(1.0D0-X))
C SECOND PART:
       PGGFG_2=ADZ4NT(BPGGXFG_2,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN BPGGXFG_2'
        WRITE(NWRT,*) 'DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB,PGGFG_2'
        WRITE(NWRT,*) DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB,PGGFG_2     
cdump        CALL QUIT
       ENDIF
C ADD 2*BETA1*\delta(1-Z)
       PGGFG_2=PGGFG_2+2.0*BETA1*FPDF
C FINAL RESULT FOR g --> g g
       PGGFG=PGGFG_1+PGGFG_2
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC


      HJSPLIT=PGGFG

      ELSE

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF (q, qbar) --> (q, qbar) g
C FOR ALL QUARKS AND ANTI-QUARKS
       PGJFJ=ADZ5NT(BPGJXFJ,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN BPGJXFJ'
        CALL QUIT
       ENDIF
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       HJSPLIT=PGJFJ

      ENDIF
      else if(iflavor.eq.1) then
      PQQFQ=0.
      PQQFQ_1=0.
      PQQFQ_2=0.
      PQGFG=0.

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF g --> q qbar
      IF(IPDF.EQ.0) THEN
       PQGFG=ADZ4NT(PQGXFG,DNLIM,UPLIM,AERR,RERR,ERREST,
     > IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(NWRT,*)' ERROR IN PQGXFG'
        CALL QUIT
       ENDIF
       HJSPLIT=PQGFG

      ELSE
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C DO THE SPLITTING OF q --> g q  OR  qbar --> g qbar
C FIRST PART:
       PQQFQ_1=ADZ3NT(BPQQXFQ_1,DNLIM,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       IF(IER.NE.0) THEN
        WRITE(*,*)' ERROR IN BPQQXFQ_1'
CZL        CALL QUIT
       ENDIF
C SECOND PART
          IF(IHADRON.EQ.-2) THEN
           FPDF=APDF_PION_MINUS(IPDF,X,AMU)
          ELSE
           FPDF=APDF(IPDF,X,AMU)
          ENDIF
      PQQFQ_2=FPDF*BPQQXFQ_2(X)
C FINAL RESULT OF q --> g q  OR  qbar --> g qbar
      PQQFQ=PQQFQ_1-PQQFQ_2
      HJSPLIT=PQQFQ

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ENDIF
      endif

      RETURN
      END

      function fgetpert(vmas,vpt,rapin,gees,q0in,fresum,pma,pert,asy,
     >                  ier_pert)
      implicit none
      include 'common.for'
      integer ier_pert
      real*8 fgetpert
      real*8 vmas,vpt,rapin,gees(3),q0in,fresum,pma,pert,asy

CCPY Error: April 14, 2004 (correct ALEPASY for A0, etc)
      CALL SET_ZFF2

      if(ibeam.eq.-1) then
C For P Pbar scattering
        call spertppbar(vmas,vpt,rapin,gees,q0in,fresum,pma,pert,asy,
     >                  ier_pert)
      else
C For P-P, P-Nucleus, pion-Nucleus scatterings
        call spertpN(vmas,vpt,rapin,gees,q0in,fresum,pma,pert,asy,
     >               ier_pert)
      endif
c returns the contribution of PERTURBATIVE-ASMYPTOTIC
      fgetpert=pma
	call flush()
      return
      end

C --------------------------------------------------------------------------
      subroutine spertpN(vmas,vpt,rapin,gees,q0in,
     >                    fresum,pma,gal_pert,gal_asymp,ier_pert)
C --------------------------------------------------------------------------
c
c The subroutine version
c
C THIS IS PERT_PN.FOR
C ADD DRELL-YAN (PHOTON) PROCESS
Cgal: OCT 1993, JULY 1994
CCPY OCTOBER 1992, JULY 1993
C THIS IS MODEIFIED TO ONLY WORK FOR (P P) AND (P NUCLEUS) MACHINE

      IMPLICIT NONE
      INCLUDE 'common.for'

      INTEGER NF_EFF
      REAL*8 PERT
      REAL*8 ASYMP,AMU
      INTEGER IRUN,NRUN
csb      REAL*8 EW_ALFA

cgal:
      integer ier_pert
      real*8 vmas,vpt,rapin,gees(3),q0in,fresum,pma,gal_pert,gal_asymp
      Real*8 Coup_M, Omega
      Double Precision RUN_MASS_TOT
      REAL*8 XMTOP,XMBOT,XMC
      COMMON/XMASS/XMTOP,XMBOT,XMC

      Integer KinCorr
      Common / CorrectKinematics / KinCorr

      real*8 pyalem
      external pyalem

      Logical First_2
      Data First_2 /.True./

      real*8 Z_beta0, Z_beta1

CJI Jan 2015: Add in hj calculation
      Integer HasJet
      REAL*8 D1s, R, t, ptj
      REAL*8 H0Approx
      External H0Approx
      Common /Jet/ D1s, R, t, ptj, HasJet
      REAL*8 KK,dsigmadt,Kgg,tau,GHJ2

      ier_pert=0

      IF(IBEAM.EQ.-1) THEN
       PRINT*,' THIS CODE IS FOR (P P) AND (P NUCLEUS) scattering ONLY'
       CALL QUIT
      ENDIF
Cgal: no longer do we enforce lowest order QCD; technically wrong here, but
c     numerically insignificant
c      IF(NORDER.EQ.2) THEN
c       WRITE(*,*) ' FORCE NORDER = 1 IN PERT.FOR FOR ALL PDF'
c       WRITE(NWRT,*) ' FORCE NORDER = 1 IN PERT.FOR FOR ALL PDF'
c       NORDER=1
c      ENDIF

      Q_V=vmas
      S_Q=Q_V

      S_LAM=ALAMBD(S_Q)
      NF_EFF=NFL(S_Q)

      If ( TYPE_V.EQ.'H0' .Or. TYPE_V.EQ.'AG' .Or.
     .     TYPE_V.EQ.'ZG' .Or. TYPE_V.EQ.'GG' ) Then
       CALL HSETUP(NF_EFF)
      ELSE IF(HasJet.Eq.1) THEN
       CALL VJSETUP(NF_EFF)
      ELSE
       CALL WSETUP(NF_EFF)
      ENDIF

      S_BETA1=(33.0-2.0*NF_EFF)/12.0

c      ECM=ECM*UNIT
      ECM2=ECM**2

CCPY
      IF (Type_V.Eq.'W+_RA_UDB' .or. Type_V.Eq.'W-_RA_DUB' 
     > .or. Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
CJI September 2013: added new names for Z and W bosons to match c++
     > .or. Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD'
     > .or. Type_V.Eq.'WU' .or. Type_V.Eq.'WD' ) THEN
        NO_SIGMA0 = 1
      ENDIF

      IF (NO_SIGMA0.EQ.1) THEN
         CONST=1.0D0/2.0D0
      ELSE
        IF(TYPE_V.EQ.'W+'.OR. TYPE_V.EQ.'HP') THEN
C THIS IS THE \sigma_0 IN MY NOTES
          CONST=PI*WCOUPL*2.0/3.0 ! = Pi/3 Sqrt[2] GF MW^2
        ELSEIF(TYPE_V.EQ.'W-'.OR. TYPE_V.EQ.'HM') THEN
C THIS IS THE \sigma_0 IN MY NOTES
          CONST=PI*WCOUPL*2.0/3.0
        ELSEIF(TYPE_V.EQ.'Z0' .OR. TYPE_V.EQ.'HZ') THEN
C THIS IS THE \sigma_0 IN MY NOTES
          CONST=PI*ZCOUPL/3.0
        ELSEIF(TYPE_V.EQ.'H0') THEN
C THIS IS THE (\sigma_0)*(Q_V)^2 IN MY NOTES
CZL compensate running alpha_s for non-canonical choice
          Z_beta0=4*S_BETA1
          Z_beta1=34*3**2/3.d0-2*NF_EFF*(4/3.d0+5*3/3.d0)
          CONST=HCOUPL*(ALPI(C2*S_Q)*Q_V)**2

        Else If (TYPE_V.EQ.'H+') then
          CONST=Pi*HpCOUPL*2.0/3.0 ! = Pi/3 CR^2/4
CsB       Moved top mass factor here from Subroutine Standard, since Q_V is not
C         defined there.
          IF (I_MODEL.EQ.1) THEN  ! TOPCOLOR MODEL
            IF(I_RUNMASS.EQ.1) THEN
              COUP_M=RUN_MASS_TOT(Q_V,6)
            ELSE
              COUP_M=MT
            ENDIF
          ELSE IF (I_MODEL.EQ.2) THEN ! FOR 2HDMIII:
            IF(I_RUNMASS.EQ.1) THEN
              COUP_M=Sqrt(RUN_MASS_TOT(Q_V,6)*RUN_MASS_TOT(Q_V,4))
            ELSE
              COUP_M=Sqrt(MT*qmas_c)
            ENDIF
          End If
          CF = 4.d0/3.d0
          CONST=CONST*COUP_M**2

        Else If (TYPE_V.EQ.'HB') then
          CONST=Pi*HbCOUPL*2.0/3.0 ! = Pi/3 CR^2/4
CsB       Moved top mass factor here from Subroutine Standard, since Q_V is not
C         defined there.
          IF (I_RUNMASS.EQ.1) THEN
            COUP_M=RUN_MASS_TOT(Q_V,5)
          ELSE
            COUP_M=XMBOT
          End If
          CF = 4.d0/3.d0

          IF(FIRST_2) THEN
            Write(*,*) ' '
            WRITE(11,*) ' I_RUNMASS, COUP_M, XMBOT '
            WRITE(11,*)  I_RUNMASS, COUP_M, XMBOT
            FIRST_2=.FALSE.
          ENDIF
          
C          If (LTOpt.GT.-1.AND.I_RUNMASS.EQ.1) then
CCPY Feb 10, 2005, eliminate "omega" in HB process which 
C is not needed if the input
C B quark mass is the MS-bar running mass at M_B scale. 
C In C(1) function, we do not include the 3.d0*DLog((Q_V/XMBOT)**2)
C term of Omega, where 
C  Omega = 3.d0*DLog((Q_V/XMBOT)**2) + 4.d0.
C The 2nd term, 4, in Omega is due to the conversion from pole mass to MS-bar
C running mass. Thus, it is not correct to include the following line
c COUP_M = COUP_M*( 1.d0 + CF*Alpi(Q_V)/4.d0*Omega )
C when a MS-bar running mass is used.
C          End If

          CONST=CONST*COUP_M**2
!          Print*, ' CONST,HbCOUPL = ', CONST,HbCOUPL
C          Print*, ' Q_V,COUP_M = ', Q_V,COUP_M

        ELSEIF(TYPE_V.EQ.'A0') THEN
C THIS IS THE \sigma_0 IN MY NOTES
C ASSUME EW_ALFA IS NOT RUNNING
C          EW_ALFA=1.0D0/137.0359895D0
C          CONST=EW_ALFA
C          CONST=(CONST**2)*4.0*PI/9.0/Q_V/Q_V
          CONST=PI**2*pyalem(q_v**2)*4.0/3.0
          CONST=CONST*pyalem(q_v**2)/3.0/pi/q_v/q_v
        ELSEIF(TYPE_V.EQ.'AA') THEN
C From notes 'How to do qqB -> gamma gamma...' pp1:
C Const = sigma_0 = 4/3 Pi^2 alpha
C With alpha = e^2/(4 Pi): Const = 2 Pi/3 e^2/2
C With ACOUPL IS (e**2)/2
          CONST = 4.d0/3.d0*PI**2 *pyalem(q_v**2)
        ELSEIF(TYPE_V.EQ.'ZZ') THEN
CsB________From Ohnemus, Owens PRD43 (91) 3627 (3): the overall factor is the
C          same for qQ->AA and qQ->ZZ, only g+ and g- differ.
CZL          CONST = 4.d0/3.d0*PI**2 *pyalem(q_v**2)
          CONST = 4.d0/3.d0*PI**2 *ACOUPL/(2d0*PI)
        ELSEIF(TYPE_V.EQ.'WW_UUB'.or.TYPE_V.EQ.'WW_DDB') THEN
          CONST = 4.d0/3.d0*PI**2 *ACOUPL/(2d0*PI)
CZL          CONST = 4.d0/3.d0*PI**2 *pyalem(q_v**2)
        ELSEIF(TYPE_V.EQ.'GL') THEN
C THIS IS THE \sigma_0 IN MY NOTES
           CONST=8.0/9.0*PI**3*ALPI(C2*S_Q)
        ELSEIF(TYPE_V.EQ.'AG') THEN
C THIS IS THE \sigma_0 IN MY NOTES
!ZL compensate running alpha_s for non-canonical choice
          Z_beta0=4*S_BETA1
          Z_beta1=34*3**2/3.d0-2*NF_EFF*(4/3.d0+5*3/3.d0)
           CONST=2*PI/8.0/8.0*PI**2*ALPI(C2*S_Q)**2
!     &     *(1+Z_beta0/2d0*ALPI(S_Q)*log(C2**2)+1d0/16d0*ALPI(S_Q)**2
!     &       *(Z_beta0**2*log(C2**2)**2-2*Z_beta1*log(C2**2)))
        ELSEIF(TYPE_V.EQ.'ZG') THEN
CsB The overall constatnt is the same as for GG -> AA
           CONST=2*PI/8.0/8.0*PI**2*ALPI(C2*S_Q)**2
        ELSEIF(TYPE_V.EQ.'GG') THEN
CSM
           CONST=PI**3/16.0*ALPI(C2*S_Q)
        ELSEif(TYPE_V.eq.'HJ') THEN
            amu = 100d0
            KK = 1d0 + ALPI(125d0)*11d0/4d0
            Kgg = 1d0/4d0/(3d0**2-1d0)**2
            tau = 4d0 * mt**2/mh**2
            GHJ2 = asin(1d0/sqrt(tau))**2
            GHJ2 = 1d0+(1d0-tau)*GHJ2
            GHJ2 =4d0*dsqrt(2d0)*(ALPI(125d0)/4d0)**2*GMU*4d0/9d0!*tau**2*GHJ2**2
            dsigmadt=GHJ2*PI*ALPI(125d0)/4d0*KK*Kgg
            const = dsigmadt/S_Q**4
        ELSE
          PRINT*,' WRONG TYPE_V'
          CALL QUIT
        ENDIF
      ENDIF

      nrun=1
      DO 900 IRUN=1,NRUN

cgal: ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cgal: enters the values of the parameters AFTER the setup routine

	qt_v=vpt
	y_v=rapin

cgal: ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

cgal:      READ(NINR,*) QT_V,Y_V
      IF(MOD(IRUN,5).EQ.0) THEN
       WRITE(*,*)QT_V,Y_V,IRUN
      ENDIF

      PERT=0.D0
      ASYMP=0.D0
      CALL YMAXIMUM
      IF(DABS(Y_V).GT.YMAX) THEN
C       WRITE(NWRT,*) 'ABS(Y_V)  > YMAX'
C       WRITE(NWRT,*)QT_V,Y_V,PERT,ASYMP,YMAX,NORDER
CsB       WRITE(NOUT,*) 'ABS(Y_V)  > YMAX'
CsB       WRITE(NOUT,920)QT_V,Y_V,PERT,ASYMP,YMAX,NORDER
       ier_pert=1
       GOTO 900
      elseIF(abs(abs(ymax)-ABS(Y_V)).LT.YMAX/100) THEN
c if rapidity is close to YMAX, then we are in a region where the program
c   is not accurate; we flag this situation
       ier_pert=2
      ENDIF

cpn Feb 2004       ACC_RERR=1.D-3
       ACC_RERR=1.D-4
C       ACC_RERR=1.D-5

       FIT3=.FALSE.

CsB___Definition of momentum fractions x1 and x2
      If (KinCorr.Eq.0) then
        X_A=Q_V/ECM*DEXP(Y_V)
        X_B=Q_V/ECM/DEXP(Y_V)
      Else If (KinCorr.Eq.1) then
        X_A=Sqrt(Q_V**2+QT_V**2)/ECM*DEXP(Y_V)
        X_B=Sqrt(Q_V**2+QT_V**2)/ECM/DEXP(Y_V)
      End If
 
CCPY      AMU=C4*Q_V
CJI leave as amu, put scales else where
CJI set to MT for perturbative and move inside if statement
CJI TESTING HJ
C      AMU=100d0

CBY Oct,2018
C setting the pert scale
      IF(iscale.EQ.0) THEN
         AMU=Q_V
      ELSE
         AMU=Sqrt(Q_V**2+QT_V**2)
      ENDIF
      IF(LTOPT.EQ.-1) THEN
        PERT=0.D0
      ELSE
        CALL PERTURB_PN(PERT,AMU)
      ENDIF

CCPY NOV 2019
CIJ
C      IF(LTOPT.NE.1) THEN
C        AMU=Q_V
C        CALL ASYMPTO_PN(X_A,X_B,AMU,ASYMP)
C      ELSE
C        AMU=Q_V
C        CALL DELSIG_PN(X_A,X_B,AMU,ASYMP)
C      ENDIF
CBY Oct31,2018
C Subroutine ASYMPTO_PN contains the option LTOPT
C      IF(LTOPT.EQ.1 ) THEN
C         IF(type_v.eq.'ZU' .OR. type_v.eq.'ZD') THEN
C            CALL DELSIG_PN(X_A,X_B,AMU,ASYMP)
C         ELSE
C            CALL ASYMPTO_PN(X_A,X_B,AMU,ASYMP)
C         ENDIF
C      ELSE
C         CALL ASYMPTO_PN(X_A,X_B,AMU,ASYMP)
C      ENDIF

CCPY NOV 2019; THIS WORKS FOR NLO ONLY:
      CALL ASYMPTO_PN(X_A,X_B,AMU,ASYMP)

      IF(DEBUG) THEN
       WRITE(NWRT,*) ' ACC_RERR =',ACC_RERR
       WRITE(NWRT,*) ' AMU,C4,Q_V,S_Q,C1,C2,C3'
       WRITE(NWRT,*) AMU,C4,Q_V,S_Q,C1,C2,C3
       IF(FIT3) THEN
        WRITE(NWRT,*) 'FIT3 = TRUE'
       ELSE
        WRITE(NWRT,*) 'FIT3 = FALSE'
       ENDIF
      ENDIF

C      WRITE(NWRT,*)'QT_V,Y_V,Q_V,PERT,ASYMP,YMAX,X_A,X_B,NORDER'
C      WRITE(NWRT,*)QT_V,Y_V,Q_V,PERT,ASYMP,YMAX,X_A,X_B,NORDER
C      WRITE(NWRT,*)' '

CsB      WRITE(NOUT,920)QT_V,Y_V,PERT,ASYMP,YMAX,NORDER
920   FORMAT(F7.2,2X,F7.2,2X,D16.4,2X,D16.4,2X,F7.2,2X,I3)
C      IF(ASK('MORE')) GOTO 10

900   CONTINUE

cgal: send back d(SIGMA)/d(Q^2)d(QT)d(Y)
      gal_pert=pert
      gal_asymp=asymp
      pma=pert-asymp

      if(HASJET.EQ.1) then
          gal_pert = gal_pert*x_a*x_b*ECM2
          gal_asymp = gal_asymp*x_a*x_b*ECM2
          pma = pma*x_a*x_b*ECM2
      endif

      END


      SUBROUTINE PERTURB_PN(PERT,AMU)
      IMPLICIT NONE
      INCLUDE 'common.for'

      REAL*8 S,T,U,TM_V,AMU_PERT
      COMMON/PERT/ S,T,U,TM_V,AMU_PERT
      REAL*8 X1LOW,X2LOW,X1MIN,X2MIN,UPLIM,AERR,RERR,ERREST
      INTEGER IER,IACTA,IACTB

      REAL*8 ADZINT,ADZ2NT
      EXTERNAL ADZINT,ADZ2NT
      LOGICAL DOX1,DOX2
      REAL*8 X1INT,X2INT
      EXTERNAL X1INT,X2INT
      REAL*8 X1INTEG,X2INTEG
      REAL*8 EXPY,RTAUP,PERT,AMU
      REAL*8 SMALL
      DATA SMALL/1.D-8/

CCPY: Oct  2008, add the corresponding PERT part for
C  Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
      IF (Type_V.Eq.'W+_RA_UDB' .or. Type_V.Eq.'W-_RA_DUB') THEN
        PERT=0.D0
        RETURN
      ENDIF


      AMU_PERT=AMU
      S=ECM2
      EXPY=DEXP(Y_V)
      TM_V=DSQRT(QT_V**2+Q_V**2)
      T=-TM_V*ECM/EXPY+Q_V**2
      U=-TM_V*ECM*EXPY+Q_V**2

      RTAUP=(TM_V+QT_V)/ECM
      X1LOW=RTAUP*EXPY
      X2LOW=RTAUP/EXPY

      UPLIM=1.0D0-SMALL
      X1MIN=-U/(S+T-Q_V**2)
      X2MIN=-T/(S+U-Q_V**2)

      IF(DEBUG) THEN
       WRITE(NWRT,*)'S,T,U,TM_V,EXPY,RTAUP,AMU'
       WRITE(NWRT,*)S,T,U,TM_V,EXPY,RTAUP,AMU
       WRITE(NWRT,*)'X1LOW,X1MIN,X2LOW,X2MIN'
       WRITE(NWRT,*)X1LOW,X1MIN,X2LOW,X2MIN
      ENDIF

      DOX1=.TRUE.
      DOX2=.TRUE.
      IF(X1LOW.GT.1.0d0) THEN
CCPY       WRITE(NWRT,*)' DOX1=FALSE'
       DOX1=.FALSE.
      ENDIF
      IF(X2LOW.GT.1.0d0) THEN
CCPY       WRITE(NWRT,*)' DOX2=FALSE'
       DOX2=.FALSE.
      ENDIF

      X1INTEG=0.d0
      X2INTEG=0.d0

      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

      IF(DOX1) THEN
       IF(DOX2) THEN
        X1INTEG=ADZINT(X1INT,X1LOW,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
        X2INTEG=ADZ2NT(X2INT,X2LOW,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       ELSE
        X1INTEG=ADZINT(X1INT,X1MIN,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       ENDIF
      ELSE
       IF(DOX2) THEN
        X2INTEG=ADZ2NT(X2INT,X2MIN,UPLIM,AERR,RERR,ERREST,
     >IER,IACTA,IACTB)
       ELSE
        WRITE(NWRT,*)' ERROR IN PERTURB_PN'
        print*, Q_V, QT_V, Y_V, TM_V
        print*, x1low, x2low, rtaup
        CALL QUIT
       ENDIF
      ENDIF

      IF(DEBUG) THEN
       WRITE(NWRT,*)'X1INTEG,X2INTEG,CONST'
       WRITE(NWRT,*)X1INTEG,X2INTEG,CONST
      ENDIF

C THIS IS d(SIGMA)/d(Q^2)d(QT^2)d(Y)
      PERT=HBARC2*ALPI(muR*AMU)*PI*CONST*(X1INTEG+X2INTEG)
     >/PREV_UNDER/PREV_UNDER
C CONVERT TO d(SIGMA)/d(Q^2)d(QT)d(Y)
      PERT=PERT*2.0*QT_V

      RETURN
      END

C --------------------------------------------------------------------------
      SUBROUTINE PNX1X2INT(X1,X2,SH,TH,UH,AMU_in,PLUMI)
C --------------------------------------------------------------------------
C A1, A2 and A4 angular terms were not througly checked.
C L0 and A3 cancels the asymptotic pieces for low qT.
      IMPLICIT NONE
      INCLUDE 'common.for'
      REAL*8 X1,X2,SH,TH,UH,AMU,PLUMI,amu_in
      REAL*8 PLUMI_1,PLUMI_2
      REAL*8 U_DB,DB_U,D_UB,UB_D,U_UB,D_DB,UB_U,DB_D
c      REAL*8 G_WP,WP_G,G_WM,WM_G,G_Z0,Z0_G
      Real*8 U_WP_G,DB_WP_G,U_G_WP,DB_G_WP,
     >       D_WM_G,UB_WM_G,D_G_WM,UB_G_WM,
     >       G_Q_Z,G_QB_Z,Q_G_Z,QB_G_Z
      REAL*8 G_G,Q_QB,QB_Q,G_Q,Q_G
      REAL*8 AMP2QQ,AMP2GQ
      EXTERNAL AMP2QQ,AMP2GQ
      REAL*8 HAMP2GG,HAMP2GQ,HAMP2QQ
      EXTERNAL HAMP2GG,HAMP2GQ,HAMP2QQ
      INTEGER k
      REAL*8 APDFP1,APDFP2,APDFP3,APDFP4,APDFP5,
     >APDFM1,APDFM2,APDFM3,APDFM4,APDFM5
      REAL*8 PLN_1,PLN_2
      Real*8 Sig1,Sig2,Sig3,Sig4
      Real*8 C_BB,BB_C,C_G_HP,BB_G_HP,C_HP_G,BB_HP_G,
     ,       B_BB,BB_B,B_G_HP,B_HP_G

CsB   I_Proc = 1 Calculate only QI QJ -->  G V process
C     I_Proc = 2 Calculate only  G QI --> QJ V process

      IF(DEBUG) THEN
       WRITE(NWRT,*) ' IN PNX1X2INT  AMU = ',AMU
      ENDIF
      PLUMI_1=0.d0
      PLUMI_2=0.d0

      amu=muF*amu_in

      If (I_Proc.Eq.2) Goto 200 ! Calculate only G QI --> QJ V process

C -----------------
C FOR QI QJ --> G V
C -----------------
      Sig1 = 1.d0
C I_Pert Assignments. I_Pert:XSect -> 0:L0, 1:A1, 2:(A2+A0), 3:2*A3, 4:A4.
      If (I_Pert.Eq.1 .or. I_Pert.Eq.3) Sig1 = -1.d0

C -----------------
      IF(TYPE_V.EQ.'W+'.OR. TYPE_V.EQ.'HP') THEN

CsB    u_D = km12 f_{u/p}(x1) f_{D/p}(x2)
C          + km13 f_{u/p}(x1) f_{S/p}(x2) + ...

       APDFP1=APDF(1,X1,AMU)
       APDFP4=APDF(4,X1,AMU)
       APDFP2=APDF(-2,X2,AMU)
       APDFP3=APDF(-3,X2,AMU)
       APDFP5=APDF(-5,X2,AMU)

       U_DB=VKM(1,2)*APDFP1*APDFP2
     >     +VKM(1,3)*APDFP1*APDFP3
     >     +VKM(1,5)*APDFP1*APDFP5
     >     +VKM(4,2)*APDFP4*APDFP2
     >     +VKM(4,3)*APDFP4*APDFP3
     >     +VKM(4,5)*APDFP4*APDFP5

CsB    D_u = km12 f_{u/p}(x2) f_{D/p}(x1)
C          + km13 f_{u/p}(x2) f_{S/p}(x1) + ...

       APDFM1=APDF(1,X2,AMU)
       APDFM4=APDF(4,X2,AMU)
       APDFM2=APDF(-2,X1,AMU)
       APDFM3=APDF(-3,X1,AMU)
       APDFM5=APDF(-5,X1,AMU)

       DB_U=VKM(1,2)*APDFM1*APDFM2
     >     +VKM(1,3)*APDFM1*APDFM3
     >     +VKM(1,5)*APDFM1*APDFM5
     >     +VKM(4,2)*APDFM4*APDFM2
     >     +VKM(4,3)*APDFM4*APDFM3
     >     +VKM(4,5)*APDFM4*APDFM5

      PLUMI_1=1.d0/SH*(     U_DB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*DB_U*AMP2QQ(SH,UH,TH) )

C -----------------
      ELSEIF(TYPE_V.EQ.'W-'.OR. TYPE_V.EQ.'HM') THEN

CsB    d_U = km12 f_{U/p}(x2) f_{d/p}(x1)
C          + km13 f_{U/p}(x2) f_{s/p}(x1) + ...

       APDFP1=APDF(-1,X2,AMU)
       APDFP4=APDF(-4,X2,AMU)
       APDFP2=APDF(2,X1,AMU)
       APDFP3=APDF(3,X1,AMU)
       APDFP5=APDF(5,X1,AMU)

       D_UB=VKM(1,2)*APDFP1*APDFP2
     >     +VKM(1,3)*APDFP1*APDFP3
     >     +VKM(1,5)*APDFP1*APDFP5
     >     +VKM(4,2)*APDFP4*APDFP2
     >     +VKM(4,3)*APDFP4*APDFP3
     >     +VKM(4,5)*APDFP4*APDFP5

CsB    U_d = km12 f_{U/p}(x1) f_{d/p}(x2)
C          + km13 f_{U/p}(x1) f_{s/p}(x2) + ...

       APDFM1=APDF(-1,X1,AMU)
       APDFM4=APDF(-4,X1,AMU)
       APDFM2=APDF(2,X2,AMU)
       APDFM3=APDF(3,X2,AMU)
       APDFM5=APDF(5,X2,AMU)

       UB_D=VKM(1,2)*APDFM1*APDFM2
     >     +VKM(1,3)*APDFM1*APDFM3
     >     +VKM(1,5)*APDFM1*APDFM5
     >     +VKM(4,2)*APDFM4*APDFM2
     >     +VKM(4,3)*APDFM4*APDFM3
     >     +VKM(4,5)*APDFM4*APDFM5

      PLUMI_1=1.d0/SH*(     D_UB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*UB_D*AMP2QQ(SH,UH,TH) )

C -----------------
      ELSE IF (TYPE_V.EQ.'H+') then

CsB    c_B = f_{c/p}(x1) f_{B/p}(x2)

       APDFP4=APDF(4,X1,AMU)
       APDFP5=APDF(-5,X2,AMU)

       C_BB=APDFP4*APDFP5

CsB    B_c = f_{c/p}(x2) f_{B/p}(x1)

       APDFM4=APDF(4,X2,AMU)
       APDFM5=APDF(-5,X1,AMU)

       BB_C=APDFP4*APDFP5

      PLUMI_1=1.d0/SH*(     C_BB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*BB_C*AMP2QQ(SH,UH,TH) )
        
C -----------------
      ELSE IF (TYPE_V.EQ.'HB') then

CsB    b_B = f_{b/p}(x1) f_{B/p}(x2)

       APDFP4=APDF(5,X1,AMU)
       APDFP5=APDF(-5,X2,AMU)

       B_BB=APDFP4*APDFP5

CsB    B_b = f_{b/p}(x2) f_{B/p}(x1)

       APDFM4=APDF(5,X2,AMU)
       APDFM5=APDF(-5,X1,AMU)

       BB_B=APDFP4*APDFP5

      PLUMI_1=1.d0/SH*(     B_BB*AMP2QQ(SH,TH,UH) +
     +                 Sig1*BB_B*AMP2QQ(SH,UH,TH) )
        
C -----------------
      ELSE IF(TYPE_V.EQ.'Z0'.OR. TYPE_V.EQ.'A0' .OR.
     > TYPE_V.EQ.'AA' .OR. TYPE_V.EQ.'ZZ' .OR.
     > TYPE_V.EQ.'HZ' .OR. TYPE_V.EQ.'GL' ) THEN

        IF(IBEAM.EQ.-2) THEN
C FOR PION_MINUS-NUCLEUS SCATTERING
C FOR (PION_MINUS-P)
C X1 FOR PION_MINUS, X2 FOR PROTON

       U_UB=APDF_PION_MINUS(1,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF_PION_MINUS(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=APDF_PION_MINUS(2,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF_PION_MINUS(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF_PION_MINUS(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=APDF(1,X2,AMU)*APDF_PION_MINUS(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF_PION_MINUS(-4,X1,AMU)

       DB_D=APDF(2,X2,AMU)*APDF_PION_MINUS(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF_PION_MINUS(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF_PION_MINUS(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

C FOR (PION_MINUS-N)

       U_UB=APDF_PION_MINUS(1,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF_PION_MINUS(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=APDF_PION_MINUS(2,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF_PION_MINUS(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF_PION_MINUS(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=APDF(2,X2,AMU)*APDF_PION_MINUS(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF_PION_MINUS(-4,X1,AMU)

       DB_D=APDF(1,X2,AMU)*APDF_PION_MINUS(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF_PION_MINUS(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF_PION_MINUS(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

       PLN_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       PLUMI_1=PLUMI_1*(1.0D0-FRACT_N)+PLN_1*FRACT_N

        ELSE

C FOR IBEAM=1 OR IBEAM=0
C FOR (P P)
       U_UB=APDF(1,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=APDF(2,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=APDF(1,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF(-4,X1,AMU)

       DB_D=APDF(2,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       IF(IBEAM.NE.0) GOTO 1
C THIS IS ADDITONAL CONTRIBUTION FOR IBEAM=0
C FOR (P N)
CCPY MAY 1996
C THESE LINES PRESENT IN THE OLD VERSION WHICH GAVE RESULTS IN THE
C PAPER, PRD 50 (1994) 4415, WITH LADINSKY WERE WRONG.
C       U_UB=APDF(2,X1,AMU)*APDF(-2,X2,AMU)
C     >     +APDF(4,X1,AMU)*APDF(-4,X2,AMU)
C
C       D_DB=APDF(1,X1,AMU)*APDF(-1,X2,AMU)
C     >     +APDF(3,X1,AMU)*APDF(-3,X2,AMU)
C     >     +APDF(5,X1,AMU)*APDF(-5,X2,AMU)
C
C       UB_U=APDF(2,X2,AMU)*APDF(-2,X1,AMU)
C     >     +APDF(4,X2,AMU)*APDF(-4,X1,AMU)
C
C       DB_D=APDF(1,X2,AMU)*APDF(-1,X1,AMU)
C     >     +APDF(3,X2,AMU)*APDF(-3,X1,AMU)
C     >     +APDF(5,X2,AMU)*APDF(-5,X1,AMU)
C
C FOR (P N)
CCPY THE FOLLOWING ARE THE CORRECTED CODE
C X1 is for P, and X2 is for N

       U_UB=APDF(1,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=APDF(2,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=APDF(2,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF(-4,X1,AMU)

       DB_D=APDF(1,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

       PLN_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       PLUMI_1=PLUMI_1*(1.0D0-FRACT_N)+PLN_1*FRACT_N


       ENDIF

CZL
      ELSE IF(Type_V.Eq.'WW_UUB' .or. Type_V.Eq.'WW_DDB') THEN

       IF(Type_V.Eq.'WW_UUB') THEN

        IF(IBEAM.EQ.-2) THEN
C FOR PION_MINUS-NUCLEUS SCATTERING
C FOR (PION_MINUS-P)
C X1 FOR PION_MINUS, X2 FOR PROTON

       U_UB=APDF_PION_MINUS(1,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF_PION_MINUS(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=0.d0

       UB_U=APDF(1,X2,AMU)*APDF_PION_MINUS(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF_PION_MINUS(-4,X1,AMU)

       DB_D=0.d0

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

C FOR (PION_MINUS-N)

       U_UB=APDF_PION_MINUS(1,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF_PION_MINUS(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=0.d0

       UB_U=APDF(2,X2,AMU)*APDF_PION_MINUS(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF_PION_MINUS(-4,X1,AMU)

       DB_D=0.d0

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

       PLN_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       PLUMI_1=PLUMI_1*(1.0D0-FRACT_N)+PLN_1*FRACT_N

        ELSE

C FOR IBEAM=1 OR IBEAM=0
C FOR (P P)
       U_UB=APDF(1,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=0.d0

       UB_U=APDF(1,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF(-4,X1,AMU)

       DB_D=0.d0

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       IF(IBEAM.NE.0) GOTO 1
C THIS IS ADDITONAL CONTRIBUTION FOR IBEAM=0
C FOR (P N)
C X1 is for P, and X2 is for N

       U_UB=APDF(1,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(-4,X2,AMU)

       D_DB=0.d0

       UB_U=APDF(2,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF(-4,X1,AMU)

       DB_D=0.d0

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

       PLN_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       PLUMI_1=PLUMI_1*(1.0D0-FRACT_N)+PLN_1*FRACT_N


       ENDIF

       ELSEIF(Type_V.Eq.'WW_DDB') THEN

        IF(IBEAM.EQ.-2) THEN
C FOR PION_MINUS-NUCLEUS SCATTERING
C FOR (PION_MINUS-P)
C X1 FOR PION_MINUS, X2 FOR PROTON

       U_UB=0.d0

       D_DB=APDF_PION_MINUS(2,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF_PION_MINUS(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF_PION_MINUS(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=0.d0

       DB_D=APDF(2,X2,AMU)*APDF_PION_MINUS(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF_PION_MINUS(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF_PION_MINUS(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

C FOR (PION_MINUS-N)

       U_UB=0.d0

       D_DB=APDF_PION_MINUS(2,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF_PION_MINUS(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF_PION_MINUS(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=0.d0

       DB_D=APDF(1,X2,AMU)*APDF_PION_MINUS(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF_PION_MINUS(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF_PION_MINUS(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

       PLN_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       PLUMI_1=PLUMI_1*(1.0D0-FRACT_N)+PLN_1*FRACT_N

        ELSE

C FOR IBEAM=1 OR IBEAM=0
C FOR (P P)
       U_UB=0.d0

       D_DB=APDF(2,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=0.d0

       DB_D=APDF(2,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       IF(IBEAM.NE.0) GOTO 1
C THIS IS ADDITONAL CONTRIBUTION FOR IBEAM=0
C FOR (P N)
C X1 is for P, and X2 is for N

       U_UB=0.d0

       D_DB=APDF(2,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(-5,X2,AMU)

       UB_U=0.d0

       DB_D=APDF(1,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=D_DB*ZFF2(2)
       UB_U=UB_U*ZFF2(1)
       DB_D=DB_D*ZFF2(2)

       PLN_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))

       PLUMI_1=PLUMI_1*(1.0D0-FRACT_N)+PLN_1*FRACT_N


       ENDIF

      ENDIF
C -----------------
CCPY Oct 2008
CJI Sept 2013: Added in ZU and ZD to match c++ code
      ELSE IF(Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
     >   .or. Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD') THEN

        IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'ZU') THEN
C FOR IBEAM=1
C FOR (P P)
       U_UB=APDF(1,X1,AMU)*APDF(-1,X2,AMU)
     >     +APDF(4,X1,AMU)*APDF(-4,X2,AMU)

       UB_U=APDF(1,X2,AMU)*APDF(-1,X1,AMU)
     >     +APDF(4,X2,AMU)*APDF(-4,X1,AMU)

       U_UB=U_UB*ZFF2(1)
       D_DB=0.D0
       UB_U=UB_U*ZFF2(1)
       DB_D=0.D0

        ELSE IF( Type_V.Eq.'Z0_RA_DDB' .or. Type_V.Eq.'ZD') THEN
C FOR IBEAM=1
C FOR (P P)
       D_DB=APDF(2,X1,AMU)*APDF(-2,X2,AMU)
     >     +APDF(3,X1,AMU)*APDF(-3,X2,AMU)
     >     +APDF(5,X1,AMU)*APDF(-5,X2,AMU)

       DB_D=APDF(2,X2,AMU)*APDF(-2,X1,AMU)
     >     +APDF(3,X2,AMU)*APDF(-3,X1,AMU)
     >     +APDF(5,X2,AMU)*APDF(-5,X1,AMU)

       U_UB=0.D0
       D_DB=D_DB*ZFF2(2)
       UB_U=0.D0
       DB_D=DB_D*ZFF2(2)

	ENDIF

      PLUMI_1=1.d0/SH*(     (U_UB+D_DB)*AMP2QQ(SH,TH,UH)+
     +                 Sig1*(UB_U+DB_D)*AMP2QQ(SH,UH,TH))


C -----------------
ccdump
      Else If ( TYPE_V.EQ.'H0' .Or. TYPE_V.EQ.'AG' .Or.
     .          TYPE_V.EQ.'ZG' .Or. TYPE_V.EQ.'GG' ) Then
C FOR g g ---> H g

       G_G=APDF(0,X1,AMU)*APDF(0,X2,AMU)
       PLUMI_1=1.0d0/SH*G_G*HAMP2GG(SH,TH,UH)

      ENDIF

1     CONTINUE

      If (I_Proc.Eq.1) Goto 999 ! Calculate only QI QJ --> G V process

 200  Continue
C -----------------
C FOR G QI --> QJ V
C -----------------
      Sig1 = 1.d0
      Sig2 = 1.d0
      Sig3 = 1.d0
      Sig4 = 1.d0
C I_Pert Assignments. I_Pert:Piece -> 0:L0, 1:A1, 2:(A2+A0), 3:2*A3, 4:A4.
      If (I_Pert.Eq.1) then
        Sig2 = -1.d0
        Sig4 = -1.d0
      Else If (I_Pert.Eq.3) then
        Sig1 = -1.d0
        Sig4 = -1.d0
      Else If (I_Pert.Eq.4) then
        Sig1 = -1.d0
        Sig2 = -1.d0
      End If

C -----------------
      IF(TYPE_V.EQ.'W+'.OR. TYPE_V.EQ.'HP') THEN

c       G_WP=APDF(0,X1,AMU)*(
c     > APDF(1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(-2,X2,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(-3,X2,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(-5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )
c
c       WP_G=APDF(0,X2,AMU)*(
c     > APDF(1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(-2,X1,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(-3,X1,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(-5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )
c
c       PLUMI_2=1.0d0/SH*(G_WP*AMP2GQ(SH,TH,UH)+WP_G*AMP2GQ(SH,UH,TH))
CsB
       U_G_WP=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       DB_G_WP=APDF(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(-3,X2,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(-5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )

       U_WP_G=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       DB_WP_G=APDF(0,X2,AMU)*(
     > APDF(-2,X1,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(-3,X1,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(-5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

C These signs correspond to the notes: How to do the Y piece pp6.
         PLUMI_2=1.d0/SH*
     >          ((Sig3*DB_G_WP + Sig1*U_G_WP)*AMP2GQ(SH,TH,UH)+
     >           (Sig4*DB_WP_G + Sig2*U_WP_G)*AMP2GQ(SH,UH,TH))

C -----------------
      ELSEIF(TYPE_V.EQ.'W-'.OR. TYPE_V.EQ.'HM') THEN

c       G_WM=APDF(0,X1,AMU)*(
c     > APDF(-1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(-4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(2,X2,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(3,X2,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )
c
c       WM_G=APDF(0,X2,AMU)*(
c     > APDF(-1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
c     >+APDF(-4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5))
c     >+APDF(2,X1,AMU)*(VKM(1,2)+VKM(4,2))
c     >+APDF(3,X1,AMU)*(VKM(1,3)+VKM(4,3))
c     >+APDF(5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )
c
c       PLUMI_2=1.0d0/SH*(G_WM*AMP2GQ(SH,TH,UH)+WM_G*AMP2GQ(SH,UH,TH))
CsB
       UB_G_WM=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(-4,X2,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       D_G_WM=APDF(0,X1,AMU)*(
     > APDF(2,X2,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(3,X2,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(5,X2,AMU)*(VKM(1,5)+VKM(4,5)) )

       UB_WM_G=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*(VKM(1,2)+VKM(1,3)+VKM(1,5))
     >+APDF(-4,X1,AMU)*(VKM(4,2)+VKM(4,3)+VKM(4,5)) )

       D_WM_G=APDF(0,X2,AMU)*(
     > APDF(2,X1,AMU)*(VKM(1,2)+VKM(4,2))
     >+APDF(3,X1,AMU)*(VKM(1,3)+VKM(4,3))
     >+APDF(5,X1,AMU)*(VKM(1,5)+VKM(4,5)) )

C These signs correspond to the notes: How to do the Y piece pp6 & pp10.
         PLUMI_2=1.d0/SH*
     >          ((Sig3*UB_G_WM + Sig1*D_G_WM)*AMP2GQ(SH,TH,UH)+
     >           (Sig4*UB_WM_G + Sig2*D_WM_G)*AMP2GQ(SH,UH,TH))

C -----------------
      Else If (TYPE_V.EQ.'H+') then

       C_G_HP=APDF(0,X1,AMU)*APDF(4,X2,AMU)

       BB_G_HP=APDF(0,X1,AMU)*APDF(-5,X2,AMU)

       C_HP_G=APDF(0,X2,AMU)*APDF(4,X1,AMU)

       BB_HP_G=APDF(0,X2,AMU)*APDF(-5,X1,AMU)

C These signs correspond to the notes: How to do the Y piece pp6.
         PLUMI_2=1.d0/SH*
     >          ((Sig3*BB_G_HP + Sig1*C_G_HP)*AMP2GQ(SH,TH,UH)+
     >           (Sig4*BB_HP_G + Sig2*C_HP_G)*AMP2GQ(SH,UH,TH))

C -----------------
      Else If (TYPE_V.EQ.'HB') then

       B_G_HP=APDF(0,X1,AMU)*APDF(5,X2,AMU)

       BB_G_HP=APDF(0,X1,AMU)*APDF(-5,X2,AMU)

       B_HP_G=APDF(0,X2,AMU)*APDF(5,X1,AMU)

       BB_HP_G=APDF(0,X2,AMU)*APDF(-5,X1,AMU)

C These signs correspond to the notes: How to do the Y piece pp6.
         PLUMI_2=1.d0/SH*
     >          ((Sig3*BB_G_HP + Sig1*B_G_HP)*AMP2GQ(SH,TH,UH)+
     >           (Sig4*BB_HP_G + Sig2*B_HP_G)*AMP2GQ(SH,UH,TH))

C -----------------
      ELSE IF(TYPE_V.EQ.'Z0'.OR. TYPE_V.EQ.'A0' .OR.
     > TYPE_V.EQ.'AA' .OR. TYPE_V.EQ.'ZZ' .OR.
     > TYPE_V.EQ.'HZ' .OR. TYPE_V.EQ.'GL' ) THEN

        IF(IBEAM.EQ.-2) THEN
C FOR PION_MINUS-NUCLEUS
C FOR (PION_MINUS P)
C X1 FOR PION_MINUS, X2 FOR P

       G_Q_Z=APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4)
     >+APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z=APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4)
     >+APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(1,X1,AMU)*ZFF2(1)+
     > APDF_PION_MINUS(4,X1,AMU)*ZFF2(4)+
     > APDF_PION_MINUS(2,X1,AMU)*ZFF2(2)+
     > APDF_PION_MINUS(3,X1,AMU)*ZFF2(3)+
     > APDF_PION_MINUS(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(-1,X1,AMU)*ZFF2(1)
     >+APDF_PION_MINUS(-4,X1,AMU)*ZFF2(4)
     >+APDF_PION_MINUS(-2,X1,AMU)*ZFF2(2)
     >+APDF_PION_MINUS(-3,X1,AMU)*ZFF2(3)
     >+APDF_PION_MINUS(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

C FOR (PION_MINUS N)
CsB
       G_Q_Z = APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(2,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4)
     >+APDF(1,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z = APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4)
     >+APDF(-1,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z = APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(2,X1,AMU)*ZFF2(1)
     >+APDF_PION_MINUS(4,X1,AMU)*ZFF2(4)
     >+APDF_PION_MINUS(1,X1,AMU)*ZFF2(2)
     >+APDF_PION_MINUS(3,X1,AMU)*ZFF2(3)
     >+APDF_PION_MINUS(5,X1,AMU)*ZFF2(5) )

       QB_G_Z = APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(-2,X1,AMU)*ZFF2(1)
     >+APDF_PION_MINUS(-4,X1,AMU)*ZFF2(4)
     >+APDF_PION_MINUS(-1,X1,AMU)*ZFF2(2)
     >+APDF_PION_MINUS(-3,X1,AMU)*ZFF2(3)
     >+APDF_PION_MINUS(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLN_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       PLUMI_2=PLUMI_2*(1.0D0-FRACT_N)+PLN_2*FRACT_N


        ELSE

C FOR (P P)
c       G_Z0=APDF(0,X1,AMU)*(
c     > (APDF(1,X2,AMU)+APDF(-1,X2,AMU))*ZFF2(1)
c     >+(APDF(4,X2,AMU)+APDF(-4,X2,AMU))*ZFF2(4)
c     >+(APDF(2,X2,AMU)+APDF(-2,X2,AMU))*ZFF2(2)
c     >+(APDF(3,X2,AMU)+APDF(-3,X2,AMU))*ZFF2(3)
c     >+(APDF(5,X2,AMU)+APDF(-5,X2,AMU))*ZFF2(5) )
c
c       Z0_G=APDF(0,X2,AMU)*(
c     > (APDF(1,X1,AMU)+APDF(-1,X1,AMU))*ZFF2(1)
c     >+(APDF(4,X1,AMU)+APDF(-4,X1,AMU))*ZFF2(4)
c     >+(APDF(2,X1,AMU)+APDF(-2,X1,AMU))*ZFF2(2)
c     >+(APDF(3,X1,AMU)+APDF(-3,X1,AMU))*ZFF2(3)
c     >+(APDF(5,X1,AMU)+APDF(-5,X1,AMU))*ZFF2(5) )
c
c       PLUMI_2=1.0d0/SH*(G_Z0*AMP2GQ(SH,TH,UH)+Z0_G*AMP2GQ(SH,UH,TH))
CsB
       G_Q_Z=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4)
     >+APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4)
     >+APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4)
     >+APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4)
     >+APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       IF(IBEAM.NE.0) GOTO 999

C FOR (P N)
c       G_Z0=APDF(0,X1,AMU)*(
c     > (APDF(2,X2,AMU)+APDF(-2,X2,AMU))*ZFF2(1)
c     >+(APDF(4,X2,AMU)+APDF(-4,X2,AMU))*ZFF2(4)
c     >+(APDF(1,X2,AMU)+APDF(-1,X2,AMU))*ZFF2(2)
c     >+(APDF(3,X2,AMU)+APDF(-3,X2,AMU))*ZFF2(3)
c     >+(APDF(5,X2,AMU)+APDF(-5,X2,AMU))*ZFF2(5) )
c
c       Z0_G=APDF(0,X2,AMU)*(
c     > (APDF(2,X1,AMU)+APDF(-2,X1,AMU))*ZFF2(1)
c     >+(APDF(4,X1,AMU)+APDF(-4,X1,AMU))*ZFF2(4)
c     >+(APDF(1,X1,AMU)+APDF(-1,X1,AMU))*ZFF2(2)
c     >+(APDF(3,X1,AMU)+APDF(-3,X1,AMU))*ZFF2(3)
c     >+(APDF(5,X1,AMU)+APDF(-5,X1,AMU))*ZFF2(5) )
c
c       PLN_2=1.0d0/SH*(G_Z0*AMP2GQ(SH,TH,UH)+Z0_G*AMP2GQ(SH,UH,TH))
C
C FOR (P N)
C X1 is for P, and X2 is for N
CsB
       G_Q_Z = APDF(0,X1,AMU)*(
     > APDF(2,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4)
     >+APDF(1,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z = APDF(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4)
     >+APDF(-1,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

cpn feb 2004  The following code is wrong: the PDFs for the first beam 
cpn should be for a proton, not for a neutron. The correct code follows below.
c$$$       Q_G_Z = APDF(0,X2,AMU)*(
c$$$     > APDF(2,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4)
c$$$     >+APDF(1,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
c$$$     >+APDF(5,X1,AMU)*ZFF2(5) )
c$$$
c$$$       QB_G_Z = APDF(0,X2,AMU)*(
c$$$     > APDF(-2,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4)
c$$$     >+APDF(-1,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
c$$$     >+APDF(-5,X1,AMU)*ZFF2(5) )

       Q_G_Z = APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4)
     >+APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z = APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4)
     >+APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLN_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       PLUMI_2=PLUMI_2*(1.0D0-FRACT_N)+PLN_2*FRACT_N

       ENDIF

!ZL
      ELSE IF(Type_V.Eq.'WW_UUB' .or. Type_V.Eq.'WW_DDB') THEN

       IF(Type_V.Eq.'WW_UUB') THEN

        IF(IBEAM.EQ.-2) THEN
C FOR PION_MINUS-NUCLEUS
C FOR (PION_MINUS P)
C X1 FOR PION_MINUS, X2 FOR P

       G_Q_Z=APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4) )

       G_QB_Z=APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(1,X1,AMU)*ZFF2(1)+
     > APDF_PION_MINUS(4,X1,AMU)*ZFF2(4) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(-1,X1,AMU)*ZFF2(1)
     >+APDF_PION_MINUS(-4,X1,AMU)*ZFF2(4) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

C FOR (PION_MINUS N)
CsB
       G_Q_Z = APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(2,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4) )

       G_QB_Z = APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4) )

       Q_G_Z = APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(2,X1,AMU)*ZFF2(1)
     >+APDF_PION_MINUS(4,X1,AMU)*ZFF2(4) )

       QB_G_Z = APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(-2,X1,AMU)*ZFF2(1)
     >+APDF_PION_MINUS(-4,X1,AMU)*ZFF2(4) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLN_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       PLUMI_2=PLUMI_2*(1.0D0-FRACT_N)+PLN_2*FRACT_N


        ELSE

C FOR (P P)
       G_Q_Z=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4) )

       G_QB_Z=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       IF(IBEAM.NE.0) GOTO 999

C FOR (P N)
C X1 is for P, and X2 is for N
CsB
       G_Q_Z = APDF(0,X1,AMU)*(
     > APDF(2,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4) )

       G_QB_Z = APDF(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4) )

       Q_G_Z = APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4) )

       QB_G_Z = APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLN_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       PLUMI_2=PLUMI_2*(1.0D0-FRACT_N)+PLN_2*FRACT_N

       ENDIF

       ELSEIF(Type_V.Eq.'WW_DDB') THEN

        IF(IBEAM.EQ.-2) THEN
C FOR PION_MINUS-NUCLEUS
C FOR (PION_MINUS P)
C X1 FOR PION_MINUS, X2 FOR P

       G_Q_Z=APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z=APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(2,X1,AMU)*ZFF2(2)+
     > APDF_PION_MINUS(3,X1,AMU)*ZFF2(3)+
     > APDF_PION_MINUS(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(-2,X1,AMU)*ZFF2(2)
     >+APDF_PION_MINUS(-3,X1,AMU)*ZFF2(3)
     >+APDF_PION_MINUS(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

C FOR (PION_MINUS N)
CsB
       G_Q_Z = APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z = APDF_PION_MINUS(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z = APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(1,X1,AMU)*ZFF2(2)
     >+APDF_PION_MINUS(3,X1,AMU)*ZFF2(3)
     >+APDF_PION_MINUS(5,X1,AMU)*ZFF2(5) )

       QB_G_Z = APDF(0,X2,AMU)*(
     > APDF_PION_MINUS(-1,X1,AMU)*ZFF2(2)
     >+APDF_PION_MINUS(-3,X1,AMU)*ZFF2(3)
     >+APDF_PION_MINUS(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLN_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       PLUMI_2=PLUMI_2*(1.0D0-FRACT_N)+PLN_2*FRACT_N


        ELSE

C FOR (P P)
       G_Q_Z=APDF(0,X1,AMU)*(
     > APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z=APDF(0,X1,AMU)*(
     > APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       IF(IBEAM.NE.0) GOTO 999

C FOR (P N)
C X1 is for P, and X2 is for N
CsB
       G_Q_Z = APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z = APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z = APDF(0,X2,AMU)*(
     > APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z = APDF(0,X2,AMU)*(
     > APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

C These signs correspond to the notes: How to do the Y piece pp6.
        PLN_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

       PLUMI_2=PLUMI_2*(1.0D0-FRACT_N)+PLN_2*FRACT_N

       ENDIF

      ENDIF

C-----------------
CCPY Oct 2008
CJI Sept 2013: Added in ZU and ZD to match c++ version
      ELSE IF(Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'Z0_RA_DDB'
     >        .or. Type_V.Eq.'ZU' .or. Type_V.Eq.'ZD') THEN

        IF( Type_V.Eq.'Z0_RA_UUB' .or. Type_V.Eq.'ZU') THEN
C FOR IBEAM=1
C FOR (P P)
       G_Q_Z=APDF(0,X1,AMU)*(
     > APDF(1,X2,AMU)*ZFF2(1)+APDF(4,X2,AMU)*ZFF2(4) )

       G_QB_Z=APDF(0,X1,AMU)*(
     > APDF(-1,X2,AMU)*ZFF2(1)+APDF(-4,X2,AMU)*ZFF2(4) )

       Q_G_Z=APDF(0,X2,AMU)*(
     > APDF(1,X1,AMU)*ZFF2(1)+APDF(4,X1,AMU)*ZFF2(4) )

       QB_G_Z=APDF(0,X2,AMU)*(
     > APDF(-1,X1,AMU)*ZFF2(1)+APDF(-4,X1,AMU)*ZFF2(4) )


        ELSEIF( Type_V.Eq.'Z0_RA_DDB' .or. Type_V.Eq.'ZD') THEN
C FOR IBEAM=1
C FOR (P P)
       G_Q_Z=APDF(0,X1,AMU)*(
     >+APDF(2,X2,AMU)*ZFF2(2)+APDF(3,X2,AMU)*ZFF2(3)
     >+APDF(5,X2,AMU)*ZFF2(5) )

       G_QB_Z=APDF(0,X1,AMU)*(
     >+APDF(-2,X2,AMU)*ZFF2(2)+APDF(-3,X2,AMU)*ZFF2(3)
     >+APDF(-5,X2,AMU)*ZFF2(5) )

       Q_G_Z=APDF(0,X2,AMU)*(
     >+APDF(2,X1,AMU)*ZFF2(2)+APDF(3,X1,AMU)*ZFF2(3)
     >+APDF(5,X1,AMU)*ZFF2(5) )

       QB_G_Z=APDF(0,X2,AMU)*(
     >+APDF(-2,X1,AMU)*ZFF2(2)+APDF(-3,X1,AMU)*ZFF2(3)
     >+APDF(-5,X1,AMU)*ZFF2(5) )

	ENDIF
C These signs correspond to the notes: How to do the Y piece pp6.
        PLUMI_2=1.d0/SH *
     >          ((Sig1*G_Q_Z + Sig3*G_QB_Z)*AMP2GQ(SH,TH,UH)+
     >           (Sig2*Q_G_Z + Sig4*QB_G_Z)*AMP2GQ(SH,UH,TH))

C -----------------
      Else If ( TYPE_V.EQ.'H0' .Or. TYPE_V.EQ.'AG' .Or.
     .          TYPE_V.EQ.'ZG' .Or. TYPE_V.EQ.'GG' ) Then
C FOR g (q, qbar) ---> H (q, qbar)

       G_Q=0.D0
       Q_G=0.D0
       DO 20 K=-5,5
        IF(K.EQ.0) GOTO 20
        G_Q=G_Q+APDF(0,X1,AMU)*APDF(K,X2,AMU)
        Q_G=Q_G+APDF(K,X1,AMU)*APDF(0,X2,AMU)
20     CONTINUE

       PLUMI_2=1.0d0/SH*(G_Q*HAMP2GQ(SH,TH,UH)+Q_G*HAMP2GQ(SH,UH,TH))

C ALSO ADD IN q qbar --> H g

       Q_QB=0.D0
       QB_Q=0.D0
       DO 30 K=1,5
        Q_QB=Q_QB+APDF(K,X1,AMU)*APDF(-K,X2,AMU)
        QB_Q=QB_Q+APDF(-K,X1,AMU)*APDF(K,X2,AMU)
30     CONTINUE

       PLUMI_2=PLUMI_2+1.0d0/SH*(Q_QB+QB_Q)*HAMP2QQ(SH,TH,UH)

C -----------------
      ENDIF
999   CONTINUE

      PLUMI=PLUMI_1+PLUMI_2

      RETURN
      END

c --------------------------------------------------------------------------
      subroutine asympto_pn(a_x_a,a_x_b,a_q,asymp)
c --------------------------------------------------------------------------
csb calculation of singular pieces for p-p and p-n
      implicit none
      include 'common.for'

      real*8 a_x_a,a_x_b,a_q,asymp,asymp_sym,asymp_ant
      real*8 amu
      real*8 a_sud,dlogqopt2,a_alpi
      common/asy_spl/ a_sud,dlogqopt2,a_alpi
      real*8 u_db,db_u,d_ub,ub_d,u_ub,d_db,ub_u,db_d
      real*8 u_db_2,db_u_2,d_ub_2,ub_d_2,u_ub_2,d_db_2,ub_u_2,db_d_2
      real*8 x1,x2,g_z0,z0_g !,g_wp,wp_g,g_wm,wm_g
      real*8 u_wp_g,db_wp_g,u_g_wp,db_g_wp,
     >       d_wm_g,ub_wm_g,d_g_wm,ub_g_wm,
     >       g_q_z,g_qb_z,q_g_z,qb_g_z
      real*8 g_g,g_q,q_g
      real*8 splitsud
      external splitsud
      logical first
      integer k !ijump
      real*8 apdfp2,apdfp3,apdfp5,splitp1,splitp4,
     >apdfm2,apdfm3,apdfm5,splitm1,splitm4
      real*8 asyn_1,asyn_2,asymp_1,asymp_2
      real*8 c_bb,bb_c,c_bb_2,bb_c_2,c_g_hp,bb_g_hp,c_hp_g,bb_hp_g,
     ,       b_bb,bb_b,b_bb_2,bb_b_2,b_g_hp,b_hp_g 
      data first /.true./
cji jan 2015: add in hj calculation
      real*8 d1s, r,t,pt
      integer hasjet
      common /jet/ d1s, r,t,pt,hasjet
      integer iflavor
      common/flavor/iflavor

cji jun 2017: add in the second order singular piece 
      real*8  a_sud_2, beta0, nf_eff
      common /asy_spl2/ a_sud_2, beta0, nf_eff

csb   i_proc = 1 calculate only qi qj -->  g v process
c     i_proc = 2 calculate only  g qi --> qj v process

cgal: no longer do we enforce lowest order qcd; technically wrong here, but
c      if(norder.eq.2) then
c       if(first) then
c        write(nwrt,*) 'norder is set to be 1 in module asympto_pn'
c        first=.false.
c       endif
c      endif

      amu=muF*a_q
      a_alpi=alpi(muR*a_q)

ccpy may 2007: to encusre cancellation betweeen asympto and pert in low
c qt, we always use the canonical parameters here.

      if(ltopt.eq.0)then
csb >>>
!        a_sud=a1*dlog(a_q**2/qt_v**2)+b1
!zl keep relation c1=b0*c2
        a_sud=a1*dlog(q_v**2/qt_v**2)+b1
ccpy        a_sud=a1*(dlog((c1/c2/b0)**2)+dlog(a_q**2/qt_v**2))+
ccpy     +        b1
      elseif(ltopt.eq.1)then
CCPY          nf_eff = nfl(amu);
          nf_eff = nfl(muR*a_q);
          beta0 = (11*ca-2*nf_eff)/12.0
!        dlogqopt2=dlog(a_q**2/qt_v**2)
!zl keep relation c1=b0*c2
        dlogqopt2=dlog(q_v**2/qt_v**2)
c        print*, dlog((c1/c2/b0)**2), dlog(a_q**2/qt_v**2)
ccpy        dlogqopt2 = dlog((c1/c2/b0)**2)+dlog(a_q**2/qt_v**2)
        a_sud=a1/2.0*dlogqopt2**2+b1*dlogqopt2
CCPY
C        a_sud_2=1.0/8.0*a1**2*dlogqopt2**4+
C     >         (1.0/2.0*a1*b1-1.0/3.0*beta0*a1)*dlogqopt2**3+
C     >         (0.5*b1**2-0.5*beta0*b1-0.5*a2)*dlogqopt2**2+
C     >         (-2*zeta3*a1**2-b2)*dlogqopt2+
C     >         (-2*zeta3*a1*b1+4.0/3.0*beta0*zeta3)
C        a_sud_2=a_sud_2*a_alpi

csb <<<
CBY Nov1,2018
C only for the NLO calculation
C        a_sud=0.5*(a_sud+a_sud_2)
        a_sud=0.5*(a_sud)

      elseif(ltopt.eq.-1)then
        a_sud=0.d0
      endif

      if(type_v.eq.'hj') then
CCPY NOV 2019; DOES THIS NEED ADDITIONAL DLOGQOPT2 FACTOR?
          a_sud = a_sud + 0.5*d1s*dlog(1d0/r**2)
      endif


      if(debug) then
       write(nwrt,*) ' in asympto_pn: amu = ',amu
      endif

      if(ibeam.eq.-1) then
       write(nwrt,*) ' only works for p p or p nucleus, or pion-nucleus'
       call quit
      endif

      if (i_proc.eq.2) goto 200 ! calculate only g qi --> qj v process

csb definition of parton densities for different parton processes.
c -----------------
c for qi qj --> g v
c -----------------
      if(type_v.eq.'W+'.or. type_v.eq.'hp'
     > .or. type_v.eq.'w+_ra_udb') then

       apdfp2=apdf(-2,a_x_b,amu)
       apdfp3=apdf(-3,a_x_b,amu)
       apdfp5=apdf(-5,a_x_b,amu)
       splitp1=splitsud(1,a_x_a,amu)
       splitp4=splitsud(4,a_x_a,amu)

       u_db=vkm(1,2)*splitp1*apdfp2
     >     +vkm(1,3)*splitp1*apdfp3
     >     +vkm(1,5)*splitp1*apdfp5
     >     +vkm(4,2)*splitp4*apdfp2
     >     +vkm(4,3)*splitp4*apdfp3
     >     +vkm(4,5)*splitp4*apdfp5

       apdfm2=apdf(-2,a_x_a,amu)
       apdfm3=apdf(-3,a_x_a,amu)
       apdfm5=apdf(-5,a_x_a,amu)
       splitm1=splitsud(1,a_x_b,amu)
       splitm4=splitsud(4,a_x_b,amu)

       db_u=vkm(1,2)*splitm1*apdfm2
     >     +vkm(1,3)*splitm1*apdfm3
     >     +vkm(1,5)*splitm1*apdfm5
     >     +vkm(4,2)*splitm4*apdfm2
     >     +vkm(4,3)*splitm4*apdfm3
     >     +vkm(4,5)*splitm4*apdfm5

       apdfp2=splitsud(-2,a_x_b,amu)
       apdfp3=splitsud(-3,a_x_b,amu)
       apdfp5=splitsud(-5,a_x_b,amu)
       splitp1=apdf(1,a_x_a,amu)
       splitp4=apdf(4,a_x_a,amu)

       u_db_2=vkm(1,2)*splitp1*apdfp2
     >     +vkm(1,3)*splitp1*apdfp3
     >     +vkm(1,5)*splitp1*apdfp5
     >     +vkm(4,2)*splitp4*apdfp2
     >     +vkm(4,3)*splitp4*apdfp3
     >     +vkm(4,5)*splitp4*apdfp5

       apdfm2=splitsud(-2,a_x_a,amu)
       apdfm3=splitsud(-3,a_x_a,amu)
       apdfm5=splitsud(-5,a_x_a,amu)
       splitm1=apdf(1,a_x_b,amu)
       splitm4=apdf(4,a_x_b,amu)

       db_u_2=vkm(1,2)*splitm1*apdfm2
     >     +vkm(1,3)*splitm1*apdfm3
     >     +vkm(1,5)*splitm1*apdfm5
     >     +vkm(4,2)*splitm4*apdfm2
     >     +vkm(4,3)*splitm4*apdfm3
     >     +vkm(4,5)*splitm4*apdfm5
csb
       if (lepasy.eq.0) then
         asymp_1=u_db+db_u+u_db_2+db_u_2
       else if (lepasy.eq.1) then
         asymp_1=u_db-db_u+u_db_2-db_u_2
       end if

c -----------------
      else if(type_v.eq.'W-'.or. type_v.eq.'hm'
     > .or. type_v.eq.'w-_ra_dub') then

       apdfp2=splitsud(2,a_x_a,amu)
       apdfp3=splitsud(3,a_x_a,amu)
       apdfp5=splitsud(5,a_x_a,amu)
       splitp1=apdf(-1,a_x_b,amu)
       splitp4=apdf(-4,a_x_b,amu)

       d_ub=vkm(1,2)*splitp1*apdfp2
     >     +vkm(1,3)*splitp1*apdfp3
     >     +vkm(1,5)*splitp1*apdfp5
     >     +vkm(4,2)*splitp4*apdfp2
     >     +vkm(4,3)*splitp4*apdfp3
     >     +vkm(4,5)*splitp4*apdfp5

       apdfm2=splitsud(2,a_x_b,amu)
       apdfm3=splitsud(3,a_x_b,amu)
       apdfm5=splitsud(5,a_x_b,amu)
       splitm1=apdf(-1,a_x_a,amu)
       splitm4=apdf(-4,a_x_a,amu)

       ub_d=vkm(1,2)*splitm1*apdfm2
     >     +vkm(1,3)*splitm1*apdfm3
     >     +vkm(1,5)*splitm1*apdfm5
     >     +vkm(4,2)*splitm4*apdfm2
     >     +vkm(4,3)*splitm4*apdfm3
     >     +vkm(4,5)*splitm4*apdfm5

       apdfp2=apdf(2,a_x_a,amu)
       apdfp3=apdf(3,a_x_a,amu)
       apdfp5=apdf(5,a_x_a,amu)
       splitp1=splitsud(-1,a_x_b,amu)
       splitp4=splitsud(-4,a_x_b,amu)

       d_ub_2=vkm(1,2)*splitp1*apdfp2
     >     +vkm(1,3)*splitp1*apdfp3
     >     +vkm(1,5)*splitp1*apdfp5
     >     +vkm(4,2)*splitp4*apdfp2
     >     +vkm(4,3)*splitp4*apdfp3
     >     +vkm(4,5)*splitp4*apdfp5

       apdfm2=apdf(2,a_x_b,amu)
       apdfm3=apdf(3,a_x_b,amu)
       apdfm5=apdf(5,a_x_b,amu)
       splitm1=splitsud(-1,a_x_a,amu)
       splitm4=splitsud(-4,a_x_a,amu)

       ub_d_2=vkm(1,2)*splitm1*apdfm2
     >     +vkm(1,3)*splitm1*apdfm3
     >     +vkm(1,5)*splitm1*apdfm5
     >     +vkm(4,2)*splitm4*apdfm2
     >     +vkm(4,3)*splitm4*apdfm3
     >     +vkm(4,5)*splitm4*apdfm5
csb
       if (lepasy.eq.0) then
         asymp_1 = d_ub+ub_d+d_ub_2+ub_d_2
       else if (lepasy.eq.1) then
         asymp_1 = d_ub-ub_d+d_ub_2-ub_d_2
       end if

c -----------------
      else if (type_v.eq.'h+') then

       apdfp5=apdf(-5,a_x_b,amu)
       splitp4=splitsud(4,a_x_a,amu)

       c_bb=splitp4*apdfp5

       apdfm5=apdf(-5,a_x_a,amu)
       splitm4=splitsud(4,a_x_b,amu)

       bb_c=splitm4*apdfm5

       apdfp5=splitsud(-5,a_x_b,amu)
       splitp4=apdf(4,a_x_a,amu)

       c_bb_2=splitp4*apdfp5

       apdfm5=splitsud(-5,a_x_a,amu)
       splitm4=apdf(4,a_x_b,amu)

       bb_c_2=splitm4*apdfm5
csb
       if (lepasy.eq.0) then
         asymp_1=c_bb+bb_c+c_bb_2+bb_c_2
       else if (lepasy.eq.1) then
         asymp_1=c_bb-bb_c+c_bb_2-bb_c_2
       end if

c -----------------
      else if (type_v.eq.'hb') then

       apdfp5=apdf(-5,a_x_b,amu)
       splitp4=splitsud(5,a_x_a,amu)

       b_bb=splitp4*apdfp5

       apdfm5=apdf(-5,a_x_a,amu)
       splitm4=splitsud(5,a_x_b,amu)

       bb_b=splitm4*apdfm5

       apdfp5=splitsud(-5,a_x_b,amu)
       splitp4=apdf(5,a_x_a,amu)

       b_bb_2=splitp4*apdfp5

       apdfm5=splitsud(-5,a_x_a,amu)
       splitm4=apdf(5,a_x_b,amu)

       bb_b_2=splitm4*apdfm5
csb
       if (lepasy.eq.0) then
         asymp_1=b_bb+bb_b+b_bb_2+bb_b_2
       else if (lepasy.eq.1) then
         asymp_1=b_bb-bb_b+b_bb_2-bb_b_2
       end if

c -----------------
      else if(type_v.eq.'z0'.or. type_v.eq.'a0' .or.
     > type_v.eq.'aa' .or. type_v.eq.'zz' .or.
     > type_v.eq.'hz' .or. type_v.eq.'gl' ) then

        if(ibeam.eq.-2) then

c for (pion_minus p)
c x_a is for pion, x_b is for proton or neutron

       ihadron=-2
c for pion_minus in splitsud
       u_ub=splitsud(1,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)
       d_db=splitsud(2,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)
       ub_u_2=apdf(1,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)
       db_d_2=apdf(2,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)

       ihadron=1
c for proton in splitsud

       ub_u=splitsud(1,a_x_b,amu)*apdf_pion_minus(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf_pion_minus(-4,a_x_a,amu)
       db_d=splitsud(2,a_x_b,amu)*apdf_pion_minus(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf_pion_minus(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf_pion_minus(-5,a_x_a,amu)
       u_ub_2=apdf_pion_minus(1,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf_pion_minus(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)
       d_db_2=apdf_pion_minus(2,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf_pion_minus(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf_pion_minus(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

csb
       if (lepasy.eq.0) then
         asymp_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >          +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >          +(d_db+d_db_2-db_d-db_d_2)*zff2(2)
       end if

c for (pion_minus n)

       ihadron=-2
c for pion_minus in splitsud

       u_ub=splitsud(1,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)
       d_db=splitsud(2,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)
       ub_u_2=apdf(2,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)
       db_d_2=apdf(1,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)

       ihadron=1
c for proton in splitsud

       ub_u=splitsud(2,a_x_b,amu)*apdf_pion_minus(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf_pion_minus(-4,a_x_a,amu)
       db_d=splitsud(1,a_x_b,amu)*apdf_pion_minus(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf_pion_minus(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf_pion_minus(-5,a_x_a,amu)
       u_ub_2=apdf_pion_minus(1,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf_pion_minus(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)
       d_db_2=apdf_pion_minus(2,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf_pion_minus(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf_pion_minus(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

csb
       if (lepasy.eq.0) then
         asyn_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >         +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asyn_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >         +(d_db+d_db_2-db_d-db_d_2)*zff2(2)

       endif

       asymp_1=asymp_1*(1.0d0-fract_n)+asyn_1*fract_n

        else

c for (p p)
       u_ub=splitsud(1,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)

       d_db=splitsud(2,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)

       ub_u=splitsud(1,a_x_b,amu)*apdf(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf(-4,a_x_a,amu)

       db_d=splitsud(2,a_x_b,amu)*apdf(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf(-5,a_x_a,amu)

       u_ub_2=apdf(1,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)

       d_db_2=apdf(2,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

       ub_u_2=apdf(1,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)

       db_d_2=apdf(2,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)
csb
       if (lepasy.eq.0) then
         asymp_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >          +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >          +(d_db+d_db_2-db_d-db_d_2)*zff2(2)
       end if

       if(ibeam.ne.0) goto 1
c for (p n)
ccpy may 1996
c these lines present in the old version which gave results in the
c paper, prd 50 (1994) 4415, with ladinsky were wrong.
c       u_ub=splitsud(2,a_x_a,amu)*apdf(-2,a_x_b,amu)
c     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)
c
c       d_db=splitsud(1,a_x_a,amu)*apdf(-1,a_x_b,amu)
c     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
c     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)
c
c       ub_u=splitsud(2,a_x_b,amu)*apdf(-2,a_x_a,amu)
c     >     +splitsud(4,a_x_b,amu)*apdf(-4,a_x_a,amu)
c
c       db_d=splitsud(1,a_x_b,amu)*apdf(-1,a_x_a,amu)
c     >     +splitsud(3,a_x_b,amu)*apdf(-3,a_x_a,amu)
c     >     +splitsud(5,a_x_b,amu)*apdf(-5,a_x_a,amu)
c
c       u_ub_2=apdf(2,a_x_a,amu)*splitsud(-2,a_x_b,amu)
c     >     +apdf(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)
c
c       d_db_2=apdf(1,a_x_a,amu)*splitsud(-1,a_x_b,amu)
c     >     +apdf(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
c     >     +apdf(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)
c
c       ub_u_2=apdf(2,a_x_b,amu)*splitsud(-2,a_x_a,amu)

       apdfp5=splitsud(-5,a_x_b,amu)
c     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)
c
c       db_d_2=apdf(1,a_x_b,amu)*splitsud(-1,a_x_a,amu)
c     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
c     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)
c
c for (p n)
ccpy the following are the corrected code
c x1 is for p, and x2 is for n
       u_ub=splitsud(1,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)

       d_db=splitsud(2,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)

       ub_u=splitsud(2,a_x_b,amu)*apdf(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf(-4,a_x_a,amu)

       db_d=splitsud(1,a_x_b,amu)*apdf(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf(-5,a_x_a,amu)

       u_ub_2=apdf(1,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)

       d_db_2=apdf(2,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)


       apdfp5=splitsud(-5,a_x_b,amu)
       ub_u_2=apdf(2,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)

       db_d_2=apdf(1,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)
csb
       if (lepasy.eq.0) then
         asyn_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >         +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asyn_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >         +(d_db+d_db_2-db_d-db_d_2)*zff2(2)

       endif

       asymp_1=asymp_1*(1.0d0-fract_n)+asyn_1*fract_n

       endif

!zl
      else if(type_v.eq.'ww_uub' .or. type_v.eq.'ww_ddb') then

       if(type_v.eq.'ww_uub') then

       apdfp5=splitsud(-5,a_x_b,amu)

        if(ibeam.eq.-2) then

c for (pion_minus p)
c x_a is for pion, x_b is for proton or neutron

       ihadron=-2
c for pion_minus in splitsud
       u_ub=splitsud(1,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)
       d_db=0.d0
       ub_u_2=apdf(1,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)
       db_d_2=0.d0

       ihadron=1
c for proton in splitsud

       ub_u=splitsud(1,a_x_b,amu)*apdf_pion_minus(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf_pion_minus(-4,a_x_a,amu)
       db_d=0.d0
       u_ub_2=apdf_pion_minus(1,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf_pion_minus(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)
       d_db_2=0.d0

csb
       if (lepasy.eq.0) then
         asymp_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >          +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >          +(d_db+d_db_2-db_d-db_d_2)*zff2(2)
       end if

c for (pion_minus n)

       ihadron=-2
c for pion_minus in splitsud

       u_ub=splitsud(1,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)
       d_db=0.d0
       ub_u_2=apdf(2,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)
       db_d_2=0.d0

       ihadron=1
c for proton in splitsud

       ub_u=splitsud(2,a_x_b,amu)*apdf_pion_minus(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf_pion_minus(-4,a_x_a,amu)
       db_d=0.d0
       u_ub_2=apdf_pion_minus(1,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf_pion_minus(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)
       d_db_2=0.d0

csb
       if (lepasy.eq.0) then
         asyn_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >         +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asyn_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >         +(d_db+d_db_2-db_d-db_d_2)*zff2(2)

       endif

       asymp_1=asymp_1*(1.0d0-fract_n)+asyn_1*fract_n

        else

c for (p p)
       u_ub=splitsud(1,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)

       d_db=0.d0

       ub_u=splitsud(1,a_x_b,amu)*apdf(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf(-4,a_x_a,amu)

       db_d=0.d0

       u_ub_2=apdf(1,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)

       d_db_2=0.d0

       ub_u_2=apdf(1,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)

       db_d_2=0.d0
csb
       if (lepasy.eq.0) then
         asymp_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >          +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >          +(d_db+d_db_2-db_d-db_d_2)*zff2(2)
       end if

       if(ibeam.ne.0) goto 1
c for (p n)
ccpy the following are the corrected code
c x1 is for p, and x2 is for n
       u_ub=splitsud(1,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)

       d_db=0.d0

       ub_u=splitsud(2,a_x_b,amu)*apdf(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf(-4,a_x_a,amu)

       db_d=0.d0

       u_ub_2=apdf(1,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)

       d_db_2=0.d0

       ub_u_2=apdf(2,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)

       db_d_2=0.d0
csb
       if (lepasy.eq.0) then
         asyn_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >         +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asyn_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >         +(d_db+d_db_2-db_d-db_d_2)*zff2(2)

       endif

       asymp_1=asymp_1*(1.0d0-fract_n)+asyn_1*fract_n

       endif

       elseif(type_v.eq.'ww_ddb') then

        if(ibeam.eq.-2) then

c for (pion_minus p)
c x_a is for pion, x_b is for proton or neutron

       ihadron=-2
c for pion_minus in splitsud
       u_ub=0.d0
       d_db=splitsud(2,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)
       ub_u_2=0.d0
       db_d_2=apdf(2,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)

       ihadron=1
c for proton in splitsud

       ub_u=0.d0
       db_d=splitsud(2,a_x_b,amu)*apdf_pion_minus(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf_pion_minus(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf_pion_minus(-5,a_x_a,amu)
       u_ub_2=0.d0
       d_db_2=apdf_pion_minus(2,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf_pion_minus(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf_pion_minus(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

csb
       if (lepasy.eq.0) then
         asymp_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >          +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >          +(d_db+d_db_2-db_d-db_d_2)*zff2(2)
       end if

c for (pion_minus n)

       ihadron=-2
c for pion_minus in splitsud

       u_ub=0.d0
       d_db=splitsud(2,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)
       ub_u_2=0.d0
       db_d_2=apdf(1,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)

       ihadron=1
c for proton in splitsud

       ub_u=0.d0
       db_d=splitsud(1,a_x_b,amu)*apdf_pion_minus(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf_pion_minus(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf_pion_minus(-5,a_x_a,amu)
       u_ub_2=0.d0
       d_db_2=apdf_pion_minus(2,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf_pion_minus(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf_pion_minus(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

csb
       if (lepasy.eq.0) then
         asyn_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >         +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asyn_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >         +(d_db+d_db_2-db_d-db_d_2)*zff2(2)

       endif

       asymp_1=asymp_1*(1.0d0-fract_n)+asyn_1*fract_n

        else

c for (p p)
       u_ub=0.d0

       d_db=splitsud(2,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)

       ub_u=0.d0

       db_d=splitsud(2,a_x_b,amu)*apdf(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf(-5,a_x_a,amu)

       u_ub_2=0.d0

       d_db_2=apdf(2,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

       ub_u_2=0.d0

       db_d_2=apdf(2,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)
csb
       if (lepasy.eq.0) then
         asymp_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >          +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >          +(d_db+d_db_2-db_d-db_d_2)*zff2(2)
       end if

       if(ibeam.ne.0) goto 1
c for (p n)
ccpy the following are the corrected code
c x1 is for p, and x2 is for n
       u_ub=0.d0

       d_db=splitsud(2,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)

       ub_u=0.d0

       db_d=splitsud(1,a_x_b,amu)*apdf(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf(-5,a_x_a,amu)

       u_ub_2=0.d0

       d_db_2=apdf(2,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

       ub_u_2=0.d0

       db_d_2=apdf(1,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)
csb
       if (lepasy.eq.0) then
         asyn_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >         +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asyn_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >         +(d_db+d_db_2-db_d-db_d_2)*zff2(2)

       endif

       asymp_1=asymp_1*(1.0d0-fract_n)+asyn_1*fract_n

       endif

      endif

ccpy
cji september 2013: added in new z types to match c++ code
      else if( type_v.eq.'z0_ra_uub' .or. type_v.eq.'z0_ra_ddb'
     >   .or. type_v.eq.'zu' .or. type_v.eq.'zd'
     >   .or. type_v.eq.'ZU' .or. type_v.eq.'ZD' ) then

        if(ibeam.ne.1) then
ccpy only for (p p) collision
           print*,' not implemented for ibeam = ', ibeam        
           call quit
        endif
           
c for (p p)
cji september 2013: added in zu to match c++ code
        if( type_v.eq.'z0_ra_uub' .or. type_v.eq.'zu'
     >         .or. type_v.eq.'ZU') then

       u_ub=splitsud(1,a_x_a,amu)*apdf(-1,a_x_b,amu)
     >     +splitsud(4,a_x_a,amu)*apdf(-4,a_x_b,amu)

       ub_u=splitsud(1,a_x_b,amu)*apdf(-1,a_x_a,amu)
     >     +splitsud(4,a_x_b,amu)*apdf(-4,a_x_a,amu)

       u_ub_2=apdf(1,a_x_a,amu)*splitsud(-1,a_x_b,amu)
     >     +apdf(4,a_x_a,amu)*splitsud(-4,a_x_b,amu)

       ub_u_2=apdf(1,a_x_b,amu)*splitsud(-1,a_x_a,amu)
     >     +apdf(4,a_x_b,amu)*splitsud(-4,a_x_a,amu)

       d_db=0.d0
       db_d=0.d0
       d_db_2=0.d0
       db_d_2=0.d0
       
cji september 2013: added in zd to match c++ code
        elseif( type_v.eq.'z0_ra_ddb' .or. type_v.eq.'zd'
     >         .or. type_v.eq.'ZD') then

       d_db=splitsud(2,a_x_a,amu)*apdf(-2,a_x_b,amu)
     >     +splitsud(3,a_x_a,amu)*apdf(-3,a_x_b,amu)
     >     +splitsud(5,a_x_a,amu)*apdf(-5,a_x_b,amu)

       db_d=splitsud(2,a_x_b,amu)*apdf(-2,a_x_a,amu)
     >     +splitsud(3,a_x_b,amu)*apdf(-3,a_x_a,amu)
     >     +splitsud(5,a_x_b,amu)*apdf(-5,a_x_a,amu)

       d_db_2=apdf(2,a_x_a,amu)*splitsud(-2,a_x_b,amu)
     >     +apdf(3,a_x_a,amu)*splitsud(-3,a_x_b,amu)
     >     +apdf(5,a_x_a,amu)*splitsud(-5,a_x_b,amu)

       db_d_2=apdf(2,a_x_b,amu)*splitsud(-2,a_x_a,amu)
     >     +apdf(3,a_x_b,amu)*splitsud(-3,a_x_a,amu)
     >     +apdf(5,a_x_b,amu)*splitsud(-5,a_x_a,amu)

       u_ub=0.d0
       ub_u=0.d0
       u_ub_2=0.d0
       ub_u_2=0.d0
       
        endif
csb
       if (lepasy.eq.0) then
         asymp_1=(u_ub+u_ub_2+ub_u+ub_u_2)*zff2(1)
     >          +(d_db+d_db_2+db_d+db_d_2)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub+u_ub_2-ub_u-ub_u_2)*zff2(1)
     >          +(d_db+d_db_2-db_d-db_d_2)*zff2(2)
       end if


c -----------------
      else if ( type_v.eq.'h0' .or. type_v.eq.'ag' .or.
     .          type_v.eq.'zg' .or. type_v.eq.'gg' ) then
c for g g --> h g

       g_g=splitsud(0,a_x_a,amu)*apdf(0,a_x_b,amu)
     >    +apdf(0,a_x_a,amu)*splitsud(0,a_x_b,amu)
c identical particle
       asymp_1=g_g

      else  if(type_v.eq.'hj') then
cji change
          amu=125d0
      a_alpi=alpi(amu)
      if(iflavor.eq.0) then
       g_g = splitsud(0,a_x_a,amu)*apdf(0,a_x_b,amu)
     >     + apdf(0,a_x_a,amu)*splitsud(0,a_x_b,amu)
       g_g = g_g +(splitsud(1,a_x_a,amu) + splitsud(2,a_x_a,amu)
     >            +splitsud(3,a_x_a,amu) + splitsud(4,a_x_a,amu)
     >            +splitsud(-1,a_x_a,amu)+ splitsud(-2,a_x_a,amu)
     >            +splitsud(-3,a_x_a,amu)+ splitsud(-4,a_x_a,amu)
     >            +splitsud(5,a_x_a,amu)+splitsud(-5,a_x_a,amu))
     >            *apdf(0,a_x_b,amu) +
     >            (splitsud(1,a_x_b,amu) + splitsud(2,a_x_b,amu)
     >            +splitsud(3,a_x_b,amu) + splitsud(4,a_x_b,amu)
     >            +splitsud(-1,a_x_b,amu)+ splitsud(-2,a_x_b,amu)
     >            +splitsud(-3,a_x_b,amu)+ splitsud(-4,a_x_b,amu)
     >            +splitsud(5,a_x_b,amu)+splitsud(-5,a_x_b,amu))
     >            *apdf(0,a_x_a,amu)
       asymp_1 = g_g
      else if(iflavor.eq.1) then
        asymp_1 = splitsud(0,a_x_a,amu)*(apdf(1,a_x_b,amu)
     >          + apdf(2,a_x_b,amu) + apdf(3,a_x_b,amu)
     >          + apdf(4,a_x_b,amu) + apdf(5,a_x_b,amu)
     >          + apdf(-1,a_x_b,amu) + apdf(-2,a_x_b,amu)
     >          + apdf(-3,a_x_b,amu) + apdf(-4,a_x_b,amu)
     >          + apdf(-5,a_x_b,amu))
        asymp_1 = asymp_1 + splitsud(0,a_x_b,amu)*(apdf(1,a_x_a,amu)
     >          + apdf(2,a_x_a,amu) + apdf(3,a_x_a,amu)
     >          + apdf(4,a_x_a,amu) + apdf(5,a_x_a,amu)
     >          + apdf(-1,a_x_a,amu) + apdf(-2,a_x_a,amu)
     >          + apdf(-3,a_x_a,amu) + apdf(-4,a_x_a,amu)
     >          + apdf(-5,a_x_a,amu))
        asymp_1 = asymp_1 +apdf(0,a_x_a,amu)*(splitsud(1,a_x_b,amu)
     >          + splitsud(2,a_x_b,amu) + splitsud(3,a_x_b,amu)
     >          + splitsud(4,a_x_b,amu) + splitsud(5,a_x_b,amu)
     >          + splitsud(-1,a_x_b,amu) + splitsud(-2,a_x_b,amu)
     >          + splitsud(-3,a_x_b,amu) + splitsud(-4,a_x_b,amu)
     >          + splitsud(-5,a_x_b,amu))
        asymp_1 = asymp_1 + apdf(0,a_x_b,amu)*(splitsud(1,a_x_a,amu)
     >          + splitsud(2,a_x_a,amu)  + splitsud(3,a_x_a,amu)
     >          + splitsud(4,a_x_a,amu)  + splitsud(5,a_x_a,amu)
     >          + splitsud(-1,a_x_a,amu) + splitsud(-2,a_x_a,amu)
     >          + splitsud(-3,a_x_a,amu) + splitsud(-4,a_x_a,amu)
     >          + splitsud(-5,a_x_a,amu))
      endif
      else
        print*, ' [asympt_pn] no such a type_v: ', type_v

      endif

1     continue

ccpy for leading order
      if(ltopt.eq.-1) goto 999

      if (i_proc.eq.1) goto 999 ! calculate only qi qj --> g v process
 200  continue
c -----------------
c for g qi --> qj v
c -----------------
      x1=a_x_a
      x2=a_x_b

      if(type_v.eq.'W+'.or. type_v.eq.'hp'
     >  .or. type_v.eq.'w+_ra_udb') then

c       g_wp=splitsud(0,x1,amu)*(
c     > apdf(1,x2,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
c     >+apdf(4,x2,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5))
c     >+apdf(-2,x2,amu)*(vkm(1,2)+vkm(4,2))
c     >+apdf(-3,x2,amu)*(vkm(1,3)+vkm(4,3))
c     >+apdf(-5,x2,amu)*(vkm(1,5)+vkm(4,5)) )
c
c       wp_g=splitsud(0,x2,amu)*(
c     > apdf(1,x1,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
c     >+apdf(4,x1,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5))
c     >+apdf(-2,x1,amu)*(vkm(1,2)+vkm(4,2))
c     >+apdf(-3,x1,amu)*(vkm(1,3)+vkm(4,3))
c     >+apdf(-5,x1,amu)*(vkm(1,5)+vkm(4,5)) )
c
c        asymp_2=g_wp+wp_g
csb
       u_g_wp=splitsud(0,x1,amu)*(
     > apdf(1,x2,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
     >+apdf(4,x2,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5)) )

       db_g_wp=splitsud(0,x1,amu)*(
     > apdf(-2,x2,amu)*(vkm(1,2)+vkm(4,2))
     >+apdf(-3,x2,amu)*(vkm(1,3)+vkm(4,3))
     >+apdf(-5,x2,amu)*(vkm(1,5)+vkm(4,5)) )

       u_wp_g=splitsud(0,x2,amu)*(
     > apdf(1,x1,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
     >+apdf(4,x1,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5)) )

       db_wp_g=splitsud(0,x2,amu)*(
     > apdf(-2,x1,amu)*(vkm(1,2)+vkm(4,2))
     >+apdf(-3,x1,amu)*(vkm(1,3)+vkm(4,3))
     >+apdf(-5,x1,amu)*(vkm(1,5)+vkm(4,5)) )

       if(lepasy.eq.0) then
        asymp_2 = u_g_wp+u_wp_g+db_g_wp+db_wp_g
       else if (lepasy.eq.1) then
        asymp_2 =-u_g_wp+u_wp_g+db_g_wp-db_wp_g
       endif

c -----------------
      else if(type_v.eq.'W-'.or. type_v.eq.'hm'
     > .or. type_v.eq.'w-_ra_dub') then

c       g_wm=splitsud(0,x1,amu)*(
c     > apdf(-1,x2,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
c     >+apdf(-4,x2,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5))
c     >+apdf(2,x2,amu)*(vkm(1,2)+vkm(4,2))
c     >+apdf(3,x2,amu)*(vkm(1,3)+vkm(4,3))
c     >+apdf(5,x2,amu)*(vkm(1,5)+vkm(4,5)) )
c
c       wm_g=splitsud(0,x2,amu)*(
c     > apdf(-1,x1,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
c     >+apdf(-4,x1,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5))
c     >+apdf(2,x1,amu)*(vkm(1,2)+vkm(4,2))
c     >+apdf(3,x1,amu)*(vkm(1,3)+vkm(4,3))
c     >+apdf(5,x1,amu)*(vkm(1,5)+vkm(4,5)) )
c
c        asymp_2=g_wm+wm_g
csb
       ub_g_wm=splitsud(0,x1,amu)*(
     > apdf(-1,x2,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
     >+apdf(-4,x2,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5)) )

       d_g_wm=splitsud(0,x1,amu)*(
     > apdf(2,x2,amu)*(vkm(1,2)+vkm(4,2))
     >+apdf(3,x2,amu)*(vkm(1,3)+vkm(4,3))
     >+apdf(5,x2,amu)*(vkm(1,5)+vkm(4,5)) )

       ub_wm_g=splitsud(0,x2,amu)*(
     > apdf(-1,x1,amu)*(vkm(1,2)+vkm(1,3)+vkm(1,5))
     >+apdf(-4,x1,amu)*(vkm(4,2)+vkm(4,3)+vkm(4,5)) )

       d_wm_g=splitsud(0,x2,amu)*(
     > apdf(2,x1,amu)*(vkm(1,2)+vkm(4,2))
     >+apdf(3,x1,amu)*(vkm(1,3)+vkm(4,3))
     >+apdf(5,x1,amu)*(vkm(1,5)+vkm(4,5)) )

       if(lepasy.eq.0) then
        asymp_2 = d_g_wm+d_wm_g+ub_g_wm+ub_wm_g
       else if (lepasy.eq.1) then
        asymp_2 =-d_g_wm+d_wm_g+ub_g_wm-ub_wm_g
       endif

c -----------------
      else if (type_v.eq.'h+') then

       c_g_hp=splitsud(0,x1,amu)*apdf(4,x2,amu)

       bb_g_hp=splitsud(0,x1,amu)*apdf(-5,x2,amu)

       c_hp_g=splitsud(0,x2,amu)*apdf(4,x1,amu)

       bb_hp_g=splitsud(0,x2,amu)*apdf(-5,x1,amu)

       if(lepasy.eq.0) then
        asymp_2 = c_g_hp+c_hp_g+bb_g_hp+bb_hp_g
       else if (lepasy.eq.1) then
        asymp_2 =-c_g_hp+c_hp_g+bb_g_hp-bb_hp_g
       endif

c -----------------
      else if (type_v.eq.'hb') then

       b_g_hp=splitsud(0,x1,amu)*apdf(5,x2,amu)

       bb_g_hp=splitsud(0,x1,amu)*apdf(-5,x2,amu)

       b_hp_g=splitsud(0,x2,amu)*apdf(5,x1,amu)

       bb_hp_g=splitsud(0,x2,amu)*apdf(-5,x1,amu)

       if(lepasy.eq.0) then
        asymp_2 = b_g_hp+b_hp_g+bb_g_hp+bb_hp_g
       else if (lepasy.eq.1) then
        asymp_2 =-b_g_hp+b_hp_g+bb_g_hp-bb_hp_g
       endif

c -----------------
      else if(type_v.eq.'z0'.or. type_v.eq.'a0' .or.
     > type_v.eq.'aa' .or. type_v.eq.'zz' .or.
     > type_v.eq.'hz'.or. type_v.eq.'gl' ) then

        if(ibeam.eq.-2) then
c for pion_minus nucleus scattering
c for (pion_minus p)

       ihadron=-2
c for pion_minus in splitsud

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(1,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4)
     >+(apdf(2,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-1,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4)
     >+(apdf(-2,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       ihadron=1
c for proton in splitsud

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(1,x1,amu))*zff2(1)
     >+(apdf_pion_minus(4,x1,amu))*zff2(4)
     >+(apdf_pion_minus(2,x1,amu))*zff2(2)
     >+(apdf_pion_minus(3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(-1,x1,amu))*zff2(1)
     >+(apdf_pion_minus(-4,x1,amu))*zff2(4)
     >+(apdf_pion_minus(-2,x1,amu))*zff2(2)
     >+(apdf_pion_minus(-3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asymp_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asymp_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

c for (pion-minus n)

       ihadron=-2
c for pion_minus in splitsud

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(2,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4)
     >+(apdf(1,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-2,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4)
     >+(apdf(-1,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       ihadron=1
c for proton in splitsud

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(2,x1,amu))*zff2(1)
     >+(apdf_pion_minus(4,x1,amu))*zff2(4)
     >+(apdf_pion_minus(1,x1,amu))*zff2(2)
     >+(apdf_pion_minus(3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(-2,x1,amu))*zff2(1)
     >+(apdf_pion_minus(-4,x1,amu))*zff2(4)
     >+(apdf_pion_minus(-1,x1,amu))*zff2(2)
     >+(apdf_pion_minus(-3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asyn_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asyn_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

        asymp_2=asymp_2*(1.0d0-fract_n)+asyn_2*fract_n

        else

c for (p p)

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(1,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4)
     >+(apdf(2,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-1,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4)
     >+(apdf(-2,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(1,x1,amu))*zff2(1)+(apdf(4,x1,amu))*zff2(4)
     >+(apdf(2,x1,amu))*zff2(2)+(apdf(3,x1,amu))*zff2(3)
     >+(apdf(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-1,x1,amu))*zff2(1)+(apdf(-4,x1,amu))*zff2(4)
     >+(apdf(-2,x1,amu))*zff2(2)+(apdf(-3,x1,amu))*zff2(3)
     >+(apdf(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asymp_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asymp_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

       if(ibeam.ne.0) goto 999

c for (p n)

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(2,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4)
     >+(apdf(1,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-2,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4)
     >+(apdf(-1,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

cpn feb 2004 the following code is wrong. the pdfs for the first beam are
cpn for the proton, not for the neutron. the corrected code follows below.
c$$$       q_g_z = splitsud(0,x2,amu)*(
c$$$     > (apdf(2,x1,amu))*zff2(1)+(apdf(4,x1,amu))*zff2(4)
c$$$     >+(apdf(1,x1,amu))*zff2(2)+(apdf(3,x1,amu))*zff2(3)
c$$$     >+(apdf(5,x1,amu))*zff2(5) )
c$$$
c$$$       qb_g_z = splitsud(0,x2,amu)*(
c$$$     > (apdf(-2,x1,amu))*zff2(1)+(apdf(-4,x1,amu))*zff2(4)
c$$$     >+(apdf(-1,x1,amu))*zff2(2)+(apdf(-3,x1,amu))*zff2(3)
c$$$     >+(apdf(-5,x1,amu))*zff2(5) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(1,x1,amu))*zff2(1)+(apdf(4,x1,amu))*zff2(4)
     >+(apdf(2,x1,amu))*zff2(2)+(apdf(3,x1,amu))*zff2(3)
     >+(apdf(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-1,x1,amu))*zff2(1)+(apdf(-4,x1,amu))*zff2(4)
     >+(apdf(-2,x1,amu))*zff2(2)+(apdf(-3,x1,amu))*zff2(3)
     >+(apdf(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asyn_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asyn_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

        asymp_2=asymp_2*(1.0d0-fract_n)+asyn_2*fract_n

        endif

!zl
      else if(type_v.eq.'ww_uub' .or. type_v.eq.'ww_ddb') then

       if(type_v.eq.'ww_uub' .or. type_v.eq.'ww_ddb') then

        if(ibeam.eq.-2) then
c for pion_minus nucleus scattering
c for (pion_minus p)

       ihadron=-2
c for pion_minus in splitsud

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(1,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-1,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4) )

       ihadron=1
c for proton in splitsud

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(1,x1,amu))*zff2(1)
     >+(apdf_pion_minus(4,x1,amu))*zff2(4) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(-1,x1,amu))*zff2(1)
     >+(apdf_pion_minus(-4,x1,amu))*zff2(4) )

       if (lepasy.eq.0) then
         asymp_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asymp_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

c for (pion-minus n)

       ihadron=-2
c for pion_minus in splitsud

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(2,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-2,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4) )

       ihadron=1
c for proton in splitsud

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(2,x1,amu))*zff2(1)
     >+(apdf_pion_minus(4,x1,amu))*zff2(4) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(-2,x1,amu))*zff2(1)
     >+(apdf_pion_minus(-4,x1,amu))*zff2(4) )

       if (lepasy.eq.0) then
         asyn_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asyn_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

        asymp_2=asymp_2*(1.0d0-fract_n)+asyn_2*fract_n

        else

c for (p p)

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(1,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-1,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(1,x1,amu))*zff2(1)+(apdf(4,x1,amu))*zff2(4) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-1,x1,amu))*zff2(1)+(apdf(-4,x1,amu))*zff2(4) )

       if (lepasy.eq.0) then
         asymp_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asymp_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

       if(ibeam.ne.0) goto 999

c for (p n)

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(2,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-2,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(1,x1,amu))*zff2(1)+(apdf(4,x1,amu))*zff2(4) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-1,x1,amu))*zff2(1)+(apdf(-4,x1,amu))*zff2(4) )

       if (lepasy.eq.0) then
         asyn_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asyn_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

        asymp_2=asymp_2*(1.0d0-fract_n)+asyn_2*fract_n

        endif

       elseif(type_v.eq.'ww_ddb') then

        if(ibeam.eq.-2) then
c for pion_minus nucleus scattering
c for (pion_minus p)

       ihadron=-2
c for pion_minus in splitsud

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(2,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-2,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       ihadron=1
c for proton in splitsud

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(2,x1,amu))*zff2(2)
     >+(apdf_pion_minus(3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(-2,x1,amu))*zff2(2)
     >+(apdf_pion_minus(-3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asymp_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asymp_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

c for (pion-minus n)

       ihadron=-2
c for pion_minus in splitsud

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(1,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-1,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       ihadron=1
c for proton in splitsud

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(1,x1,amu))*zff2(2)
     >+(apdf_pion_minus(3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf_pion_minus(-1,x1,amu))*zff2(2)
     >+(apdf_pion_minus(-3,x1,amu))*zff2(3)
     >+(apdf_pion_minus(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asyn_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asyn_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

        asymp_2=asymp_2*(1.0d0-fract_n)+asyn_2*fract_n

        else

c for (p p)

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(2,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-2,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(2,x1,amu))*zff2(2)+(apdf(3,x1,amu))*zff2(3)
     >+(apdf(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-2,x1,amu))*zff2(2)+(apdf(-3,x1,amu))*zff2(3)
     >+(apdf(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asymp_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asymp_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

       if(ibeam.ne.0) goto 999

c for (p n)

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(1,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-1,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(2,x1,amu))*zff2(2)+(apdf(3,x1,amu))*zff2(3)
     >+(apdf(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-2,x1,amu))*zff2(2)+(apdf(-3,x1,amu))*zff2(3)
     >+(apdf(-5,x1,amu))*zff2(5) )

       if (lepasy.eq.0) then
         asyn_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asyn_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if

        asymp_2=asymp_2*(1.0d0-fract_n)+asyn_2*fract_n

        endif

       endif

ccpy
cji sept 2013: added in zu and zd to match c++ version
      else if(type_v.eq.'z0_ra_uub' .or. type_v.eq.'z0_ra_ddb'
     >  .or. type_v.eq.'zu' .or. type_v.eq.'zd'
     >  .or. type_v.eq.'ZU' .or. type_v.eq.'ZD') then

        if(ibeam.ne.1) then
ccpy only for (p p) collision
           print*,' not implemented for ibeam = ', ibeam        
           call quit
        endif
           
c for (p p)
        if( type_v.eq.'z0_ra_uub' .or. type_v.eq.'zu'
     >                  .or. type_v.eq.'ZU') then

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(1,x2,amu))*zff2(1)+(apdf(4,x2,amu))*zff2(4) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-1,x2,amu))*zff2(1)+(apdf(-4,x2,amu))*zff2(4) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(1,x1,amu))*zff2(1)+(apdf(4,x1,amu))*zff2(4) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-1,x1,amu))*zff2(1)+(apdf(-4,x1,amu))*zff2(4) )

        elseif( type_v.eq.'z0_ra_ddb' .or. type_v.eq.'zd'
     >                  .or. type_v.eq.'ZD') then

       g_q_z = splitsud(0,x1,amu)*(
     > (apdf(2,x2,amu))*zff2(2)+(apdf(3,x2,amu))*zff2(3)
     >+(apdf(5,x2,amu))*zff2(5) )

       g_qb_z = splitsud(0,x1,amu)*(
     > (apdf(-2,x2,amu))*zff2(2)+(apdf(-3,x2,amu))*zff2(3)
     >+(apdf(-5,x2,amu))*zff2(5) )

       q_g_z = splitsud(0,x2,amu)*(
     > (apdf(2,x1,amu))*zff2(2)+(apdf(3,x1,amu))*zff2(3)
     >+(apdf(5,x1,amu))*zff2(5) )

       qb_g_z = splitsud(0,x2,amu)*(
     > (apdf(-2,x1,amu))*zff2(2)+(apdf(-3,x1,amu))*zff2(3)
     >+(apdf(-5,x1,amu))*zff2(5) )
       
        endif

       if (lepasy.eq.0) then
         asymp_2 = g_q_z + g_qb_z + q_g_z + qb_g_z
       else if (lepasy.eq.1) then
         asymp_2 =-g_q_z + g_qb_z + q_g_z - qb_g_z
       end if


c---------------------
      else if ( type_v.eq.'h0' .or. type_v.eq.'ag' .or.
     .          type_v.eq.'zg' .or. type_v.eq.'gg' ) then
c for q g ---> h g

       g_q=0.d0
       q_g=0.d0
       do 10 k=-5,5
        if(k.eq.0) goto 10
        g_q=g_q+apdf(0,x1,amu)*splitsud(k,x2,amu)
        q_g=q_g+splitsud(k,x1,amu)*apdf(0,x2,amu)
10     continue

       asymp_2=g_q+q_g

      endif

999   continue

      asymp=asymp_1+asymp_2

csb   lepasy=-1 is built in 5/12/95
      asymp_sym=asymp
      asymp_ant=asymp

ccpy set ltopt=0 for calculating the asymptotic part
c    set ltopt=1 for calculating the delta_sigma from qt=0 to pt
c    set ltopt=-1 for calculating the leading order result

      if(ltopt.eq.0) then
c for asymptotic part

c this is d(sigma)/d(q^2)d(qt^2)d(y)
        asymp_sym=hbarc2*const/ecm2*asymp_sym*a_alpi/(2.0*qt_v**2)
     >        /prev_under/prev_under
        asymp_ant=hbarc2*const/ecm2*asymp_ant*a_alpi/(2.0*qt_v**2)
     >        /prev_under/prev_under
c convert to d(sigma)/d(q^2)d(qt)d(y)
        asymp_sym=asymp_sym*2.0*qt_v
        asymp_ant=asymp_ant*2.0*qt_v
      elseif(ltopt.eq.1) then
c for delta_sigma

c this is d(sigma)/d(q^2)d(y)  after integrating qt^2 from 0 to pt^2
        asymp_sym=hbarc2*const/ecm2*asymp_sym
     >        /prev_under/prev_under
        asymp_ant=hbarc2*const/ecm2*asymp_ant
     >        /prev_under/prev_under
      elseif(ltopt.eq.-1) then
c for calculating the leading order result
        asymp_sym=hbarc2*const/ecm2*asymp_sym
     >        /prev_under/prev_under
        asymp_ant=hbarc2*const/ecm2*asymp_ant
     >        /prev_under/prev_under
      endif

      asymp=asymp_sym

      return
      end

C --------------------------------------------------------------------------
      subroutine x1x2int(x1,x2,sh,th,uh,amu,plumi)
C --------------------------------------------------------------------------
cgal: needed to distinguish between pp and ppbar,pN routines
      implicit none
      real*8 x1,x2,sh,th,uh,amu,plumi
      include 'common.for'
c
      if(ibeam.eq.-1) then ! ppBar
        call X1X2INT_PP(X1,X2,SH,TH,UH,AMU,PLUMI)
      else                 ! pp or pn
        call PNX1X2INT(X1,X2,SH,TH,UH,AMU,PLUMI)
      endif
c
      return
      end

! ---------------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION RUN_MASS_TOT(Q,NF)
! ---------------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (NN=6)
      PARAMETER (ZETA3 = 1.202056903159594D0)
      DIMENSION AM(NN),YMSB(NN)

      REAL*8 qmas_u,qmas_d,qmas_s,qmas_c,qmas_b,qmas_t
      COMMON/QMASES/ qmas_u,qmas_d,qmas_s,qmas_c,qmas_b,qmas_t
      real*8 MT,MW,MZ,MH,MA,MW2,MZ2,
     >WCOUPL,ZCOUPL,HCOUPL,ACOUPL,HpCOUPL,HbCOUPL
      COMMON/STAND1/ MT,MW,MZ,MH,MA,MW2,MZ2,
     >WCOUPL,ZCOUPL,HCOUPL,ACOUPL,HpCOUPL,HbCOUPL
      REAL*8 XMTOP,XMBOT,XMC
      COMMON/XMASS/XMTOP,XMBOT,XMC

c      REAL*8 X_A,X_B,ECM,ECM2,YMAX
c      INTEGER IBEAM,LEPASY,LTOPT,i_Model,i_RunMass
c      COMMON/INPUT2/ X_A,X_B,ECM,ECM2,YMAX,IBEAM,LEPASY,LTOPT,
c     ,i_Model,i_RunMass

      B0(NF) = (33.D0-2.D0*NF)/12D0
      B1(NF) = (102D0-38D0/3D0*NF)/16D0
      B2(NF) = (2857D0/2D0-5033D0/18D0*NF+325D0/54D0*NF**2)/64D0
      G0(NF) = 1D0
      G1(NF) = (202D0/3D0-20D0/9D0*NF)/16D0
      G2(NF) = (1249D0-(2216D0/27D0+160D0/3D0*ZETA3)*NF
     .       - 140D0/81D0*NF**2)/64D0
      C1(NF) = G1(NF)/B0(NF) - B1(NF)*G0(NF)/B0(NF)**2
      C2(NF) = ((G1(NF)/B0(NF) - B1(NF)*G0(NF)/B0(NF)**2)**2
     .       + G2(NF)/B0(NF) + B1(NF)**2*G0(NF)/B0(NF)**3
     .       - B1(NF)*G1(NF)/B0(NF)**2 - B2(NF)*G0(NF)/B0(NF)**2)/2D0
      TRAN(X)=1D0+4D0/3D0*Alpi(X)
      CQ(X,NF)=(2D0*B0(NF)*X)**(G0(NF)/B0(NF))
     .            *(1D0+C1(NF)*X+C2(NF)*X**2)

      PI=4.0D0*ATAN(1.0D0)

      AMS=qmas_s
      AMC=qmas_c
      AMB=XMBOT
      AMT=MT

      ACC = 1.D-8
      AM(1) = qmas_u
      AM(2) = qmas_d
      AM(3) = AMS
      AM(4) = AMC
      AM(5) = AMB
      AM(6) = AMT

C      Print*, ' AMS(i) =',AM(1),AM(2),AM(3),AM(4),AM(5),AM(6)

      IF(NF.GE.4)THEN
       XMSB = AM(NF)/TRAN(AM(NF))
       XMHAT = XMSB/CQ(Alpi(AM(NF)),NF)
      ELSE
       XMSB = 0
       XMHAT = 0
      ENDIF

C      Print*, ' ALFS(Mi)/Pi =',Alpi(AM(4)),Alpi(AM(5)),Alpi(AM(6))

      YMSB(3) = AMS
      IF(NF.EQ.3)THEN
       YMSB(4) = YMSB(3)*CQ(Alpi(AM(4)),3)/
     .                   CQ(Alpi( 1.D0),3)
       YMSB(5) = YMSB(4)*CQ(Alpi(AM(5)),4)/
     .                   CQ(Alpi(AM(4)),4)
       YMSB(6) = YMSB(5)*CQ(Alpi(AM(6)),5)/
     .                   CQ(Alpi(AM(5)),5)
      ELSEIF(NF.EQ.4)THEN
       YMSB(4) = XMSB
       YMSB(5) = YMSB(4)*CQ(Alpi(AM(5)),4)/
     .                   CQ(Alpi(AM(4)),4)
       YMSB(6) = YMSB(5)*CQ(Alpi(AM(6)),5)/
     .                   CQ(Alpi(AM(5)),5)
      ELSEIF(NF.EQ.5)THEN
       YMSB(5) = XMSB
       YMSB(4) = YMSB(5)*CQ(Alpi(AM(4)),4)/
     .                   CQ(Alpi(AM(5)),4)
       YMSB(6) = YMSB(5)*CQ(Alpi(AM(6)),5)/
     .                   CQ(Alpi(AM(5)),5)
      ELSEIF(NF.EQ.6)THEN
       YMSB(6) = XMSB
       YMSB(5) = YMSB(6)*CQ(Alpi(AM(5)),5)/
     .                   CQ(Alpi(AM(6)),5)
       YMSB(4) = YMSB(5)*CQ(Alpi(AM(4)),4)/
     .                   CQ(Alpi(AM(5)),4)
      ENDIF
      IF(Q.LT.AMC)THEN
       N0=3
       Q0 = 1.D0
      ELSEIF(Q.LE.AMB)THEN
       N0=4
       Q0 = AMC
      ELSEIF(Q.LE.AMT)THEN
       N0=5
       Q0 = AMB
      ELSE
       N0=6
       Q0 = AMT
      ENDIF
      RUN_MASS_TOT = YMSB(N0)*CQ(Alpi(Q),N0)/
     .               CQ(Alpi(Q0),N0)

      RETURN
      END

!-----------------------------------------------------------------------------


c --------------------------------------------------------------------------
      subroutine delsig_pn(a_x_a,a_x_b,a_q,asymp)
c --------------------------------------------------------------------------
csb calculation of singular pieces for p-p and p-n
      implicit none
      include 'common.for'

      real*8 a_x_a,a_x_b,a_q,asymp,asymp_sym,asymp_ant
      real*8 amu
      real*8 a_sud,dlogqopt2,a_alpi
      common/asy_spl/ a_sud,dlogqopt2,a_alpi
      real*8 u_db,db_u,d_ub,ub_d,u_ub,d_db,ub_u,db_d
      real*8 asymp_1,asymp_2
      integer iflavor
      common/flavor/iflavor
      real*8 P12,P11,P10
      real*8 P24,P23,P22,P21,P20
      external P12,P11,P10
      external P24,P23,P22,P21,P20

      real*8 P10log
      real*8 P22log,P21log,P20log
      external P10log
      external P22log,P21log,P20log

cji jun 2017: add in the second order singular piece 
      real*8  a_sud_2, beta0, nf_eff, x, q
      common /asy_spl2/ a_sud_2, beta0, nf_eff
      integer delsigorder
      common /delsig/ delsigorder
      real*8 splitHoppet, lnmuf,lnmur
      external splitHoppet
      logical first
      save first
      data first /.true./

      !if(first) then
      !    first = .false.
      !call testHoppet
      !endif

      amu=a_q
      a_alpi=alpi(mur*amu)
      nf=NFL(mur*amu)
      call WSETUP(nf)

      amu = muf*amu

        dlogqopt2=dlog(q_v**2/qt_v**2)

        if( type_v.eq.'z0_ra_uub' .or. type_v.eq.'ZU' ) then

       if(delSigOrder.ge.1) then
       u_ub = apdf(1,x_a,amu)*apdf(-1,x_b,amu)
     >      + apdf(4,x_a,amu)*apdf(-4,x_b,amu)
       endif
       if(delSigOrder.ge.2) then
       u_ub=u_ub
     >     +((P12(1,-1,amu)+P12(4,-4,amu))*dlogqopt2**2
     >     +(P11(1,-1,amu)+P11(4,-4,amu))*dlogqopt2
     >     +P10(1,-1,amu)+P10(4,-4,amu))*a_alpi
         if(mur.ne.1.or.muf.ne.1) then
           lnMuR = -2*dlog(mur)
           lnMuF = -2*dlog(muf)
           u_ub=u_ub
     >         +(P10log(1,-1,amu,lnMuR,lnMuF))*a_alpi
         endif
       endif
       if(delSigOrder.ge.3) then
       u_ub=u_ub
     >    +((P24(1,-1,amu)+P24(4,-4,amu))*dlogqopt2**4
     >    +(P23(1,-1,amu)+P23(4,-4,amu))*dlogqopt2**3
     >    +(P22(1,-1,amu)+P22(4,-4,amu))*dlogqopt2**2
     >    +(P21(1,-1,amu)+P21(4,-4,amu))*dlogqopt2
     >    +(P20(1,-1,amu)+P20(4,-4,amu)))*a_alpi**2
         if(mur.ne.1.or.muf.ne.1) then
           lnMuR = -2*dlog(mur)
           lnMuF = -2*dlog(muf)
           u_ub=u_ub
     >         +(P22log(1,-1,amu,lnMuR,lnMuF)*dlogqopt2**2
     >         + P21log(1,-1,amu,lnMuR,lnMuF)*dlogqopt2
     >         + P20log(1,-1,amu,lnMuR,lnMuF))*a_alpi**2
         endif
       endif

       if(delSigOrder.ge.1) then
       ub_u = apdf(-1,x_a,amu)*apdf(1,x_b,amu)
     >      + apdf(-4,x_a,amu)*apdf(4,x_b,amu)
       endif
       if(delSigOrder.ge.2) then
       ub_u=ub_u
     >     +((P12(-1,1,amu)+P12(-4,4,amu))*dlogqopt2**2
     >     +(P11(-1,1,amu)+P11(-4,4,amu))*dlogqopt2
     >     +P10(-1,1,amu)+P10(-4,4,amu))*a_alpi
         if(mur.ne.1.or.muf.ne.1) then
           lnMuR = -2*dlog(mur)
           lnMuF = -2*dlog(muf)
           ub_u=ub_u
     >         +(P10log(-1,1,amu,lnMuR,lnMuF))*a_alpi
         endif
       endif
       if(delSigOrder.ge.3) then
       ub_u=ub_u
     >    +((P24(-1,1,amu)+P24(-4,4,amu))*dlogqopt2**4
     >    +(P23(-1,1,amu)+P23(-4,4,amu))*dlogqopt2**3
     >    +(P22(-1,1,amu)+P22(-4,4,amu))*dlogqopt2**2
     >    +(P21(-1,1,amu)+P21(-4,4,amu))*dlogqopt2
     >    +(P20(-1,1,amu)+P20(-4,4,amu)))*a_alpi**2
         if(mur.ne.1.or.muf.ne.1) then
           lnMuR = -2*dlog(mur)
           lnMuF = -2*dlog(muf)
           ub_u=ub_u
     >         +(P22log(-1,1,amu,lnMuR,lnMuF)*dlogqopt2**2
     >         + P21log(-1,1,amu,lnMuR,lnMuF)*dlogqopt2
     >         + P20log(-1,1,amu,lnMuR,lnMuF))*a_alpi**2
         endif
       endif

       d_db=0.d0
       db_d=0.d0
       
cji september 2013: added in zd to match c++ code
        elseif( type_v.eq.'z0_ra_ddb' .or. type_v.eq.'ZD' ) then

       if(delSigOrder.ge.1) then
       d_db = apdf(2,x_a,amu)*apdf(-2,x_b,amu)
     >      + apdf(3,x_a,amu)*apdf(-3,x_b,amu)
     >      + apdf(5,x_a,amu)*apdf(-5,x_b,amu)
       endif
       if(delSigOrder.ge.2) then
       d_db=d_db
     >       +((P12(2,-2,amu)+P12(3,-3,amu)
     >       +P12(5,-5,amu))*dlogqopt2**2
     >       +(P11(2,-2,amu)+P11(3,-3,amu)
     >       +P11(5,-5,amu))*dlogqopt2
     >       +P10(2,-2,amu)+P10(3,-3,amu)
     >       +P10(5,-5,amu))*a_alpi
       endif
       if(delSigOrder.ge.3) then
       d_db=d_db
     >    +((P24(2,-2,amu)+P24(3,-3,amu)
     >      +P24(5,-5,amu))*dlogqopt2**4
     >     +(P23(2,-2,amu)+P23(3,-3,amu)
     >      +P23(5,-5,amu))*dlogqopt2**3
     >     +(P22(2,-2,amu)+P22(3,-3,amu)
     >      +P22(5,-5,amu))*dlogqopt2**2
     >     +(P21(2,-2,amu)+P21(3,-3,amu)
     >      +P21(5,-5,amu))*dlogqopt2
     >     +(P20(2,-2,amu)+P20(3,-3,amu)
     >      +P20(5,-5,amu)))*a_alpi**2
       endif

       if(delSigOrder.ge.1) then
       db_d = apdf(-2,x_a,amu)*apdf(2,x_b,amu)
     >      + apdf(-3,x_a,amu)*apdf(3,x_b,amu)
     >      + apdf(-5,x_a,amu)*apdf(5,x_b,amu)
       endif
       if(delSigOrder.ge.2) then
       db_d=db_d
     >       +((P12(-2,2,amu)+P12(-3,3,amu)
     >       +P12(-5,5,amu))*dlogqopt2**2
     >      +(P11(-2,2,amu)+P11(-3,3,amu)
     >       +P11(-5,5,amu))*dlogqopt2
     >       +P10(-2,2,amu)+P10(-3,3,amu)
     >       +P10(-5,5,amu))*a_alpi
       endif
       if(delSigOrder.ge.3) then
       db_d=db_d
     >    +((P24(-2,2,amu)+P24(-3,3,amu)
     >      +P24(-5,5,amu))*dlogqopt2**4
     >     +(P23(-2,2,amu)+P23(-3,3,amu)
     >      +P23(-5,5,amu))*dlogqopt2**3
     >     +(P22(-2,2,amu)+P22(-3,3,amu)
     >      +P22(-5,5,amu))*dlogqopt2**2
     >     +(P21(-2,2,amu)+P21(-3,3,amu)
     >      +P21(-5,5,amu))*dlogqopt2
     >     +(P20(-2,2,amu)+P20(-3,3,amu)
     >      +P20(-5,5,amu)))*a_alpi**2
       endif

       u_ub=0.d0
       ub_u=0.d0
       

c -----------------
      else
        print*, ' [delsig_pn] no such a type_v: ', type_v

      endif

       if (lepasy.eq.0) then
         asymp_1=(u_ub+ub_u)*zff2(1)
     >          +(d_db+db_d)*zff2(2)
       else if (lepasy.eq.1) then
         asymp_1=(u_ub-ub_u)*zff2(1)
     >          +(d_db-db_d)*zff2(2)
       end if

      asymp=asymp_1

csb   lepasy=-1 is built in 5/12/95
      asymp_sym=asymp
      asymp_ant=asymp

c for delta_sigma

c this is d(sigma)/d(q^2)d(y)  after integrating qt^2 from 0 to pt^2
        asymp_sym=hbarc2*const/ecm2*asymp_sym
     >        /prev_under/prev_under
        asymp_ant=hbarc2*const/ecm2*asymp_ant
     >        /prev_under/prev_under

      asymp=asymp_sym

      return
      end

      function Splithoppet(iloop,pid,x,amu) 
      implicit none

      integer iloop, pid, nf,i
      real*8 x, amu

      real*8 splitVec(-6:6), splithoppet
      integer nfl
      external nfl

      nf = NFL(amu) 
      call hoppetEvalSplit(x,amu,iloop,nf,splitVec)

      if(pid.eq.1.or.pid.eq.2) then
          i=3-pid
      elseif(pid.eq.-1.or.pid.eq.-2) then
          i=-pid-3
      else
          i=pid
      endif

      splithoppet = splitvec(i)/x

      end


      function P12(pid1,pid2,amu)
      implicit none
      integer pid1, pid2
      real*8 amu
      real*8 P12
      include "common.for"

      nf = NFL(amu)
    
      P12 = -0.5*A1*APDF(pid1,x_a,amu)*APDF(pid2,x_b,amu)

      end

      function P11(pid1,pid2,amu)
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P11
      real*8 splitHoppet
      external splitHoppet

      P11 = -B1*APDF(pid1,x_a,amu)*APDF(pid2,x_b,amu)
     >      -0.5*(SPLITHoppet(1,pid1,x_a,amu)*APDF(pid2,x_b,amu)
     >           +SPLITHoppet(1,pid2,x_b,amu)*APDF(pid1,x_a,amu))

      end

      function P10(pid1,pid2,amu)
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P10, C1xf
      external C1xf

      P10 = APDF(pid1,x_a,amu)*C1xf(pid2,x_b,amu) 
     >    + APDF(pid2,x_b,amu)*C1xf(pid1,x_a,amu) 

      end

      function P10log(pid1,pid2,amu,lnmur,lnmuf)
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P10log, lnmur, lnmuf
      real*8 splitHoppet
      external splitHoppet

      P10log= 0.5*lnmuf*(SPLITHoppet(1,pid1,x_a,amu)*APDF(pid2,x_b,amu)
     >                  +SPLITHoppet(1,pid2,x_b,amu)*APDF(pid1,x_a,amu))

      end

      function P24(pid1,pid2,amu)
      implicit none
      integer pid1, pid2
      real*8 amu
      real*8 P24
      include "common.for"

      P24 = 1d0/8d0*A1**2*APDF(pid1,x_a,amu)*APDF(pid2,x_b,amu)

      end

      function P23(pid1,pid2,amu)
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P23, beta0
      real*8 splitHoppet
      external splitHoppet

      nf = NFL(amu)
      CA = 3
      beta0 = (11*CA-2*nf)/12d0

      P23=(0.5*A1*B1-1d0/3d0*A1*beta0)
     >   *APDF(pid1,x_a,amu)*APDF(pid2,x_b,amu)
     >   +0.25*A1*(SPLITHoppet(1,pid1,x_a,amu)*APDF(pid2,x_b,amu)
     >            +SPLITHoppet(1,pid2,x_b,amu)*APDF(pid1,x_a,amu))

      end

      function P22(pid1,pid2,amu)
      implicit none
      integer pid1, pid2
      real*8 amu
      real*8 P22, beta0
      include "common.for"
      real*8 splitHoppet, C1xF
      external splitHoppet, C1xF

      beta0 = (11*CA-2*nf)/12d0

      P22 = -0.5*A1*APDF(pid1,x_a,amu)*C1xF(pid2,x_b,amu)
     >  -0.5*A1*APDF(pid2,x_b,amu)*C1xF(pid1,x_a,amu)
     >  +(0.5*B1**2-0.5*beta0*B1-0.5*A2)
     >    *APDF(pid1,x_a,amu)*APDF(pid2,x_b,amu)
     >  -0.25*beta0*APDF(pid1,x_a,amu)*Splithoppet(1,pid2,x_b,amu)
     >  -0.25*beta0*APDF(pid2,x_b,amu)*Splithoppet(1,pid1,x_a,amu)
     >  +0.5*b1*APDF(pid1,x_a,amu)*Splithoppet(1,pid2,x_b,amu)
     >  +0.5*b1*APDF(pid2,x_b,amu)*Splithoppet(1,pid1,x_a,amu)
     >  +1d0/8d0*splithoppet(11,pid1,x_a,amu)*APDF(pid2,x_b,amu)
     >  +1d0/8d0*splithoppet(11,pid2,x_b,amu)*APDF(pid1,x_a,amu)
     >  +1d0/4d0*splithoppet(1,pid1,x_a,amu)*splithoppet(1,pid2,x_b,amu)

      end

      function P21(pid1,pid2,amu)
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P21
      real*8 splitHoppet,beta0, C1xF, C1P1xF
      external splitHoppet, C1xF, C1P1xF

      beta0 = (11*CA-2*nf)/12d0

      P21 = (-2*ZETA3*A1**2-B2)*APDF(pid1,x_a,amu)*APDF(pid2,x_b,amu)
     > -B1*APDF(pid1,x_a,amu)*C1xF(pid2,x_b,amu)
     > -B1*APDF(pid2,x_b,amu)*C1xF(pid1,x_a,amu)
     > -0.5*C1xF(pid1,x_a,amu)*splithoppet(1,pid2,x_b,amu)
     > -0.5*C1xF(pid2,x_b,amu)*splithoppet(1,pid1,x_a,amu)
     > -0.5*C1P1xF(pid1,x_a,amu)*APDF(pid2,x_b,amu)
     > -0.5*C1P1xF(pid2,x_b,amu)*APDF(pid1,x_a,amu)
     > +beta0*C1xF(pid1,x_a,amu)*APDF(pid2,x_b,amu)
     > +beta0*C1xF(pid2,x_b,amu)*APDF(pid1,x_a,amu)
     > -0.25*APDF(pid1,x_a,amu)*splithoppet(2,pid2,x_b,amu)
     > -0.25*APDF(pid2,x_b,amu)*splithoppet(2,pid1,x_a,amu)

      end

      function P20(pid1,pid2,amu)
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P20
      real*8 splitHoppet, beta0, C1xF, C2xF
      external splitHoppet, C1xF, C2xF

      beta0 = (11*CA-2*nf)/12d0

      P20 = (-2*ZETA3*B1+4d0/3d0*beta0*ZETA3)*A1
     >           *APDF(pid1,x_a,amu)*APDF(pid2,x_b,amu)
     > -ZETA3*A1*APDF(pid1,x_a,amu)*splithoppet(1,pid2,x_b,amu)
     > -ZETA3*A1*APDF(pid2,x_b,amu)*splithoppet(1,pid1,x_a,amu)
     > +C1xF(pid1,x_a,amu)*C1xF(pid2,x_b,amu)
     > +APDF(pid1,x_a,amu)*C2xF(pid2,x_b,amu)
     > +APDF(pid2,x_b,amu)*C2xF(pid1,x_a,amu)

      end

      function P22log(pid1,pid2,amu,lnmur,lnmuf) 
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P22log, beta0
      real*8 splitHoppet, lnmur,lnmuf
      external splitHoppet

      beta0 = (11*CA-2*nf)/12d0

      P22log=lnmur*0.5*beta0*A1*Apdf(pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      -lnmuf*6.0/24.0*A1*(Splithoppet(1,pid1,x_a,amu)
     >                          *apdf(pid2,x_b,amu)
     >                         +Splithoppet(1,pid2,x_b,amu)
     >                          *apdf(pid1,x_a,amu))
      end

      function P21log(pid1,pid2,amu,lnmur,lnmuf) 
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P21log, beta0
      real*8 splitHoppet, lnmur,lnmuf
      external splitHoppet

      beta0 = (11*CA-2*nf)/12d0

      P21log=lnmur*0.5*beta0*(2*B1*apdf(pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      +splitHoppet(1,pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      +splitHoppet(1,pid2,x_b,amu)*apdf(pid1,x_a,amu))
     >-lnmuf/4.0*(2*B1*(
     >      +splitHoppet(1,pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      +splitHoppet(1,pid2,x_b,amu)*apdf(pid1,x_a,amu))
     >      +splitHoppet(1,pid1,x_a,amu)*splithoppet(1,pid2,x_b,amu)
     >      +splitHoppet(11,pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      +splitHoppet(11,pid2,x_b,amu)*apdf(pid1,x_a,amu))
      end

      function P20log(pid1,pid2,amu,lnmur,lnmuf) 
      IMPLICIT none
      include "common.for"
      integer pid1, pid2
      real*8 amu, P20log, beta0
      real*8 splitHoppet, lnmur,lnmuf, C1xf, C1P1xf
      external splitHoppet, C1xf, C1P1xf

      beta0 = (11*CA-2*nf)/12d0

      P20log=lnmur*beta0*(C1xf(pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      +C1xf(pid2,x_b,amu)*apdf(pid1,x_a,amu))
     >+lnmuf/4.0*(2*C1xf(pid1,x_a,amu)*SPLITHoppet(1,pid2,x_b,amu)
     >      +2*C1xf(pid2,x_b,amu)*SPLITHoppet(1,pid1,x_a,amu) 
     >      +2*C1P1xf(pid1,x_a,amu)*apdf(pid2,x_b,amu) 
     >      +2*C1P1xf(pid2,x_b,amu)*apdf(pid1,x_a,amu) 
     >      +SPLITHoppet(2,pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      +SPLITHoppet(2,pid2,x_b,amu)*apdf(pid1,x_a,amu))
     >-lnmur*lnmuf/2.0*beta0*(
     >      SPLITHoppet(1,pid1,x_a,amu)*apdf(pid2,x_b,amu)
     >      +SPLITHoppet(1,pid2,x_b,amu)*apdf(pid1,x_a,amu))
      end

      function C1xF(pid,x,amu)
      implicit none
      real*8 C1xF,x,amu
      integer pid
      include "common.for"
      real*8 CJJ1xfj_1,uplim,dnlim,aerr,rerr
      real*8 CJJ1xfj_2,fpdf,intc1xf,errest,adz3nt
      real*8 amu_int, x_int
      integer iacta, iactb,ier, pid_int
      external intc1xf, adz3nt
      common/c1int/x_int,amu_int,pid_int

      x_int = x
      amu_int = amu
      pid_int = pid

      FPDF = APDF(pid,x,amu)

      CJJ1XFJ_1 = 0.25d0*CF*(PI**2-8.d0)*FPDF
      UPLIM=1.0
      DNLIM=X
      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

          CJJ1XFJ_2=ADZ3NT(IntC1xF,DNLIM,UPLIM,AERR,RERR,ERREST,
     >    IER,IACTA,IACTB)

      C1xF = CJJ1xfj_1+CJJ1xfj_2

      end

      function C2xF(pid,x,amu)
      implicit none
      real*8 C2xF,x,amu
      integer pid
      include "common.for"
      real*8 CJJ1xfj_1,uplim,dnlim,aerr,rerr
      real*8 CJJ1xfj_2,fpdf,intc1xf,errest,adz3nt
      real*8 CJJ1xfj_3, spcjfj2
      integer iacta, iactb,ier, pid_int
      external intc2xf, adz3nt, spcjfj2
      real*8 x_int, amu_int
      common/c2int/x_int,amu_int,pid_int

      x_int = x
      amu_int = amu
      pid_int = pid

      FPDF = APDF(pid,x,amu)

C C(2) delta(1-z) Contribution
          CJJ1XFJ_1 = ((CA*CF*(59.D0/18.D0*ZETA3-1535.D0/192.D0
     >          +215.D0/216.D0*PI2-PI2**2/240.D0)
     >          +0.25*CF**2*(-15.D0*ZETA3+511.D0/16.D0-67.D0*PI2/12.D0
     >          +17.D0/45.D0*PI2**2)-1.D0/16.D0*(PI2-8)**2*CF**2
     >          +1.D0/864.D0*CF*NF*(192.D0*ZETA3+1143.D0-152.D0*PI2))
     >          *0.5)*FPDF

      UPLIM=1.0
      DNLIM=X
      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

          CJJ1XFJ_2=ADZ3NT(IntC2xF,DNLIM,UPLIM,AERR,RERR,ERREST,
     >    IER,IACTA,IACTB)

C (1/(1-z))_{+} piece
       CJJ1XFJ_3=(CA*CF*(7.D0*ZETA3/2.D0-101.D0/27.D0)
     > +CF*NF*(14.D0/27.D0))*SPCJFJ2(pid,X,AMU)/2d0

      C2xF = CJJ1xfj_1+CJJ1xfj_2+CJJ1XFJ_3

      end

      function C1P1xF(pid,x,amu)
      implicit none
      real*8 C1P1xF,x,amu
      integer pid
      include "common.for"
      real*8 CJJ1xfj_1,uplim,dnlim,aerr,rerr
      real*8 CJJ1xfj_2,fpdf,intc1xf,errest,adz3nt
      real*8 splithoppet, x_int,amu_int
      integer iacta, iactb,ier,pid_int
      common/c1p1int/x_int,amu_int,pid_int
      external intc1p1xf, adz3nt, splithoppet

      pid_int = pid
      x_int = x
      amu_int = amu

      FPDF = splithoppet(1,pid,x,amu)

      CJJ1XFJ_1 = 0.25d0*CF*(PI**2-8.d0)*FPDF
      UPLIM=1.0
      DNLIM=X
      AERR=ACC_AERR
      RERR=ACC_RERR
      IACTA=2
      IACTB=2

          CJJ1XFJ_2=ADZ3NT(IntC1P1xF,DNLIM,UPLIM,AERR,RERR,ERREST,
     >    IER,IACTA,IACTB)

      C1P1xF = CJJ1xfj_1+CJJ1xfj_2

      end

      function intC1xF(z)
      implicit none
      include "common.for"
      real*8 z,x_int,amu_int,intC1xF
      real*8 fpdf, t1,t2,c1xf,cqq,cqg
      integer pid
      common/c1int/x_int,amu_int,pid


      FPDF=APDF(pid,Z,AMU_int)
      T1=1.0/Z
      T2=0.5*CF*(1.0-X_int/Z)

      cqq=t1*t2*fpdf

      FPDF=APDF(0,x_int/Z,AMU_int)
      T2=0.5d0*Z*(1.0d0-Z)

      cqg=t1*t2*fpdf

      intc1xf=cqq+cqg

      end

      function intC1P1xF(z)
      implicit none
      include "common.for"
      real*8 z,x_int,amu_int,intC1P1xF
      real*8 fpdf, t1,t2,c1xf,cqq,cqg
      real*8 splithoppet
      external splitHoppet
      integer pid
      common/c1p1int/x_int,amu_int,pid

      FPDF=splitHoppet(1,pid,z,amu_int)
      T1=1.0/Z
      T2=0.5*CF*(1.0-X_int/Z)

      cqq=t1*t2*fpdf

      FPDF=splitHoppet(1,0,x_int/z,amu_int)
      T2=0.5d0*Z*(1.0d0-Z)

      cqg=t1*t2*fpdf

      intc1p1xf=cqq+cqg

      end

      function intC2xf(z)
      implicit none
      include "common.for"
      real*8 z,x_int,amu_int,intC2xF
      real*8 fpdf, t1,t2,cqq,cqg,cqqb,cqqp
      real*8 splithoppet,y,xli
      external splitHoppet,xli
      integer pid,i
      common/c2int/x_int,amu_int,pid

      FPDF=APDF(pid,x_int/Z,AMU_int)

      T1=1.0/Z
          Y=Z
          T2=CA*CF*((1.0+Y**2)/(1-Y)*(-xLi(3,1-y)/2.0
     >+xLi(3,y)-xLi(2,y)*log(y)/2d0
     >-0.5*xLi(2,y)*log(1-y)-1.0/24d0*log(y)**3-0.5*log(1-y)**2*log(y)
     >+1.0/12.0*PI2*log(1-y)-pi2/8d0)
     >+1.0/(1-y)*(-0.25*(11-3*y*y)*ZETA3
     >-1/48d0*(-y*y+12*y+11)*log(y)**2
     >-1.0/36.0*(83*y*y-36*y+29)*log(y)+Pi2*y/4d0)
     >+(1-y)*(xLi(2,y)/2d0+0.5*log(1-y)*log(y))
     >+(y+100)/27d0+0.25*y*log(1-y))
     >+CF*NF*((1+y*y)/(72*(1-y))*log(y)*(3.0*log(y)+10)+(-19*y-37)/108)
     >+CF**2*((1+y*y)/(1-y)*(xLi(3,1-y)/2d0+xLi(2,y)*log(1-y)/2d0
     >+3*xLi(2,y)*log(y)/2d0+0.75*log(y)*log(1-y)**2
     >+log(y)**2*log(1-y)/4d0-1/12d0*pi2*log(1-y))
     >+(1-y)*(-xLi(2,y)-1.5*log(1-y)*log(y)+2*pi2/3d0-29/4d0)
     >+1/24d0*(1+y)*log(y)**3
     >+1/(1-y)*(1/8d0*(-2*y*y+2*y+3)*log(y)**2
     >+0.25*(17*y*y-13*y+4)*log(y))-y/4d0*log(1-y))
     >+CF*CF*((1+y*y)/(1-y)*(5*ZETA3
     >-5*xLi(3,y))/2d0)
     >-CF*CF/4d0*((2*pi2-18)*(1-y)-(1+y)*log(y))     
     >+CF*(1/y*(1-y)*(2*y*y-y+2)*(xLi(2,y)/6+1/6d0*log(1-y)*log(y)
     >-pi2/36)
     >+1/(216*y)*(1-y)*(136*y*y-143*y+172)
     >-1/48d0*(8*y*y+3*y+3)*log(y)**2
     >+1d0/36d0*(32*y*y-30*y+21)*log(y)
     >+1/24d0*(1+y)*log(y)**3 )
       T2 =T2/2d0
        T2 = T2 + 0.25*CF*(1-y)*CF*(Pi2/2d0-4)
      Cqq=T1*T2*FPDF

      FPDF=APDF(0,x_int/Z,AMU_int)

            y=z

      T2=CA*(-1/(12*y)*(1-y)*(11*y*y-y+2)*xLi(2,1-y)
     >+(2*y*y-2*y+1)*(xLi(3,1-y)/8-xLi(2,1-y)*log(1-y)/8
     >+log(1-y)**3/48d0)
     >+(2*y*y+2*y+1)*(3*xLi(3,-y)/8+xLi(3,1/(1+y))/4d0
     >-xLi(2,-y)*log(y)/8-log(1+y)**3/24+log(y)**2*log(1+y)/16
     >+pi2/48*log(1+y))
     >+0.25*y*(1+y)*xLi(2,-y)+y*xLi(3,y)-0.5*y*xLi(2,1-y)*log(y)
     >-y*xLi(2,y)*log(y)-3.0/8.0*(2*y*y+1)*ZETA3-149*y*y/216
     >-(44*y*y-12*y+3)/96*log(y)**2
     >+(68*y*y+6*pi2*y-30*y+21)/72*log(y)+pi2*y/24+43*y/48+43/(108*y)
     >+(2*y+1)/48*log(y)**3-0.5*y*log(1-y)*log(y)**2
     >-(1-y)/8*y*log(1-y)**2
     >+0.25*y*(1+y)*log(1+y)*log(y)+(3-4*y)/16*y*log(1-y)-35/48d0)
     >+CF*((2*y*y-2*y+1)*(ZETA3-xLi(3,1-y)/8-xLi(3,y)/8
     >+xLi(2,1-y)*log(1-y)/8+xLi(2,y)*log(y)/8-log(1-y)**3/48
     >+log(y)*log(1-y)**2/16+log(y)**2*log(1-y)/16)-3*y*y/8
     >-(4*y*y-2*y+1)/96*log(y)**3+(-8*y*y+12*y+1)/64*log(y)**2
     >+(-8*y*y+23*y+8)/32*log(y)+5*pi2/24*(1-y)*y+11*y/32
     >+(1-y)*y/8*log(1-y)**2-0.25*(1-y)*y*log(1-y)*log(y)
     >-(3-4*y)*y/16*log(1-y)-9/32d0)
     >-CF/4.0*(y*log(y)+0.5*(1-y*y)+(pi2-8)*y*(1-y))
     >+0.25*y*(1-y)*CF*(pi2/2d0-4)

      Cqg = t1*t2*fpdf

      fpdf=apdf(-pid,x_int/z,amu_int)
      T2=CF*(CF-1d0/2d0*CA)*((1d0+Z**2)/(1d0+Z)*(3d0*xLi(3,-Z)/2d0
     >  +xLi(3,Z)+xLi(3,1d0/(1d0+Z))-xLi(2,-Z)*log(Z)/2d0
     >  -xLi(2,Z)*log(Z)/2d0-1d0/24d0*log(Z)**3-1d0/6d0*log(1d0+Z)**3
     >  +1d0/4d0*log(1d0+Z)*log(Z)**2+pi2/12d0*log(1d0+Z)-3d0*Zeta3/4d0)
     >  +(1d0-Z)*(xLi(2,Z)/2d0+1d0/2d0*log(1d0-Z)*log(Z)+15d0/8d0)
     >  -1d0/2d0*(1d0+Z)*(xLi(2,-Z)+log(Z)*log(1d0+Z))
     >  +pi2/24d0*(Z-3d0)+1d0/8d0*(11d0*Z+3d0)*log(Z))
     >  +CF*(1d0/(12d0*Z)*(1d0-Z)*(2d0*Z**2-Z+2d0)
     >  *(xLi(2,Z)+log(1d0-Z)*log(Z)-pi2/6d0)
     >  +1d0/(432d0*Z)*(1d0-Z)*(136*Z**2-143d0*Z+172D0)
     >  -1d0/96d0*(8d0*Z**2+3d0*Z+3d0)*log(z)**2
     >  +1d0/72d0*(32d0*z**2-30d0*z+21d0)*log(Z)
     >  +1d0/48d0*(1d0+z)*log(Z)**3)

      Cqqb=t1*t2*fpdf

      fpdf=0
      do i=-5,5 
      if(abs(i).ne.abs(pid).and.i.ne.0) then
          fpdf = fpdf + apdf(i,x_int/z,amu_int)
      endif
      enddo

      T2=CF*(1d0/(12d0*Z)*(1d0-Z)*(2d0*Z**2-Z+2d0)
     >  *(xLi(2,Z)+log(1d0-Z)*log(Z)-pi2/6d0)
     >  +(1d0/(432d0*Z))*(1d0-Z)*(136*Z**2-143*Z+172)
     >  +(1d0/48d0)*(1d0+Z)*log(Z)**3
     >  -(1d0/96d0)*(8d0*Z**2+3*Z+3)*log(z)**2
     >  +(1d0/72d0)*(32d0*Z**2-30*Z+21)*log(Z))

      Cqqp=t1*t2*fpdf

      intc2xf = Cqq+Cqg+Cqqb+Cqqp

      end





