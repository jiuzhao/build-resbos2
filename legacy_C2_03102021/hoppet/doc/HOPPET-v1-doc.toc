\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Perturbative evolution in QCD}{5}{section.2}
\contentsline {section}{\numberline {3}Numerical techniques}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Higher order matrix representation}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Evolution operators}{10}{subsection.3.2}
\contentsline {section}{\numberline {4}Single-flavour grids and convolutions}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Grid definitions (\texttt {grid\_def})}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}$x$--space functions}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Grid convolution operators}{13}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Other operations on \texttt {grid\_conv} objects}{15}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Derived \texttt {grid\_conv} objects}{16}{subsubsection.4.3.2}
\contentsline {subsection}{\numberline {4.4}Truncated moments}{16}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Parton Luminosities}{17}{subsection.4.5}
\contentsline {section}{\numberline {5}Multi-flavour grids and convolutions}{18}{section.5}
\contentsline {subsection}{\numberline {5.1}Full-flavour PDFs and flavour representations}{18}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Human representation.}{19}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Evolution representation}{20}{subsubsection.5.1.2}
\contentsline {subsection}{\numberline {5.2}Splitting function matrices}{21}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Derived splitting matrices}{23}{subsubsection.5.2.1}
\contentsline {subsection}{\numberline {5.3}The DGLAP convolution components}{23}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}QCD constants}{23}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}DGLAP splitting matrices}{24}{subsubsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.3}Mass threshold matrices}{25}{subsubsection.5.3.3}
\contentsline {subsubsection}{\numberline {5.3.4}Putting it together: \texttt {dglap\_holder}}{26}{subsubsection.5.3.4}
\contentsline {section}{\numberline {6}DGLAP evolution}{28}{section.6}
\contentsline {subsection}{\numberline {6.1}Running coupling}{28}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}DGLAP evolution}{30}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Direct evolution}{30}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Precomputed evolution and the \texttt {evln\_operator}}{31}{subsubsection.6.2.2}
\contentsline {section}{\numberline {7}Tabulated PDFs}{32}{section.7}
\contentsline {subsection}{\numberline {7.1}Preparing a PDF table}{32}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Accessing a table}{34}{subsection.7.2}
\contentsline {section}{\numberline {8}Streamlined interface}{35}{section.8}
\contentsline {subsection}{\numberline {8.1}Initialisation}{35}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Usage}{36}{subsection.8.2}
\contentsline {section}{\numberline {9}Benchmarks}{38}{section.9}
\contentsline {subsection}{\numberline {9.1}Accuracy}{38}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Timing}{41}{subsection.9.2}
\contentsline {section}{\numberline {10}Conclusions}{42}{section.10}
\contentsline {section}{\numberline {A}Example programs}{43}{appendix.A}
\contentsline {subsection}{\numberline {A.1}General interface}{43}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}Streamlined interface}{46}{subsection.A.2}
\contentsline {subsection}{\numberline {A.3}Other general-interface examples}{48}{subsection.A.3}
\contentsline {section}{\numberline {B}HOPPET reference guide}{49}{appendix.B}
\contentsline {section}{\numberline {C}Initialisation of grid quantities}{49}{appendix.C}
\contentsline {section}{\numberline {D}NNLO splitting functions}{52}{appendix.D}
\contentsline {section}{\numberline {E}Useful tips on Fortran\nobreakspace {}95}{53}{appendix.E}
\contentsline {paragraph}{Free form.}{53}{section*.3}
\contentsline {paragraph}{Modules, and features relating to arrays.}{53}{section*.4}
\contentsline {paragraph}{Dynamic memory allocation, pointers.}{55}{section*.5}
\contentsline {paragraph}{Derived types.}{56}{section*.6}
\contentsline {paragraph}{Operator overloading}{56}{section*.7}
\contentsline {paragraph}{Floating-point precision:}{57}{section*.8}
\contentsline {paragraph}{Optional and keyword arguments}{57}{section*.9}
