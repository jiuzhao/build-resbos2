#!/bin/bash

if [[ $1 == "" ]]; then
  echo "./gen_grid.sh [-V W+/W-/ZU/ZD]"
	echo "[--sqrts 8000] [--part yasy/delsig/lo]"
  exit
fi

VB="W+"
part="delsig"
sqrts=8000
Ecm=8

for (( i=1; i<=$#; i++)); do
    echo "${i}" "${!i}"
    j=$((i+1))
  if [[ ${!i} == "-V" ]]; then
    VB=${!j}
  elif [[ ${!i} == "--sqrts" ]]; then
    sqrts=${!j}
		Ecm=$(echo "$sqrts / 1000 "| bc)
  elif [[ ${!i} == "--part" ]]; then
    part=${!j}
  fi
done

DIR=${Ecm}TeV
pref=legacy_${DIR}_${VB}

if [[ $part == "yasy" ]]; then
	grid1="./$DIR/${pref}_asy.out"
	grid2="./$DIR/${pref}_y.out"
elif [[ $part == "delsig" ]]; then
  grid1="./$DIR/${pref}_delsig.out"
  grid2="-"	
elif [[ $part == "lo" ]]; then
	grid1="./$DIR/${pref}_lo.out"
	grid2="-"
else
	echo "no part"
	exit
fi

if [[ $VB == "Z"* ]]; then
	QMn=100
else
	QMn=0
fi

in=$DIR/resbos_${VB}_${part}.in

cat > $in << EOF
1,20,20000,50,500000,982019                              > # runs; test iterations (50*20000); MC events (50*100000); seed for random #
$grid1                                                   > Main data grid
$grid2                                                   > Y piece grid (same PDF or -)
0, 0, 0, 0, 1, 0                                         > Un/reweight,kKFacP&Y,iYG,iMG,iMM
0.0, -4.5, 0.0, 10000.0, 4.5                             > Cuts(1): pTlmin,ymin,DelR,pTlmax,ymax
$QMn, 1000, 0.0, 1000., -10., 10.                        > Cuts(2): QMn,QMx,QTMn,QTMx,yWmn,yWmx
0.0, 10000.,0.0                                          > Cuts(3):min,max transvers mass of boson, min missing ET
1.0, 173.0,125.0                                         > Luminosity (pb^-1), mt,mh
ROOTNT2                                                  > Output fromat
1, 903, 0, 5, 0, 0, 0, 0.0                               > qT_Sep,PDF,Proc,Sc,ACut,ACut,iSub,iHQApp,cutM45
WW,0                                                     > hDecay, i_decay
80.379,  2.085                                           > W mass and width
91.1876, 2.4952                                          > Z mass and width
5, 0,  1                                                 > EW flag, qed flag, CKM_flag
EOF

cat $in
./resbos_root ${in%.in}

