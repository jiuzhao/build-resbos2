#!/bin/bash


DIR=8TeV

for vb in "W+" "W-" "ZU" "ZD"; do
	for part in yasy delsig lo; do

nohup ./gen_FO.sh -V $vb --sqrt 8000 --part $part \
	> $DIR/resbos_${vb}_$part.log &

	done
done
