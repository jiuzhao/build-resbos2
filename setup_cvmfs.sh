## LCG
LCGROOT=/cvmfs/sft.cern.ch/lcg/releases
CMTCONFIG=x86_64-centos7-gcc8-opt

## gcc
source ${LCGROOT}/gcc/8.3.0/x86_64-centos7/setup.sh

## HOPPET
export LD_LIBRARY_PATH=/afs/cern.ch/work/j/jiuzhao/theory/hoppet-1.2.0/lib:${LD_LIBRARY_PATH}
export PATH=/afs/cern.ch/work/j/jiuzhao/theory/hoppet-1.2.0/bin:${PATH}

## LHAPDF6
LHAPDFSYS=$LCGROOT/MCGenerators/lhapdf/6.3.0-ff26e/${CMTCONFIG}
export PATH=$LHAPDFSYS/bin:${PATH}
export LD_LIBRARY_PATH=$LHAPDFSYS/lib:${LD_LIBRARY_PATH}
export LHAPDF_DATA_PATH=${LCGROOT}/../external/lhapdfsets/current:${LHAPDFSYS}/share/LHAPDF
export PYTHONPATH=${LHAPDFSYS}/lib/python3.7/site-packages:${PYTHONPATH}

## resbos2
export LD_LIBRARY_PATH=/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/lib64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-build:${LD_LIBRARY_PATH}
export PATH=/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/bin:${PATH}
## root
#source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.20.08/x86_64-centos7-gcc48-opt/bin/thisroot.sh
# BAT
export PATH="/afs/cern.ch/work/j/jiuzhao/theory/bat/BAT_TEST/bin:$PATH"
export LD_LIBRARY_PATH="/afs/cern.ch/work/j/jiuzhao/theory/bat/BAT_TEST/lib:$LD_LIBRARY_PATH"
export CPATH="/afs/cern.ch/work/j/jiuzhao/theory/bat/BAT_TEST/include:$CPATH"
export PKG_CONFIG_PATH="/afs/cern.ch/work/j/jiuzhao/theory/bat/BAT_TEST/lib/pkgconfig:$PKG_CONFIG_PATH"


#source /home/public/Softwares/ROOT/root-6.22.06/obj/bin/thisroot.sh
