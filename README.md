# Build resbos2

## build BAT
```
git clone https://github.com/bat/bat
cd bat
./autogen.sh --prefix=.
make
make install

```
## build hoppet 
```
 ./configure --prefix=/afs/cern.ch/work/j/jiuzhao/theory/hoppet-1.2.0
make
make install
```
## Set Environment
```
source setup_cvmfs.sh
```

## build Resbos2
```
cd resbos2
mkdir build
cd build
cmake ..  -DHoppet_ROOT_DIR=/afs/cern.ch/work/j/jiuzhao/theory/hoppet-1.2.0 -DCMAKE_INSTALL_PREFIX=.
make -j
make install

```


