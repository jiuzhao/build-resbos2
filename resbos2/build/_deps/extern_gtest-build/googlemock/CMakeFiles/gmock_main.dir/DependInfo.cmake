# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/extern_gtest-src/googlemock/src/gmock_main.cc" "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/extern_gtest-build/googlemock/CMakeFiles/gmock_main.dir/src/gmock_main.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_CREATE_SHARED_LIBRARY=1"
  "LOGURU_UNSAFE_SIGNAL_HANDLER=0"
  "LOGURU_WITH_STREAMS=1"
  "gmock_main_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "_deps/extern_gtest-src/googlemock/include"
  "_deps/extern_gtest-src/googlemock"
  "_deps/extern_gtest-src/googletest/include"
  "_deps/extern_gtest-src/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/extern_gtest-build/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/extern_gtest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
