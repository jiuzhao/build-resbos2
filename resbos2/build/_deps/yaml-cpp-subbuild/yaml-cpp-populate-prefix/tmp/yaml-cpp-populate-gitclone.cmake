
if(NOT "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-subbuild/yaml-cpp-populate-prefix/src/yaml-cpp-populate-stamp/yaml-cpp-populate-gitinfo.txt" IS_NEWER_THAN "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-subbuild/yaml-cpp-populate-prefix/src/yaml-cpp-populate-stamp/yaml-cpp-populate-gitclone-lastrun.txt")
  message(STATUS "Avoiding repeated git clone, stamp file is up to date: '/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-subbuild/yaml-cpp-populate-prefix/src/yaml-cpp-populate-stamp/yaml-cpp-populate-gitclone-lastrun.txt'")
  return()
endif()

execute_process(
  COMMAND ${CMAKE_COMMAND} -E rm -rf "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-src"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to remove directory: '/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-src'")
endif()

# try the clone 3 times in case there is an odd git clone issue
set(error_code 1)
set(number_of_tries 0)
while(error_code AND number_of_tries LESS 3)
  execute_process(
    COMMAND "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2727/stable/linux-64/bin/git"  clone --no-checkout "https://github.com/jbeder/yaml-cpp.git" "yaml-cpp-src"
    WORKING_DIRECTORY "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps"
    RESULT_VARIABLE error_code
    )
  math(EXPR number_of_tries "${number_of_tries} + 1")
endwhile()
if(number_of_tries GREATER 1)
  message(STATUS "Had to git clone more than once:
          ${number_of_tries} times.")
endif()
if(error_code)
  message(FATAL_ERROR "Failed to clone repository: 'https://github.com/jbeder/yaml-cpp.git'")
endif()

execute_process(
  COMMAND "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2727/stable/linux-64/bin/git"  checkout 4edff1fa5dbfca16fc72d89870841bee89f8ef89 --
  WORKING_DIRECTORY "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-src"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to checkout tag: '4edff1fa5dbfca16fc72d89870841bee89f8ef89'")
endif()

set(init_submodules TRUE)
if(init_submodules)
  execute_process(
    COMMAND "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2727/stable/linux-64/bin/git"  submodule update --recursive --init 
    WORKING_DIRECTORY "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-src"
    RESULT_VARIABLE error_code
    )
endif()
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: '/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-src'")
endif()

# Complete success, update the script-last-run stamp file:
#
execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy
    "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-subbuild/yaml-cpp-populate-prefix/src/yaml-cpp-populate-stamp/yaml-cpp-populate-gitinfo.txt"
    "/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-subbuild/yaml-cpp-populate-prefix/src/yaml-cpp-populate-stamp/yaml-cpp-populate-gitclone-lastrun.txt"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to copy script-last-run stamp file: '/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/_deps/yaml-cpp-subbuild/yaml-cpp-populate-prefix/src/yaml-cpp-populate-stamp/yaml-cpp-populate-gitclone-lastrun.txt'")
endif()

