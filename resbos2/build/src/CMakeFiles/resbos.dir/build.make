# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2727/stable/linux-64/bin/cmake

# The command to remove a file.
RM = /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2727/stable/linux-64/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /afs/cern.ch/work/j/jiuzhao/theory/resbos2

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build

# Include any dependencies generated for this target.
include src/CMakeFiles/resbos.dir/depend.make

# Include the progress variables for this target.
include src/CMakeFiles/resbos.dir/progress.make

# Include the compile flags for this target's objects.
include src/CMakeFiles/resbos.dir/flags.make

src/CMakeFiles/resbos.dir/main.cc.o: src/CMakeFiles/resbos.dir/flags.make
src/CMakeFiles/resbos.dir/main.cc.o: ../src/main.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object src/CMakeFiles/resbos.dir/main.cc.o"
	cd /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/src && /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2727/stable/linux-64/bin/ccache /cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/resbos.dir/main.cc.o -c /afs/cern.ch/work/j/jiuzhao/theory/resbos2/src/main.cc

src/CMakeFiles/resbos.dir/main.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/resbos.dir/main.cc.i"
	cd /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/src && /cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /afs/cern.ch/work/j/jiuzhao/theory/resbos2/src/main.cc > CMakeFiles/resbos.dir/main.cc.i

src/CMakeFiles/resbos.dir/main.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/resbos.dir/main.cc.s"
	cd /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/src && /cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /afs/cern.ch/work/j/jiuzhao/theory/resbos2/src/main.cc -o CMakeFiles/resbos.dir/main.cc.s

# Object files for target resbos
resbos_OBJECTS = \
"CMakeFiles/resbos.dir/main.cc.o"

# External object files for target resbos
resbos_EXTERNAL_OBJECTS =

src/resbos: src/CMakeFiles/resbos.dir/main.cc.o
src/resbos: src/CMakeFiles/resbos.dir/build.make
src/resbos: src/libResbos.so
src/resbos: src/Beam/libResbosBeam.so
src/resbos: /cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/6.3.0-ff26e/x86_64-centos7-gcc8-opt/lib/libLHAPDF.so
src/resbos: /afs/cern.ch/work/j/jiuzhao/theory/hoppet-1.2.0/lib/libhoppet_v1.a
src/resbos: src/Calculation/libResbosCalculation.so
src/resbos: src/Process/libResbosProcess.so
src/resbos: src/Utilities/libResbosUtilities.so
src/resbos: src/User/libResbosUser.so
src/resbos: _deps/fmt-build/libfmt.so.6.2.1
src/resbos: src/CMakeFiles/resbos.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable resbos"
	cd /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/src && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/resbos.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/CMakeFiles/resbos.dir/build: src/resbos

.PHONY : src/CMakeFiles/resbos.dir/build

src/CMakeFiles/resbos.dir/clean:
	cd /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/src && $(CMAKE_COMMAND) -P CMakeFiles/resbos.dir/cmake_clean.cmake
.PHONY : src/CMakeFiles/resbos.dir/clean

src/CMakeFiles/resbos.dir/depend:
	cd /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /afs/cern.ch/work/j/jiuzhao/theory/resbos2 /afs/cern.ch/work/j/jiuzhao/theory/resbos2/src /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/src /afs/cern.ch/work/j/jiuzhao/theory/resbos2/build/src/CMakeFiles/resbos.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/CMakeFiles/resbos.dir/depend

